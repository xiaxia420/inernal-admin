package de.sportsbook;

import io.prometheus.client.spring.boot.EnablePrometheusEndpoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@SpringBootApplication
@EnablePrometheusEndpoint
public class BetagoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BetagoApplication.class, args);

        try {
            log.info("Running on {}", InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException ignored) {
        }

    }

}
