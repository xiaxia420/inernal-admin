package de.sportsbook.activbet.react;

import de.sportsbook.activbet.configuration.BetagoWebProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
//@Component
//@Profile({"dev", "qa", "production", "enable-ssr"})
public class React {

    private long avgSsrTime = -1;

    private final GaugeService gaugeService;

    public static final String LISTENING_ON_PORT = "Listening on port: ";
    public static final String NODE_VERSION_FILE = ".node-version";
    public static final String WORKING_DIR = "/var/tmp/betago";
    public static final String SSR_APP_FILE = "ssr.min.js";

    private synchronized void logSsrTime(long ssrTime) {
        if (avgSsrTime == -1) {
            avgSsrTime = ssrTime;
        } else {
            avgSsrTime = (avgSsrTime + ssrTime) / 2;
        }
        gaugeService.submit("ssr_time", avgSsrTime);
    }

    @Autowired
    private HttpServletRequest request;

    private Integer servicePort;
    private RestTemplate restTemplate;

    private void installNode(String version, String icu4cVersion) throws IOException, InterruptedException {
        log.info("Installing node version {} to {}", version, WORKING_DIR);

        String nodeName = getNodeName(version);
        String archiveType = ".tar.xz";
        String archiveName = nodeName + archiveType;

        URL url = new URL("https://nodejs.org/dist/" + version + "/" + archiveName);
        URLConnection connection = url.openConnection();

        double size = connection.getContentLengthLong();
        double sizeMb = size / 1_000_000;

        log.info("Downloading {} ({} MB)", url.toString(), sizeMb);
        BufferedInputStream in = new BufferedInputStream(url.openStream());
        Path archivePath = Paths.get(WORKING_DIR, archiveName);
        FileOutputStream out = new FileOutputStream(archivePath.toFile());
        byte data[] = new byte[1024];
        int count;
        while ((count = in.read(data, 0, 1024)) != -1) {
            out.write(data, 0, count);
        }
        log.info("Download complete.");

        // Extract the archive
        Runtime runtime = Runtime.getRuntime();
        String extractCommand = "tar -xf " + archivePath.toAbsolutePath().toString();

        log.info("Extracting archive: {}", extractCommand);
        Process process = runtime.exec(extractCommand, null, Paths.get(WORKING_DIR).toAbsolutePath().toFile());

        if (process.waitFor() == 0) {
            log.info("Extract complete.");
        } else {
            log.info("Extract failed: {}", StreamUtils.copyToString(process.getErrorStream(), StandardCharsets.UTF_8));
        }

        log.info("Installing icu4c version {}.", icu4cVersion);
        // Create a dummy package.json so npm works
        Process dummyPackageJson = runtime.exec("echo '{}' > package.json", null, Paths.get(WORKING_DIR).toAbsolutePath().toFile());
        dummyPackageJson.waitFor();
        Path nodePath = Paths.get(getNodeName(version), "bin", "node");
        Path npmCliPath = Paths.get(getNodeName(version), "lib", "node_modules", "npm", "bin", "npm-cli.js");
        ProcessBuilder npmProcessBuilder = new ProcessBuilder(
 nodePath.toString(),
            npmCliPath.toString(),
            "i",
            "icu4c-data@" + icu4cVersion)
        .directory(Paths.get(WORKING_DIR).toAbsolutePath().toFile())
        .redirectErrorStream(true);

        npmProcessBuilder.environment()
                         .put("npm_config_cache", Paths.get(WORKING_DIR, ".npm")
                         .toAbsolutePath().toString());
        Process npmProcess = npmProcessBuilder.start();
        logProcessOutput(npmProcess);

        int exitCode = npmProcess.waitFor();
        if (exitCode == 0) {
            log.info("Icu4c installed.");
        } else {
            throw new IOException("Error installing the full-icu npm package. Process exited with code " + exitCode);
        }

        // Write the node version to a file to be able to detect the installed version
        Files.write(Paths.get(WORKING_DIR, NODE_VERSION_FILE), version.getBytes(), StandardOpenOption.CREATE);
    }

    private void logProcessOutput(Process process) throws IOException {
        log.info("------------ Process output ------------");
        try (BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            String line;
            while ((line = in.readLine()) != null) {
                log.info(line);
            }
        }
    }

    private void copyAppToWorkingDir() throws IOException {
        log.info("Copying SSR application to working dir.");

        ClassPathResource resource = new ClassPathResource(SSR_APP_FILE);
        StreamUtils.copy(resource.getInputStream(), new FileOutputStream(Paths.get(WORKING_DIR, SSR_APP_FILE).toFile()));
    }

    private String getNodeName(String version) {
        return "node-" + version + "-linux-x64";
    }

    private void deletePathRecursively(Path path) throws IOException {
        if (!path.toFile().exists()) {
            return;
        }
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public React(BetagoWebProperties webProperties, GaugeService gaugeService) {
        this.gaugeService = gaugeService;

        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setReadTimeout(2000);
        requestFactory.setConnectTimeout(1000);
        restTemplate = new RestTemplate(requestFactory);

        try {
            String nodeVersion = webProperties.getSsrNodeVersion();
            String icu4cVersion = webProperties.getIcu4cVersion();

            Path workingDirPath = Paths.get(WORKING_DIR);
            Path nodeVersionPath = Paths.get(WORKING_DIR, NODE_VERSION_FILE);

            File workingDir = workingDirPath.toFile();
            if (workingDir.exists()) {

                log.info("Working dir already exists, checking installed node version.");

                File nodeVersionFile = nodeVersionPath.toFile();
                if (nodeVersionFile.exists()) {
                    String installedNodeVersion = Files.readAllLines(nodeVersionPath).stream().findFirst().orElse(null);
                    if (!Objects.equals(installedNodeVersion, nodeVersion)) {
                        log.info("Found outdated node {}. Deleting and installing {}.", installedNodeVersion, nodeVersion);
                        // Delete the existing version
                        deleteNode(installedNodeVersion);
                        // Install new version
                        installNode(nodeVersion, icu4cVersion);
                    } else {
                        log.info("Installed node version is up-to-date ({}).", nodeVersion);
                    }
                } else {
                    // Node wasn't installed, installing
                    installNode(nodeVersion, icu4cVersion);
                }

                // Node is now setup properly

                // Delete the existing .js file
                deletePathRecursively(Paths.get(WORKING_DIR, SSR_APP_FILE));

                // Copy the current version to working dir
                copyAppToWorkingDir();

            } else {
                // Setup from zero
                // Create the folder
                Files.createDirectories(workingDirPath);
                installNode(nodeVersion, icu4cVersion);
                copyAppToWorkingDir();
            }

            // Start the service
            Runtime rt = Runtime.getRuntime();
            Path nodeExecutablePath = Paths.get(WORKING_DIR, getNodeName(nodeVersion), "bin", "node").toAbsolutePath();

            Path icuDataDir = Paths.get(WORKING_DIR, "node_modules", "icu4c-data").toAbsolutePath();
            Path appPath = Paths.get(WORKING_DIR, SSR_APP_FILE).toAbsolutePath();
            String runCommand = nodeExecutablePath.toString() + " --icu-data-dir=" + icuDataDir.toAbsolutePath().toString() + " " + appPath.toString();
            log.info("Starting SSR service: {}", runCommand);
            Process process = rt.exec(runCommand);
            new Thread(() -> {
                InputStream inputStream = process.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String strLine;
                try {
                    while ((strLine = br.readLine()) != null) {
                        if (strLine.startsWith(LISTENING_ON_PORT)) {
                            int port = Integer.parseInt(strLine.substring(LISTENING_ON_PORT.length()));
                            log.info("SSR Service started on port {}", port);
                            servicePort = port;
                        } else {
                            log.info("PROCESS: {}", strLine);
                        }
                    }

                    int exitCode = process.waitFor();
                    if (exitCode > 0) {
                        log.error("SSR Service exited with status {}", exitCode);
                    } else {
                        log.info("SSR Service exited with status {}", exitCode);
                    }
                } catch (Exception e) {
                    log.error("", e);
                }

            }).start();

        } catch (Exception e) {
            log.error("Error setting up SSR", e);
        }
    }

    private void deleteNode(String installedNodeVersion) throws IOException {
        deletePathRecursively(Paths.get(WORKING_DIR, getNodeName(installedNodeVersion)));
        deletePathRecursively(Paths.get(WORKING_DIR, getNodeName(installedNodeVersion) + ".tar.xz"));
        deletePathRecursively(Paths.get(WORKING_DIR, NODE_VERSION_FILE));
    }

    public Result renderToString(Map<String, Object> state) {

        if (servicePort == null) {
            log.warn("Tried to SSR but the service has not started yet.");
            return new Result();
        }

        long start = System.nanoTime();
        String url = new UrlPathHelper().getPathWithinApplication(request);
        if (request.getQueryString() != null) {
            url += "?" + request.getQueryString();
        }

        Map<String, Object> map = new HashMap<>();
        map.put("location", url);
        map.put("state", state);

        Result result;
        try {
            result = restTemplate.postForObject("http://127.0.0.1:" + servicePort, map, Result.class);
        } catch (Exception e) {
            result = new Result();
            if (e instanceof HttpStatusCodeException) {
                log.error("SSR Exception: {}", ((HttpStatusCodeException) e).getResponseBodyAsString());
            } else {
                log.error("Unknown SSR Exception", e);
            }
        }

        long renderTime = (System.nanoTime() - start) / 1_000_000;
        logSsrTime(renderTime);

        return result;
    }

    @Data
    public static class Result {
        private String pageTitle;
        private String html;
    }

}
