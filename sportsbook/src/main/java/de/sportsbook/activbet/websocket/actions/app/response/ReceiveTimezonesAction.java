package de.sportsbook.activbet.websocket.actions.app.response;

import de.sportsbook.activbet.services.app.dtos.TimeZoneDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveTimezonesAction extends Action {
    private List<TimeZoneDto> timezones;
}
