package de.sportsbook.activbet.websocket.actions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Action {
    private Integer actionId;
    private String actionRequest;
}