package de.sportsbook.activbet.websocket.actions.payment.request;

import de.sportsbook.activbet.api.payment.enums.PayOutRequestFlag;
import de.sportsbook.activbet.api.payment.enums.PaymentDirection;
import de.sportsbook.activbet.api.payment.enums.PaymentTransactionStatus;
import de.sportsbook.activbet.api.payment.enums.PayOutRequestStatus;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestPayOutRequestsAction extends Action {
    private Integer companyId;
    private Integer customerId;
    private String customerUsername;
    private Integer payOutUserId;
    private String from;
    private String to;
    private String keyWord;
    private PayOutRequestStatus status;
    private PayOutRequestFlag flag;
    private Integer pageIndex;
    private Integer pageSize;
    private Boolean orderDescending;
}
