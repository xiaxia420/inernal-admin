package de.sportsbook.activbet.websocket.actions.user;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestLoginConfirmationAction extends Action {

    // See LoginSuccessfulAction
    private String type = "user/REQUEST_CONFIRM_LOGIN";

}
