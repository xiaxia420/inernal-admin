package de.sportsbook.activbet.websocket.actions.ticket.request;

import de.sportsbook.activbet.api.ticket.enums.TicketActive;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.EnumSet;

@Getter
@Setter
@ToString
public class RequestEvaluateTicketAction extends Action {
    private Integer id;
}
