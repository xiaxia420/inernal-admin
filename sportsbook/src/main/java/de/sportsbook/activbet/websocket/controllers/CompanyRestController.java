package de.sportsbook.activbet.websocket.controllers;

import de.sportsbook.activbet.api.company.entities.CashierReport;
import de.sportsbook.activbet.api.company.entities.Company;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.company.CompanyService;
import de.sportsbook.activbet.services.company.dtos.CashFlowDto;
import de.sportsbook.activbet.services.company.dtos.CompanyDto;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.websocket.actions.company.request.RequestCashFlowsAction;
import de.sportsbook.activbet.websocket.actions.company.request.RequestCashierReportAction;
import de.sportsbook.activbet.websocket.actions.company.request.RequestCompanyTreeAction;
import de.sportsbook.activbet.websocket.actions.company.response.ReceiveCashFlowsAction;
import de.sportsbook.activbet.websocket.actions.company.response.ReceiveCashierReportAction;
import de.sportsbook.activbet.websocket.actions.company.response.ReceiveCompanyTreeAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Objects;

@Slf4j
@Controller
public class CompanyRestController extends BaseRestfulController {
    @Autowired
    private CompanyService companyService;

    /**
     *  get companies tree structure
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/company/REQUEST_COMPANY_TREE")
    public ReceiveCompanyTreeAction requestCompanyTree(@RequestBody RequestCompanyTreeAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        //Session session = SessionHolder.getSafeSession();
        List<CompanyDto> companies = companyService.requestCompanyTree();
        ReceiveCompanyTreeAction answer = new ReceiveCompanyTreeAction();
        answer.setActionId(action.getActionId());
        answer.setCompanies(companies);
        return answer;
    }

    /**
        get company list
    */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/company/REQUEST_COMPANY_LIST")
    public ReceiveCompanyTreeAction requestCompanyList(@RequestBody RequestCompanyTreeAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        //Session session = SessionHolder.getSafeSession();
        List<CompanyDto> companies = companyService.requestAllCompanies();
        ReceiveCompanyTreeAction answer = new ReceiveCompanyTreeAction();
        answer.setActionId(action.getActionId());
        answer.setCompanies(companies);
        return answer;
    }

    /**
     *  get cashier report
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/company/REQUEST_WEB_CASHIER_REPORT")
    public ReceiveCashierReportAction requestCashierReport(@RequestBody RequestCashierReportAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        Integer companyId = action.getCompanyId();
        CashierReport reports = companyService.requestCashierReport(session, companyId, action.getFrom(), action.getTo());

        if (StringUtils.isEmpty(reports.getName()) && Objects.nonNull(companyId) ) {
            Company company = companyService.requestCompanyByCompanyId(companyId);
            if (Objects.nonNull(company)) {
                reports.setName(company.getName1() + (StringUtils.isEmpty(company.getName2()) ? "" : " " + company.getName2()));
            }
        }

        ReceiveCashierReportAction answer = new ReceiveCashierReportAction();
        answer .setActionId(action.getActionId());
        answer.setCashierReports(reports);
        answer.setCompanyId(companyId);
        return answer;
    }

    /*
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/company/REQUEST_CASH_FLOWS")
    public ReceiveCashFlowsAction requestCashFlows(@RequestBody RequestCashFlowsAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        CashFlowDto cashFlow = companyService.requestCashFlow(session, action.getCurrencyId());

        ReceiveCashFlowsAction answer = new ReceiveCashFlowsAction();
        answer.setActionId(action.getActionId());
        answer.setCashFlow(cashFlow);
        return answer;
    }
     */
}