package de.sportsbook.activbet.websocket.actions.user;

import de.sportsbook.activbet.services.user.dtos.UserRolesDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LoginSuccessfulAction extends Action {

    // The type is necessary here because we need to distinguish
    // between a successful login and request login confirmation
    // in the frontend.
    private String type = "user/RECEIVE_LOGIN_SUCCESSFUL";

    private Long loginTime;
    private String username;
    private String locale;
    private UserRolesDto userRoles;
    private Integer userCurrencyId;

    //private String email;
    //private Boolean acceptOddModification;
    //private Long automLogoutTime;
    //private Boolean acceptTermsAndConditions;
    //private Boolean forcePasswordChange;
    // after login, set web app language to user prefer language
    //private String userPreferLanguage;
    //private Boolean isWebCashier;

}
