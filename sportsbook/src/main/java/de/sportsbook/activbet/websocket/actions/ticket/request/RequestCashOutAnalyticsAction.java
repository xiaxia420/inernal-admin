package de.sportsbook.activbet.websocket.actions.ticket.request;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestCashOutAnalyticsAction extends Action {
    private String from;
    private String to;
}
