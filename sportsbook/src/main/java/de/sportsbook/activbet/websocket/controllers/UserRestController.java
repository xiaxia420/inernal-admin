package de.sportsbook.activbet.websocket.controllers;

import de.sportsbook.activbet.exceptions.*;
import de.sportsbook.activbet.api.oauth.enums.LogoutReason;
import de.sportsbook.activbet.services.oauth.OauthService;
import de.sportsbook.activbet.services.user.UserService;
import de.sportsbook.activbet.services.user.dtos.UserRolesDto;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.websocket.actions.AckAction;
import de.sportsbook.activbet.websocket.actions.Action;
import de.sportsbook.activbet.websocket.actions.user.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
public class UserRestController extends BaseRestfulController {

    @Autowired
    private UserService userService;

    @Autowired
    private OauthService oAuthService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/user/REQUEST_LOGIN")
    public Action login(@RequestBody LoginAction action) throws InternalException, LoginFailedException {
        try {
            Session session = SessionHolder.getSafeSession();
            UserRolesDto userRolesDto = userService.login(session, action.getUsername(), action.getPassword());
            return this.loginSuccess(session, action.getActionId(), userRolesDto);
        } catch (UserAlreadyLoggedInException e) {
            RequestLoginConfirmationAction answer = new RequestLoginConfirmationAction();
            answer.setActionId(action.getActionId());
            return answer;
        }
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/user/REQUEST_LOGOUT")
    public AckAction logout(@RequestBody LogoutAction action) {
        Session session = SessionHolder.getSafeSession();
        boolean isAutomaticLogout = action.getIsAutomaticLogout() != null && action.getIsAutomaticLogout();
        LogoutReason logoutReason = isAutomaticLogout ? LogoutReason.AUTOMATIC_LOGOUT : LogoutReason.MANUAL_LOGOUT;

        String username = session.getAttribute(SessionAttributes.USERNAME);
        String remoteAddress = session.getAttribute(SessionAttributes.REMOTE_ADDRESS);
        String userAgent = session.getAttribute(SessionAttributes.USER_AGENT);

        userService.logout(session, logoutReason);
        oAuthService.logout(username, remoteAddress, userAgent, logoutReason);
        return new AckAction(action);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/user/REQUEST_CONFIRM_LOGIN")
    public Action confirmLogin(@RequestBody RequestConfirmLoginAction action) throws LoginFailedException {
        Session session = SessionHolder.getSafeSession();
        UserRolesDto userRolesDto = userService.confirmLogin(session, null);
        return this.loginSuccess(session, action.getActionId(), userRolesDto);
    }

    private LoginSuccessfulAction loginSuccess(Session session, Integer actionId, UserRolesDto userRolesDto) {
        LoginSuccessfulAction answer = new LoginSuccessfulAction();
        answer.setActionId(actionId);
        answer.setLoginTime(session.getAttribute(SessionAttributes.LOGIN_TIME));
        answer.setUsername(session.getAttribute(SessionAttributes.USERNAME));
        answer.setLocale(session.getAttribute(SessionAttributes.LOCALE));
        answer.setUserRoles(userRolesDto);
        answer.setUserCurrencyId(session.getAttribute(SessionAttributes.USER_CURRENCY_ID));
        return answer;
    }

}