package de.sportsbook.activbet.websocket.controllers;

import de.sportsbook.activbet.exceptions.PayloadException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.api.oauth.enums.LogoutReason;
import de.sportsbook.activbet.services.user.UserService;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.websocket.actions.ErrorAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.Map;

@Slf4j
@ControllerAdvice(basePackageClasses = MessageControllerAdvice.class)
public class MessageControllerAdvice {

    public static final String SESSION_EXPIRED = "SESSION_EXPIRED";

    @Autowired
    private UserService userService;

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public ErrorAction handleException(Exception e, @Payload Map<String, Object> payload) {
        String message = e.getMessage();
        log.debug("Exception handling WebSocket Message: " + message, e);
        ErrorAction reply = new ErrorAction();
        reply.setActionId((Integer) payload.get("actionId"));
        if (e instanceof UnauthorizedUserException) {
            // The API token is no longer valid (probably because the user logged in from elsewhere)
            userService.logout(SessionHolder.getSession(false), LogoutReason.FORCE_LOGOUT);
            reply.setError(SESSION_EXPIRED);
            return reply;
        }
        if (!e.getClass().getName().startsWith("de.betserv")) {
            log.error("Exception", e);
            message = String.valueOf(e.getMessage());
        }
        reply.setError(message);
        if (e instanceof PayloadException) {
            reply.setPayload(((PayloadException) e).getPayload());
        }
        return reply;
    }

}
