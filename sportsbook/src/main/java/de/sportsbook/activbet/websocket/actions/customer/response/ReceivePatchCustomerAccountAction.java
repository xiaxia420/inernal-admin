package de.sportsbook.activbet.websocket.actions.customer.response;

import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerAccountBookingDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceivePatchCustomerAccountAction extends Action {
    CustomerAccountBookingDto accountBooking;
}
