package de.sportsbook.activbet.websocket.actions.company.request;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestCashFlowsAction extends Action {
    private Integer currencyId;
}
