package de.sportsbook.activbet.websocket.actions.ticket.request;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestUpdateTicketStatisticFlagAction extends Action {
    private String ticketNumber;
}
