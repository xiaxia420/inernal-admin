package de.sportsbook.activbet.websocket.actions;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
public class ErrorAction extends Action {

    private final String type = "ERROR";

    private String error;
    private Map<String, Object> payload;

}
