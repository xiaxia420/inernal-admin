package de.sportsbook.activbet.websocket.actions.ticket.response;

import de.sportsbook.activbet.services.tickets.dtos.TicketDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReceiveEvaluateTicketAction extends Action {
    private TicketDto ticket;
}
