package de.sportsbook.activbet.websocket.actions.app.response;

import de.sportsbook.activbet.api.app.entities.Language;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveLanguagesAction extends Action {

    private List<Language> languages;

}
