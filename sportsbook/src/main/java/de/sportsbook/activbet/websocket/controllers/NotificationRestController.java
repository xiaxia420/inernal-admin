package de.sportsbook.activbet.websocket.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.notification.entities.CashFlowNotification;
import de.sportsbook.activbet.api.ticket.entities.Ticket;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.notifications.NotificationService;
import de.sportsbook.activbet.services.notifications.dtos.CashFlowNotificationDto;
import de.sportsbook.activbet.services.notifications.dtos.MessageNotificationDto;
import de.sportsbook.activbet.services.notifications.dtos.PayOutRequestNotificationDto;
import de.sportsbook.activbet.services.notifications.dtos.PaymentTransactionNotificationDto;
import de.sportsbook.activbet.services.tickets.TicketService;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.websocket.actions.RequestAction;
import de.sportsbook.activbet.websocket.actions.notification.request.RequestCashFlowNotificationAction;
import de.sportsbook.activbet.websocket.actions.notification.response.ReceiveCashFlowNotificationAction;
import de.sportsbook.activbet.websocket.actions.notification.response.ReceiveMessageNotificationAction;
import de.sportsbook.activbet.websocket.actions.notification.response.ReceivePayOutRequestNotificationAction;
import de.sportsbook.activbet.websocket.actions.notification.response.ReceivePaymentTransactionNotificationAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.List;

@Slf4j
@Controller
public class NotificationRestController extends BaseRestfulController {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    @Qualifier("frontendObjectMapper")
    private ObjectMapper objectMapper;

    /** is there open messages ? */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/notification/REQUEST_MESSAGE_NOTIFICATION")
    public ReceiveMessageNotificationAction requestMessageNotification(@RequestBody RequestAction action) throws ApiErrorException, UnauthorizedUserException {
        MessageNotificationDto dto = notificationService.requestMessageNotification();
        ReceiveMessageNotificationAction answer = new ReceiveMessageNotificationAction();
        answer.setNotification(dto);
        answer.setActionId(action.getActionId());

        return answer;
    }
    
    /** is there open payout requests ? */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/notification/REQUEST_PAY_OUT_REQUEST_NOTIFICATION")
    public ReceivePayOutRequestNotificationAction requestPayOutRequestNotification(@RequestBody RequestAction action) throws ApiErrorException, UnauthorizedUserException {
        PayOutRequestNotificationDto dto = notificationService.requestPayOutRequestNotification();
        ReceivePayOutRequestNotificationAction answer = new ReceivePayOutRequestNotificationAction();
        answer.setNotification(dto);
        answer.setActionId(action.getActionId());
        return answer;
    }

    /** is there open payout transactions */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/notification/REQUEST_PAYMENT_TRANSACTION_NOTIFICATION")
    public ReceivePaymentTransactionNotificationAction requestPaymentTransactionNotification(@RequestBody RequestAction action) throws ApiErrorException, UnauthorizedUserException {
        PaymentTransactionNotificationDto dto = notificationService.requestPaymentTransactionNotification();
        ReceivePaymentTransactionNotificationAction answer = new ReceivePaymentTransactionNotificationAction();
        answer.setNotification(dto);
        answer.setActionId(action.getActionId());
        return answer;
    }

    /** cashflow notification */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/notification/REQUEST_CASH_FLOW_NOTIFICATION")
    public ReceiveCashFlowNotificationAction requestCashFlowNotification(@RequestBody RequestCashFlowNotificationAction action) throws ApiErrorException, UnauthorizedUserException {
        Session session = SessionHolder.getSafeSession();

        // not null
        CashFlowNotification notification = notificationService.requestCashFlowNotification(session, action.getFrom(), action.getCurrencyId());

        ZonedDateTime lastTicketSubmitAt = null;
        PageWrapper<Ticket> ticketsWrapper = ticketService.requestTickets(session, "", null, "", null, null, null, null, action.getCurrencyId(), 0, 1);
        List<Ticket> tickets = ticketsWrapper.getItems();
        if (!CollectionUtils.isEmpty(tickets)) {
            lastTicketSubmitAt = tickets.get(0).getInsertTime();
        }

        CashFlowNotificationDto dto = new CashFlowNotificationDto(notification, lastTicketSubmitAt);
        dto.setCurrencyId(action.getCurrencyId());

        try {
            String body = objectMapper.writeValueAsString(dto);
            System.out.println(body);
        } catch (Exception e) {
        }

        ReceiveCashFlowNotificationAction answer = new ReceiveCashFlowNotificationAction();
        answer.setNotification(dto);
        answer.setActionId(action.getActionId());
        return answer;
    }

}
