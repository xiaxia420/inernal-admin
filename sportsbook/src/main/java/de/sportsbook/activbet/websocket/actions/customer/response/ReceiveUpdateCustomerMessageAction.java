package de.sportsbook.activbet.websocket.actions.customer.response;

import de.sportsbook.activbet.services.customer.dtos.CustomerMessageDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReceiveUpdateCustomerMessageAction extends Action {
    private CustomerMessageDto message;
}
