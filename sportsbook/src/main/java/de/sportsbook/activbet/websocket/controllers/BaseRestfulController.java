package de.sportsbook.activbet.websocket.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.PayloadException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class BaseRestfulController {

    @Autowired
    private ObjectMapper objectMapper;

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleException(UnauthorizedUserException e) {
        ErrorResponse response = new ErrorResponse();
        response.setType("UNAUTHORIZED_USER_EXCEPTION");
        response.setError(e.getMessage());
        response.setTrace(ExceptionUtils.getStackTrace(e));
        return response;
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleException(ApiErrorException e) {
        ErrorResponse response = new ErrorResponse();
        response.setType("API_ERROR_EXCEPTION");
        response.setError(e.getError());
        response.setTrace(ExceptionUtils.getStackTrace(e));
        response.setApiUrl(e.getRequestUrl());
        response.setApiBody(e.getRequestBody());
        return response;
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleException(Exception e) {
        ErrorResponse response = new ErrorResponse();
        response.setType("INTERNAL_ERROR");
        response.setError(e.getMessage());
        response.setTrace(ExceptionUtils.getStackTrace(e));
        return response;
    }

}
