package de.sportsbook.activbet.websocket.actions.payment.response;

import de.sportsbook.activbet.services.payment.dtos.PaymentProviderDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceivePaymentProvidersAction extends Action {
    private List<PaymentProviderDto> paymentProviders;
}
