package de.sportsbook.activbet.websocket.actions.payment.request;

import de.sportsbook.activbet.api.payment.enums.PaymentDirection;
import de.sportsbook.activbet.api.payment.enums.PaymentTransactionStatus;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestPaymentTransactionAction extends Action {
    private Integer id;
}
