package de.sportsbook.activbet.websocket.actions.user;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ForceLogoutAction extends Action {
    public String type = "user/FORCE_LOGOUT";
}
