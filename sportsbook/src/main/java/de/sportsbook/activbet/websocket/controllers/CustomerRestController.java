package de.sportsbook.activbet.websocket.controllers;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.entities.Customer;
import de.sportsbook.activbet.api.customer.entities.CustomerAccountBooking;
import de.sportsbook.activbet.api.customer.entities.CustomerMessage;
import de.sportsbook.activbet.api.customer.entities.CustomerLog;
import de.sportsbook.activbet.api.customer.enums.CustomerType;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.customer.CustomerService;
import de.sportsbook.activbet.services.customer.dtos.CustomerAccountBookingDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerMessageDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerLogDto;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.utils.LocaleUtils;
import de.sportsbook.activbet.websocket.actions.customer.request.*;
import de.sportsbook.activbet.websocket.actions.customer.response.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class CustomerRestController extends BaseRestfulController {

    @Autowired
    private CustomerService customerService;

    /**
    * get forgot password requests
    * */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_FORGOT_PASSWORD_LOGS")
    public ReceiveResetPasswordLogsAction requestResetPasswordLogs(@RequestBody RequestResetPasswordLogsAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        PageWrapper<CustomerLog> page = customerService.requestCustomerResetPasswordLogs(action.getPageIndex(), action.getPageSize());
        ReceiveResetPasswordLogsAction answer = new ReceiveResetPasswordLogsAction();
        PaginationFilterDto filter = new PaginationFilterDto(page);
        List<CustomerLogDto> dtos = page.getItems().stream().map(CustomerLogDto::new).collect(Collectors.toList());
        answer.setPagination(filter);
        answer.setCustomerLogs(dtos);
        return answer;
    }

    /**
        request search customers
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_SEARCH_CUSTOMERS")
    public ReceiveSearchCustomersAction requestSearchCustomers(@RequestBody RequestSearchCustomersAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        PageWrapper<Customer> page = customerService.requestCustomers(action.getPageIndex(), action.getPageSize(), action.getCompanyId(), action.getKeyWord(), CustomerType.CUSTOMER);

        ReceiveSearchCustomersAction answer = new ReceiveSearchCustomersAction();
        PaginationFilterDto filter = new PaginationFilterDto(page);
        answer.setPagination(filter);

        if (CollectionUtils.isEmpty(page.getItems())) {
            answer.setCustomers(null);
        } else {
            List<CustomerDto> dtos = page.getItems().stream().map(CustomerDto::new).collect(Collectors.toList());
            answer.setCustomers(dtos);
        }
        return answer;
    }

    /**
     * get web cashier list of a company
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_SEARCH_CASHIERS")
    public ReceiveSearchCustomersAction requestCompanyCashiers(@RequestBody RequestSearchCustomersAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        PageWrapper<Customer> page = customerService.requestCustomers(
            action.getPageIndex(), action.getPageSize(), action.getCompanyId(), null, CustomerType.WEB_CASHIER
        );
        ReceiveSearchCustomersAction answer = new ReceiveSearchCustomersAction();
        answer.setActionId(action.getActionId());
        answer.setPagination(new PaginationFilterDto(page));
        answer.setCustomers(page.getItems().stream().map(CustomerDto::new).collect(Collectors.toList()));
        return answer;
    }

    /**
    * get bookings of a customer
    * */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_CUSTOMER_ACCOUNT_BOOKINGS")
    public ReceiveCustomerAccountBookingAction requestCustomerPaymentTransactions(@RequestBody RequestCustomerAccountBookingAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        PageWrapper<CustomerAccountBooking> page = customerService.requestCustomerAccountBookings(
            session,
            action.getPageIndex(),
            action.getPageSize(),
            action.getCustomerId(),
            action.getCustomerUsername(),
            action.getKeyWord(),
            action.getFrom(),
            action.getTo(),
            action.getBookingType(),
            action.getBookingReason(),
            action.getOnlyTickets(),
            action.getOnlyPayments(),
            action.getOrderDescend()
        );

        PaginationFilterDto filter = new PaginationFilterDto(page);
        List<CustomerAccountBookingDto> bookings = page.getItems().stream().map(CustomerAccountBookingDto::new).collect(Collectors.toList());

        ReceiveCustomerAccountBookingAction answer = new ReceiveCustomerAccountBookingAction();
        answer.setActionId(action.getActionId());
        answer.setPagination(filter);
        answer.setAccountBookings(bookings);
        return answer;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_PATCH_CUSTOMER_ACCOUNT")
    public ReceivePatchCustomerAccountAction requestPatchCustomerAccount(@RequestBody RequestPatchCustomerAccountAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        CustomerAccountBookingDto dto = customerService.patchCustomerBooking(action.getCustomerId(), action.getDirection(), action.getValue(), action.getText());
        ReceivePatchCustomerAccountAction answer = new ReceivePatchCustomerAccountAction();
        answer.setActionId(action.getActionId());
        answer.setAccountBooking(dto);
        return answer;
    }

    /**
       request single customer (web cashier, customer)
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_SINGLE_CUSTOMER_BY_ID")
    public ReceiveSearchSingleCustomerAction requestSingleCustomer(@RequestBody RequestSearchSingleCustomerAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        CustomerDto customer = customerService.requestCustomerbyId(action.getCustomerId());
        ReceiveSearchSingleCustomerAction answer = new ReceiveSearchSingleCustomerAction();
        answer.setActionId(action.getActionId());
        answer.setCustomer(customer);

        answer.setRequestTimeStamp(LocaleUtils.getDateTimeForUser(ZonedDateTime.now(ZoneId.of("UTC"))));
        return answer;
    }

    /**
     * request customer messages
     * */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_CUSTOMER_MESSAGES")
    public ReceiveCustomerMessagesAction requestCustomerMessages(@RequestBody RequestCustomerMessagesAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        PageWrapper<CustomerMessage> page = customerService.requestCustomerMessages(
            session, action.getPageIndex(), action.getPageSize(), action.getFrom(), action.getTo(), action.getMessageStatus()
        );

        ReceiveCustomerMessagesAction answer = new ReceiveCustomerMessagesAction();
        PaginationFilterDto pagination = new PaginationFilterDto(page);
        answer.setPagination(pagination);

        List<CustomerMessageDto> messages = page.getItems().stream().map(CustomerMessageDto::new).collect(Collectors.toList());
        answer.setMessages(messages);

        return answer;
    }

    /**
     * set customer message to already read
     * */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_UPDATE_CUSTOMER_MESSAGE")
    public ReceiveUpdateCustomerMessageAction requestChangeCustomerMessage(@RequestBody RequestUpdateCustomerMessageAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        CustomerMessageDto message = customerService.requestUpdateCustomerMessage(
            action.getId()
        );

        ReceiveUpdateCustomerMessageAction answer = new ReceiveUpdateCustomerMessageAction();
        answer.setMessage(message);
        return answer;
    }

    /**
     * close customer message
     * */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/customer/REQUEST_CLOSE_CUSTOMER_MESSAGE")
    public ReceiveUpdateCustomerMessageAction requestCloseCustomerMessage(@RequestBody RequestUpdateCustomerMessageAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        CustomerMessageDto message = customerService.requestCloseCustomerMessage(
                action.getId()
        );

        ReceiveUpdateCustomerMessageAction answer = new ReceiveUpdateCustomerMessageAction();
        answer.setMessage(message);
        return answer;
    }



}