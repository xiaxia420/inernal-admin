package de.sportsbook.activbet.websocket.actions.payment.request;

import de.sportsbook.activbet.api.customer.enums.CustomerAccountBookingReason;
import de.sportsbook.activbet.api.customer.enums.CustomerAccountBookingType;
import de.sportsbook.activbet.api.payment.enums.PaymentDirection;
import de.sportsbook.activbet.api.payment.enums.PaymentTransactionStatus;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestPaymentTransactionsAction extends Action {
    private Integer paymentProviderId;
    private Integer paymentChannelId;
    private Integer customerId;
    private String customerUsername;
    private PaymentDirection direction;
    private PaymentTransactionStatus status;
    private String from;
    private String to;

    private Integer pageIndex;
    private Integer pageSize;
    private Boolean orderDescend;
}
