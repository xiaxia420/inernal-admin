package de.sportsbook.activbet.websocket.actions.company.response;

import de.sportsbook.activbet.api.company.entities.CashierReport;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReceiveCashierReportAction extends Action {
    private Integer companyId;
    private CashierReport cashierReports;
}
