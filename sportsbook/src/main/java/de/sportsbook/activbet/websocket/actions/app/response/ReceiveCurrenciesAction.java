package de.sportsbook.activbet.websocket.actions.app.response;

import de.sportsbook.activbet.api.customer.entities.Currency;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveCurrenciesAction extends Action {
    private List<Currency> currencies;
}
