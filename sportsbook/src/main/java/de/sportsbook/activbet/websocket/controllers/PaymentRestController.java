package de.sportsbook.activbet.websocket.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction.PaymentTransactionPage;
import de.sportsbook.activbet.api.payment.entities.PayOutRequest;
import de.sportsbook.activbet.api.payment.entities.PayOutRequest.PayOutRequestPage;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.payment.PaymentService;
import de.sportsbook.activbet.services.payment.dtos.PaymentChannelDto;
import de.sportsbook.activbet.services.payment.dtos.PaymentProviderDto;
import de.sportsbook.activbet.services.payment.dtos.PaymentTransactionDto;
import de.sportsbook.activbet.services.payment.dtos.PayOutRequestDto;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.websocket.actions.RequestAction;
import de.sportsbook.activbet.websocket.actions.payment.request.RequestPaymentTransactionAction;
import de.sportsbook.activbet.websocket.actions.payment.request.RequestPaymentTransactionsAction;
import de.sportsbook.activbet.websocket.actions.payment.request.RequestPayOutRequestsAction;
import de.sportsbook.activbet.websocket.actions.payment.request.RequestUpdatePayoutRequestAction;
import de.sportsbook.activbet.websocket.actions.payment.response.*;
import de.sportsbook.activbet.websocket.actions.ticket.response.ReceiveUpdateTicketStatisticFlagAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class PaymentRestController extends BaseRestfulController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    @Qualifier("frontendObjectMapper")
    private ObjectMapper objectMapper;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_PAYMENT_PROVIDERS")
    public ReceivePaymentProvidersAction requestPaymentProviderList(@RequestBody RequestAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        List<PaymentProviderDto> providers = paymentService.requestPaymentProviderList();
        ReceivePaymentProvidersAction answer = new ReceivePaymentProvidersAction();
        answer.setActionId(action.getActionId());
        answer.setPaymentProviders(providers);
        return answer;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_PAYMENT_CHANNELS")
    public ReceivePaymentChannelsAction requestPaymentChannelList(@RequestBody RequestAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        List<PaymentChannelDto> dtos = paymentService.requestPaymentChannels();
        ReceivePaymentChannelsAction answer = new ReceivePaymentChannelsAction();
        answer.setActionId(action.getActionId());
        answer.setPaymentChannels(dtos);
        return answer;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_PAYMENT_TRANSACTIONS")
    public ReceivePaymentTransactionsAction requestPaymentTransactions(@RequestBody RequestPaymentTransactionsAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();

        PaymentTransactionPage page = paymentService.requestPaymentTransactions(
            session,
            action.getPaymentProviderId(),
            action.getPaymentChannelId(),
            action.getCustomerId(),
            action.getCustomerUsername(),
            action.getDirection(),
            action.getStatus(),
            action.getFrom(),
            action.getTo(),
            action.getPageIndex(), action.getPageSize(), action.getOrderDescend()
        );

        ReceivePaymentTransactionsAction answer = new ReceivePaymentTransactionsAction();
        answer.setActionId(action.getActionId());
        PaginationFilterDto pagination = new PaginationFilterDto(page);
        answer.setPagination(pagination);
        List<PaymentTransactionDto> dtos = page.getItems().stream().map(PaymentTransactionDto::new).collect(Collectors.toList());

        /*
        try {
            String str = objectMapper.writeValueAsString(dtos);
            System.out.println(str);
        } catch (Exception e) {
        }
         */

        answer.setPaymentTransactions(dtos);
        return answer;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_PAY_OUT_REQUESTS")
    public ReceivePayOutRequestsAction requestPayOutRequests(@RequestBody RequestPayOutRequestsAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();

        PayOutRequestPage page = paymentService.requestPayOutRequests(
            session, action.getCompanyId(), action.getCustomerId(), action.getCustomerUsername(),
            action.getPayOutUserId(), action.getStatus(), action.getFlag(), action.getKeyWord(), action.getOrderDescending(),
            action.getFrom(), action.getTo(),
            action.getPageIndex(), action.getPageSize()
        );

        ReceivePayOutRequestsAction answer = new ReceivePayOutRequestsAction();
        answer.setActionId(action.getActionId());
        PaginationFilterDto pagination = new PaginationFilterDto(page);
        answer.setPagination(pagination);
        List<PayOutRequestDto> dtos = page.getItems().stream().map(PayOutRequestDto::new).collect(Collectors.toList());
        answer.setPayOutRequests(dtos);
        return answer;
    }

    /**
        allow payment transaction
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_ACCEPT_PAYMENT_TRANSACTION")
    public ReceivePaymentTransactionAction requestAcceptPaymentTransaction(@RequestBody RequestPaymentTransactionAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        PaymentTransactionDto dto = paymentService.requestHandlePaymentTransaction(session, action.getId(), true);
        ReceivePaymentTransactionAction answer = new ReceivePaymentTransactionAction();
        answer.setActionId(action.getActionId());
        answer.setPaymentTransaction(dto);
        return answer;
    }

    /**
     * deny payment transaction
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_DENY_PAYMENT_TRANSACTION")
    public ReceivePaymentTransactionAction requestDenyPaymentTransaction(@RequestBody RequestPaymentTransactionAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        PaymentTransactionDto dto = paymentService.requestHandlePaymentTransaction(session, action.getId(), false);
        ReceivePaymentTransactionAction answer = new ReceivePaymentTransactionAction();
        answer.setActionId(action.getActionId());
        answer.setPaymentTransaction(dto);
        return answer;
    }

    /**
     * sync payment transaction
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_SYNC_PAYMENT_TRANSACTION")
    public ReceivePaymentTransactionAction requestSyncPaymentTransaction(@RequestBody RequestPaymentTransactionAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        PaymentTransactionDto dto = paymentService.requestSyncPaymentTransaction(action.getId());
        ReceivePaymentTransactionAction answer = new ReceivePaymentTransactionAction();
        answer.setActionId(action.getActionId());
        answer.setPaymentTransaction(dto);
        return answer;
    }

    /**
     * allow payout request
     * */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_ACCEPT_PAYOUT_REQUEST")
    public ReceiveUpdatePayoutRequestAction requestAcceptPayoutRequest(@RequestBody RequestUpdatePayoutRequestAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        paymentService.requestHandlePayOutRequest(action.getId(), true);
        ReceiveUpdatePayoutRequestAction answer = new ReceiveUpdatePayoutRequestAction();
        answer.setActionId(action.getActionId());
        return answer;
    }

    /**
    *  deny payout request
    * */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/payment/REQUEST_DENY_PAYOUT_REQUEST")
    public ReceiveUpdatePayoutRequestAction requestDenyPayoutRequest(@RequestBody RequestUpdatePayoutRequestAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        paymentService.requestHandlePayOutRequest(action.getId(), false);

        ReceiveUpdatePayoutRequestAction answer = new ReceiveUpdatePayoutRequestAction();
        answer.setActionId(action.getActionId());
        return answer;
    }

}