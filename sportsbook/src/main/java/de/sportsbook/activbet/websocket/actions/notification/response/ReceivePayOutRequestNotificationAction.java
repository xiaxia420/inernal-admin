package de.sportsbook.activbet.websocket.actions.notification.response;

import de.sportsbook.activbet.services.notifications.dtos.MessageNotificationDto;
import de.sportsbook.activbet.services.notifications.dtos.PayOutRequestNotificationDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReceivePayOutRequestNotificationAction extends Action {
    private PayOutRequestNotificationDto notification;
}
