package de.sportsbook.activbet.websocket.actions.company.response;

import de.sportsbook.activbet.api.company.entities.Company;
import de.sportsbook.activbet.services.company.dtos.CompanyDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveCompanyTreeAction extends Action {
    private List<CompanyDto> companies;
}
