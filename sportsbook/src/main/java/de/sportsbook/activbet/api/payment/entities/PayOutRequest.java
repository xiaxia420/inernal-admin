package de.sportsbook.activbet.api.payment.entities;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.entities.ActionCustomer;
import de.sportsbook.activbet.api.payment.enums.PayOutRequestFlag;
import de.sportsbook.activbet.api.payment.enums.PaymentDirection;
import de.sportsbook.activbet.api.payment.enums.PaymentTransactionStatus;
import de.sportsbook.activbet.api.payment.enums.PayOutRequestStatus;
import de.sportsbook.activbet.api.system.entities.SystemClient;
import de.sportsbook.activbet.api.system.entities.TechnicalData;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class PayOutRequest {
    private BigDecimal value;
    private String pinCode;
    private ZonedDateTime expireTime;
    private PayOutRequestStatus payOutRequestStatus;
    private PayOutRequestFlag payOutRequestFlag;
    private TechnicalData technicalData;
    private Integer customerId;
    private ActionCustomer customer;
    private Integer clientId;
    private SystemClient client;

    public static class PayOutRequestPage extends PageWrapper<PayOutRequest> {
    }

}