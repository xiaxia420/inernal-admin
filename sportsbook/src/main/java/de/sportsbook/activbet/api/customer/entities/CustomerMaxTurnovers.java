package de.sportsbook.activbet.api.customer.entities;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CustomerMaxTurnovers {
    private BigDecimal perDay;
    private BigDecimal perWeek;
    private BigDecimal perMonth;
}
