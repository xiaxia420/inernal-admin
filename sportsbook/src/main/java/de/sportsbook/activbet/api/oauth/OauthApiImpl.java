package de.sportsbook.activbet.api.oauth;

import de.sportsbook.activbet.api.Api;
import de.sportsbook.activbet.configuration.OauthProperties;
import de.sportsbook.activbet.api.oauth.enums.LogoutReason;
import lombok.Builder;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Base64;

@Service
public class OauthApiImpl extends Api implements OauthApi {

    @Autowired
    private OauthProperties oAuthProperties;

    private UriComponentsBuilder oauthManagementUriBuilder() {
        return UriComponentsBuilder.fromHttpUrl(oAuthProperties.getManagementBaseUrl());
    }

    @Override
    protected HttpHeaders getHttpHeaders() {
        Base64.Encoder encoder = Base64.getEncoder();
        HttpHeaders headers = new HttpHeaders();
        String authorization = encoder.encodeToString((oAuthProperties.getClientId() + ":" + oAuthProperties.getClientSecret()).getBytes());
        headers.add("Authorization", "Basic " + authorization);
        return headers;
    }

    @Override
    public void postLogout(String userId, String remoteAddress, String userAgent, LogoutReason logoutReason) {
        post(this.oauthManagementUriBuilder().pathSegment("user", "logout").build(),
                LogoutLogDto.builder().userId(userId).remoteAddress(remoteAddress).userAgent(userAgent).logoutReason(logoutReason).build(),
                String.class);
    }

    @Getter
    @Builder
    public static class LogoutLogDto {
        private String userId;
        private String remoteAddress;
        private String userAgent;
        private LogoutReason logoutReason;
    }

}
