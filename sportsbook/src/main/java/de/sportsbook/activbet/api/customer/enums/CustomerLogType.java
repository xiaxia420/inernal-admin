package de.sportsbook.activbet.api.customer.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CustomerLogType implements FlagEnum {

    ACTIVATION(0),
    ACTIVATION_REFRESH(1),
    ACTIVATION_CONFIRMATION(2),
    INSERT_BY_USER(5),
    INSERT_BY_CASHIER(6),
    UPDATE(10),
    UPDATE_BY_USER(11),
    RESET_PASSWORD(15),
    RESET_PASSWORD_CONFIRMATION(16),
    CHANGE_PASSWORD(17),
    CHANGE_PASSWORD_CONFIRMATION(18),
    LOGIN(20),
    LOGOUT(21),
    SESSION_EXPIRED(22),
    RRFRESH_TOKEN(25);

    private final int flag;
}
