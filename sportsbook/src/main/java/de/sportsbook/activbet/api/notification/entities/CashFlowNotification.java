package de.sportsbook.activbet.api.notification.entities;

import de.sportsbook.activbet.api.company.entities.CashFlow;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class CashFlowNotification {

    private CashFlowElement lastOnlinePayIn;
    private CashFlowElement lastOnlinePayOut;
    private CashFlowElement lastShopPayIn;
    private CashFlowElement lastShopPayOut;
    private CashFlowElement lastAnonymousTicketPayIn;
    private CashFlowElement lastAnonymousTicketPayOut;

    private BigDecimal onlinePayInByOnlineCustomer;
    private BigDecimal onlinePayInByShopCustomer;
    private BigDecimal onlinePayIn;
    private BigDecimal shopPayInByOnlineCustomer;
    private BigDecimal shopPayInByShopCustomer;
    private BigDecimal shopPayIn;
    private BigDecimal anonymousTicketPayIn;
    private BigDecimal totalShopPayIn;
    private BigDecimal totalPayIn;
    private BigDecimal onlinePayOutByOnlineCustomer;
    private BigDecimal onlinePayOutByShopCustomer;
    private BigDecimal onlinePayOut;
    private BigDecimal shopPayOutByOnlineCustomer;
    private BigDecimal shopPayOutByShopCustomer;
    private BigDecimal shopPayOut;
    private BigDecimal anonymousTicketPayOut;
    private BigDecimal totalShopPayOut;
    private BigDecimal totalPayOut;
    private BigDecimal onlineBalanceByOnlineCustomer;
    private BigDecimal onlineBalanceByShopCustomer;
    private BigDecimal onlineBalance;
    private BigDecimal shopBalanceByOnlineCustomer;
    private BigDecimal shopBalanceByShopCustomer;
    private BigDecimal shopBalance;
    private BigDecimal anonymousTicketBalance;
    private BigDecimal totalShopBalance;
    private BigDecimal totalBalance;
    private BigDecimal totalCustomerAccountBalance;
}
