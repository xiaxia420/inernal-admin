package de.sportsbook.activbet.api.payment.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PaymentChannelPayMethod implements FlagEnum {
    BANK(0),
    SOFORT(1),
    GIROPAY(2),
    PAYPAL(3),
    CREDITCARD(10),
    Mastercard(11),
    VISA(12),
    PAYSAFECARD(21),
    SKRILL(22),
    USSD(31),
    VISA_QR(32);

    private final int flag;
}
