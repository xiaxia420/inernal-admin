package de.sportsbook.activbet.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Base64;
import java.util.Collections;

public class ClientApi extends Api {

    protected ApiMediaType getAccept() {
        return new ApiMediaType("customer", "v10");
    }

    @Override
    protected HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();

        String auth = "Basic " + Base64.getMimeEncoder()
                .encodeToString((configuration.getUser() + ":" +
                        configuration.getPassword()).getBytes());

        headers.add("Authorization", auth);
        headers.setAccept(Collections.singletonList(getAccept()));

        return headers;
    }

    @Autowired
    protected ApiProperties configuration;

    protected UriComponentsBuilder clientApiUriBuilder() {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUrl());
        uriBuilder.queryParam("api-version", "1.0");
        return uriBuilder;
    }
}
