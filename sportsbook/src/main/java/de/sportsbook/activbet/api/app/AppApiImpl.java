package de.sportsbook.activbet.api.app;

import de.sportsbook.activbet.api.BearerApi;
import de.sportsbook.activbet.api.app.entities.Language;
import de.sportsbook.activbet.api.app.entities.Language.LanguageList;
import de.sportsbook.activbet.api.customer.entities.Currency;
import de.sportsbook.activbet.api.customer.entities.Currency.CurrencyList;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
@Slf4j
public class AppApiImpl extends BearerApi implements AppApi {

    @Override
    public List<Language> getLanguages() throws UnauthorizedUserException, ApiErrorException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("languages", "list");
        try {
            return get(uri.build(), LanguageList.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get.languages_list", e);
            throw new ApiErrorException("get.languages_list", e, uri.toUriString(), "");
        }
    }

    @Override
    public List<Currency> getCurrencies() throws UnauthorizedUserException, ApiErrorException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("currencies", "list");
        try {
            return get(uri.build(), CurrencyList.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /currencies/list", e);
            throw new ApiErrorException("get /currencies/list", e, uri.toUriString(), "");
        }
    }
}