package de.sportsbook.activbet.api.payment;

import de.sportsbook.activbet.api.BearerApi;
import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.payment.entities.PaymentChannel;
import de.sportsbook.activbet.api.payment.entities.PaymentProvider;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction.PaymentTransactionPage;
import de.sportsbook.activbet.api.payment.entities.PayOutRequest.PayOutRequestPage;
import de.sportsbook.activbet.api.payment.enums.*;
import de.sportsbook.activbet.api.system.entities.Browser;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

@Slf4j
@Service
public class PaymentApiImpl extends BearerApi implements PaymentApi {

    /**
     *  get all payment providers Amole, HelloCash
     *  */
    @Override
    public PageWrapper<PaymentProvider> getPaymentProviders(
        @NonNull Integer pageIndex, @NonNull Integer pageSize, String keyWord
    ) throws UnauthorizedUserException, ApiErrorException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("paymentProviders");
        uri.queryParam("pageIndex", 0);
        uri.queryParam("pageSize", 1000);
        if (!StringUtils.isEmpty(keyWord)) {
            uri.queryParam("search", keyWord);
        }

        try {
            PageWrapper<PaymentProvider> page = get(uri.build(), PaymentProvider.PaymentProviderPage.class);
            return page;
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /paymentProviders/list", e);
            throw new ApiErrorException("get /paymentProviders/list", e, uri.toUriString(), "");
        }
    }

    /**
     *  get all payment channels
     *  */
    @Override
    public PageWrapper<PaymentChannel> getPaymentChannels(
            @NonNull Integer pageIndex, @NonNull Integer pageSize,
            Integer paymentProviderId, PaymentChannelFlag paymentChannelFlag
    ) throws UnauthorizedUserException, ApiErrorException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("paymentChannels");

        if (Objects.nonNull(paymentProviderId)) {
            uri.queryParam("paymentProviderId", paymentProviderId);
        }

        if (Objects.nonNull(paymentChannelFlag)) {
            uri.queryParam("paymentChannelFlag", paymentChannelFlag.getFlag());
        }

        uri.queryParam("pageIndex", pageIndex);
        uri.queryParam("pageSize", pageSize);

        try {
            return get(uri.build(), PaymentChannel.PaymentChannelPage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /paymentChannels/list", e);
            throw new ApiErrorException("get /paymentChannels", e, uri.toUriString(), "");
        }
    }

    /**
        get payment transactions
     */
    @Override
    public PaymentTransactionPage getPaymentTransactions(
        @NonNull Integer pageIndex, @NonNull Integer pageSize,
        Integer paymentProviderId,
        Integer paymentChannelId,
        Integer customerId,
        PaymentDirection direction,
        PaymentTransactionStatus status,
        Boolean isNotStatus,
        String from,
        String to,
        Boolean orderDescend
    ) throws ApiErrorException, UnauthorizedUserException {

        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("paymentTransactions");
        uri.queryParam("pageIndex", pageIndex);
        uri.queryParam("pageSize", pageSize);

        if (Objects.nonNull(paymentProviderId)) {
            uri.queryParam("paymentProviderId", paymentProviderId);
        }

        if (Objects.nonNull(paymentChannelId)) {
            uri.queryParam("paymentChannelId", paymentChannelId);
        }

        if (Objects.nonNull(customerId)) {
            uri.queryParam("customerId", customerId);
        }

        if (Objects.nonNull(direction)) {
            uri.queryParam("direction", direction.getFlag());
        }

        if (Objects.nonNull(status)) {
            uri.queryParam("status", status.getFlag());
        }

        if (Objects.nonNull(isNotStatus)) {
            uri.queryParam("isNotStatus", isNotStatus);
        }

        if (!StringUtils.isEmpty(from)) {
            uri.queryParam("from", from);
        }

        if (!StringUtils.isEmpty(to)) {
            uri.queryParam("until", to);
        }

        if (Objects.nonNull(orderDescend)) {
            uri.queryParam("orderDescend", orderDescend);
        }

        try {
            return get(uri.build(), PaymentTransaction.PaymentTransactionPage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch(Exception e) {
            log.error("get paymentTransactions", e);
            throw new ApiErrorException("get paymentTransactions", e, uri.toUriString(), "");
        }
    }

    /**
    * get pay out requjests
    * */
    @Override
    public PayOutRequestPage getPayOutRequests(
            @NonNull Integer pageIndex, @NonNull Integer pageSize,
            Integer companyId, Integer customerId, Integer payOutUserId,
            PayOutRequestStatus status, PayOutRequestFlag flag, String keyWord,
            String from, String to,
            Boolean orderDescending
    ) throws ApiErrorException, UnauthorizedUserException {

        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("PayOutRequests");
        uri.queryParam("pageIndex", pageIndex);
        uri.queryParam("pageSize", pageSize);

        if (Objects.nonNull(companyId)) {
            uri.queryParam("companyId", companyId);
        }

        if (Objects.nonNull(customerId)) {
            uri.queryParam("customerId", customerId);
        }

        if (Objects.nonNull(payOutUserId)) {
            uri.queryParam("payOutUserId", payOutUserId);
        }

        if (Objects.nonNull(status)) {
            uri.queryParam("status", status.getFlag());
        }

        if (Objects.nonNull(flag)) {
            uri.queryParam("flag", flag.getFlag());
        }

        if (!StringUtils.isEmpty(keyWord)) {
            uri.queryParam("search", keyWord);
        }

        if (!StringUtils.isEmpty(from)) {
            uri.queryParam("from", from);
        }

        if (!StringUtils.isEmpty(to)) {
            uri.queryParam("to", to);
        }

        if (Objects.nonNull(orderDescending)) {
            uri.queryParam("orderDescending", orderDescending);
        }

        try {
            return get(uri.build(), PayOutRequestPage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch(Exception e) {
            log.error("get PayOutRequests", e);
            throw new ApiErrorException("get PayOutRequests", e, uri.toUriString(), "");
        }
    }

    @Override
    public PaymentTransaction putAcceptPaymentTransaction(Integer id, String remoteAddress, String userAgent)
        throws ApiErrorException, UnauthorizedUserException
    {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("paymentTransactions", String.valueOf(id), "accept");

        Browser browser = new Browser();
        browser.setUserAgent(userAgent);
        browser.setRemoteAddress(remoteAddress);

        try {
            return put(uri.build(), browser, PaymentTransaction.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch(Exception e) {
            log.error("put /paymentTransactions/" + id + "/accept", e);
            throw new ApiErrorException("put /paymentTransactions/" + id + "/accept", e, uri.toUriString(), "");
        }
    }

    @Override
    public PaymentTransaction putDeclinePaymentTransaction(Integer id, String remoteAddress, String userAgent)
        throws ApiErrorException, UnauthorizedUserException
    {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("paymentTransactions", String.valueOf(id), "decline");

        Browser browser = new Browser();
        browser.setUserAgent(userAgent);
        browser.setRemoteAddress(remoteAddress);

        try {
            return put(uri.build(), browser, PaymentTransaction.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch(Exception e) {
            log.error("put /paymentTransactions/" + id + "/decline", e);
            throw new ApiErrorException("put /paymentTransactions/" + id + "/decline", e, uri.toUriString(), "");
        }
    }

    @Override
    public PaymentTransaction putSyncPaymentTransaction(Integer id)
            throws ApiErrorException, UnauthorizedUserException
    {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("paymentTransactions", String.valueOf(id), "synchronize");

        try {
            return put(uri.build(), null, PaymentTransaction.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch(Exception e) {
            log.error("put /paymentTransactions/" + id + "/synchronize", e);
            throw new ApiErrorException("put /paymentTransactions/" + id + "/synchronize", e, uri.toUriString(), "");
        }
    }

    /**
     * allow payout request in shop
     * */
    @Override
    public void putAcceptPayOutRequest(Integer id) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("PayOutRequests", String.valueOf(id));
        try {
            put(uri.build(), null, Object.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch(Exception e) {
            log.error("put /PayOutRequests/" + id, e);
            throw new ApiErrorException("put /PayOutRequests/" + id, e, uri.toUriString(), "");
        }
    }

    /**
     * deny payout request
     * */
    @Override
    public void deleteForDenyPayOutRequest(Integer id) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("PayOutRequests", String.valueOf(id));
        try {
            delete(uri.build(), Object.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch(Exception e) {
            log.error("delete /PayOutRequests/" + id, e);
            throw new ApiErrorException("delete /PayOutRequests/" + id, e, uri.toUriString(), "");
        }
    }

}
