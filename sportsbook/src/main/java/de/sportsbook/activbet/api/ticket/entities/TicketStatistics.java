package de.sportsbook.activbet.api.ticket.entities;

import lombok.Data;
import sun.security.krb5.internal.Ticket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;

@Data
public class TicketStatistics {
    private Integer companyId;
    private String companyShortcut;
    private String companyName1;
    private String companyName2;
    private String currencyIso;
    private Integer currencyDecimalDigits;
    private Integer count;
    private BigDecimal payIn;
    private BigDecimal stakeFee;
    private BigDecimal stake;
    private Integer openCount;
    private BigDecimal openPayIn;
    private BigDecimal openStakeFee;
    private BigDecimal openStake;
    private BigDecimal openWin;
    private BigDecimal openWinFee;
    private BigDecimal openPayOut;
    private Integer cancelledCount;
    private BigDecimal cancelledPayIn;
    private BigDecimal cancelledStakeFee;
    private BigDecimal cancelledStake;
    private Integer evaluatedCount;
    private BigDecimal evaluatedPayIn;
    private BigDecimal evaluatedStakeFee;
    private BigDecimal evaluatedStake;
    private BigDecimal possibleWin;
    private BigDecimal possibleWinFee;
    private BigDecimal possiblePayOut;
    private BigDecimal win;
    private BigDecimal winFee;
    private BigDecimal payOut;
    private BigDecimal revenue;
    private BigDecimal revenuePercent;
    private BigDecimal cashFlow;
    private BigDecimal cashFlowPercent;

    public void addNext(TicketStatistics next) {

        if (Objects.isNull(next)) {
            return;
        }

        // count
        count = Objects.isNull(count) ? 0 : count;
        this.count = this.count + next.count;

        // payIn
        payIn = Objects.isNull(payIn) ? new BigDecimal(0) : payIn;
        this.payIn = this.payIn.add(next.getPayIn());

        // stakeFee
        stakeFee = Objects.isNull(stakeFee) ? new BigDecimal(0) : stakeFee;
        this.stakeFee = stakeFee.add(next.stakeFee);

        // stake
        stake = Objects.isNull(stake) ? new BigDecimal(0) : stake;
        this.stake = stake.add(next.stake);

        // openCount
        openCount = Objects.isNull(openCount) ? 0: openCount;
        this.openCount = openCount + next.getOpenCount();

        //openPayIn
        openPayIn = Objects.isNull(openPayIn) ? new BigDecimal(0) : openPayIn;
        openPayIn = openPayIn.add(next.getOpenPayIn());

        //openStakeFee;
        openStakeFee = Objects.isNull(openStakeFee) ? new BigDecimal(0) : openStakeFee;
        openStakeFee = openStakeFee.add(next.getOpenStakeFee());

        //openStake;
        openStake = Objects.isNull(openStake) ? new BigDecimal(0) : openStake;
        openStake = openStake.add(next.getOpenStake());

        //openWin;
        openWin = Objects.isNull(openWin) ? new BigDecimal(0) : openWin;
        openWin = openWin.add(next.getOpenWin());

        //openWinFee;
        openWinFee = Objects.isNull(openWinFee) ? new BigDecimal(0) : openWinFee;
        openWinFee = openWinFee.add(next.getOpenWinFee());

        //openPayOut;
        openPayOut = Objects.isNull(openPayOut) ? new BigDecimal(0) : openPayOut;
        openPayOut = openPayOut.add(next.getOpenPayOut());

        // cancelledCount
        cancelledCount = Objects.isNull(cancelledCount) ? 0 : cancelledCount;
        this.cancelledCount = this.cancelledCount + next.getCancelledCount();

        // cancelledPayIn;
        cancelledPayIn = Objects.isNull(cancelledPayIn) ? new BigDecimal(0) : cancelledPayIn;
        cancelledPayIn = cancelledPayIn.add(next.getCancelledPayIn());

        // cancelledStakeFee;
        cancelledStakeFee = Objects.isNull(cancelledStakeFee) ? new BigDecimal(0) : cancelledStakeFee;
        cancelledStakeFee = cancelledStakeFee.add(next.getCancelledStakeFee());

        // cancelledStake;
        cancelledStake = Objects.isNull(cancelledStake) ? new BigDecimal(0) : cancelledStake;
        cancelledStake = cancelledStake.add(next.getCancelledStake());

        // evaluatedCount
        evaluatedCount = Objects.isNull(evaluatedCount) ? 0: evaluatedCount;
        this.evaluatedCount = this.evaluatedCount + next.getEvaluatedCount();

        // evaluatedPayIn;
        evaluatedPayIn = Objects.isNull(evaluatedPayIn) ? new BigDecimal(0) : evaluatedPayIn;
        evaluatedPayIn = evaluatedPayIn.add(next.getEvaluatedPayIn());

        // evaluatedStakeFee;
        evaluatedStakeFee = Objects.isNull(evaluatedStakeFee) ? new BigDecimal(0) : evaluatedStakeFee;
        evaluatedStakeFee = evaluatedStakeFee.add(next.getEvaluatedStakeFee());

        // evaluatedStake;
        evaluatedStake = Objects.isNull(evaluatedStake) ? new BigDecimal(0) : evaluatedStake;
        evaluatedStake = evaluatedStake.add(next.getEvaluatedStake());

        // possibleWin;
        possibleWin = Objects.isNull(possibleWin) ? new BigDecimal(0) : possibleWin;
        possibleWin = possibleWin.add(next.getPossibleWin());

        // possibleWinFee;
        possibleWinFee = Objects.isNull(possibleWinFee) ? new BigDecimal(0) : possibleWinFee;
        possibleWinFee = possibleWinFee.add(next.getPossibleWinFee());

        // possiblePayOut;
        possiblePayOut = Objects.isNull(possiblePayOut) ? new BigDecimal(0) : possiblePayOut;
        possiblePayOut = possiblePayOut.add(next.getPossiblePayOut());

        // win;
        win = Objects.isNull(win) ? new BigDecimal(0) : win;
        win = win.add(next.getWin());

        // winFee;
        winFee = Objects.isNull(winFee) ? new BigDecimal(0) : winFee;
        winFee = winFee.add(next.getWinFee());

        // payOut;
        payOut = Objects.isNull(payOut) ? new BigDecimal(0) : payOut;
        payOut = payOut.add(next.getPayOut());

        // revenue;
        revenue = Objects.isNull(revenue) ? new BigDecimal(0) : revenue;
        revenue = revenue.add(next.getRevenue());

        // cashFlow;
        cashFlow = Objects.isNull(cashFlow) ? new BigDecimal(0) : cashFlow;
        cashFlow = cashFlow.add(next.getCashFlow());
    }

    public static class TicketStatisticsList extends ArrayList<TicketStatistics> {
    }
}
