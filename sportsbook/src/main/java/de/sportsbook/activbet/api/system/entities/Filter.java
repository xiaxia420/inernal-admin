package de.sportsbook.activbet.api.system.entities;

import de.sportsbook.activbet.api.system.enums.FilterType;
import lombok.Data;

@Data
public class Filter {
    private FilterType type;
    private String property;
    private Boolean isNot;
    private Object value;
    //private
}
