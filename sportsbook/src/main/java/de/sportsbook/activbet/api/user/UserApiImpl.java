package de.sportsbook.activbet.api.user;

import de.sportsbook.activbet.api.BearerApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@Slf4j
public class UserApiImpl extends BearerApi implements UserApi {

    @Override
    public User getMe() {
        //try {

        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("myaccount");
        User user = get(uri.build(), User.class);
        return user;

        /*
        } catch (AlreadyLoggedException e) {
            Throwable cause = e.getCause();
            if (cause instanceof HttpStatusCodeException) {
                // 401 unauthorized
                HttpStatusCodeException codeException = (HttpStatusCodeException) cause;
                if (codeException.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                    throw new ApiErrorException("error.get_me.unauthorized", "");
                }
            }
        } catch (Exception e) {
            log.error("error get user me", e);
        }
        throw new ApiErrorException("error.get_me.unknown_error", "");
         */
    }

}