package de.sportsbook.activbet.api.customer.entities;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.enums.CustomerLogType;
import de.sportsbook.activbet.api.system.enums.ClientType;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.EnumSet;

@Data
public class CustomerLog {
    private Integer id;
    private ZonedDateTime insertTime;
    private CustomerLogType userLogType;
    private EnumSet<ClientType> clientType;
    private String remoteAddress;
    private String hostAddress;
    private String userAgent;
    private Boolean successful;
    private String message;
    private String errorMessage;

    private Boolean isDeprecated = false; // set by system
    private String keyWord;

    public static class CustomerLogPage extends PageWrapper<CustomerLog> {
    }
}
