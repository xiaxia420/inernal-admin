package de.sportsbook.activbet.api.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(Include.NON_NULL)
public class User {
    private String name;
    private String email;
    private Integer companyId;
    private Integer currencyId;
    private Integer language;
    private String lastName;
    private List<String> roles;
    private UserRights rights;
}
