package de.sportsbook.activbet.api.customer.enums;

import de.sportsbook.serialization.BitFlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CustomerAdvertisingUsage implements BitFlagEnum {
    NONE(0),
    EMAIL(1 << 0),
    SMS(1 << 1),
    POST(1 << 2),
    PHONE(1 << 3);

    private final int flag;
}
