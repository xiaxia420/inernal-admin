package de.sportsbook.activbet.api.customer.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CustomerType implements FlagEnum {

    NONE(0),
    USER(1),
    CUSTOMER(2),
    WEB_CASHIER(3),
    CASHIER(4),
    CLIENT(5),
    AGENT(9),
    IMPORT_DEVICE(10),
    EXPORT_DEVICE(11),
    INFORMATION_DEVICE(12);

    private final int flag;
}
