package de.sportsbook.activbet.api.customer.entities;

import lombok.Data;

@Data
public class CustomerSetting {
    private Integer settingKey;
    private String settingName;
    private String type;
    private String value;
    private String inheritance;
    private String inheritedValue;
    private Integer inheritedId;
    private String inheritedName;
}
