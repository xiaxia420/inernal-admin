package de.sportsbook.activbet.api.payment;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.payment.entities.PaymentChannel;
import de.sportsbook.activbet.api.payment.entities.PaymentProvider;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction.PaymentTransactionPage;
import de.sportsbook.activbet.api.payment.entities.PayOutRequest.PayOutRequestPage;
import de.sportsbook.activbet.api.payment.enums.*;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.NonNull;

public interface PaymentApi {
    PageWrapper<PaymentProvider> getPaymentProviders(
        @NonNull Integer pageIndex, @NonNull Integer pageSize, String keyWord
    ) throws ApiErrorException, UnauthorizedUserException;

    PageWrapper<PaymentChannel> getPaymentChannels(
        @NonNull Integer pageIndex, @NonNull Integer pageSize,
        Integer paymentProviderId, PaymentChannelFlag paymentChannelFlag
    ) throws ApiErrorException, UnauthorizedUserException;

    PaymentTransactionPage getPaymentTransactions(
            @NonNull Integer pageIndex, @NonNull Integer pageSize,
            Integer paymentProviderId,
            Integer paymentChannelId,
            Integer customerId,
            PaymentDirection direction,
            PaymentTransactionStatus status,
            Boolean isNotStatus,
            String from,
            String to,
            Boolean orderDescend
    ) throws ApiErrorException, UnauthorizedUserException;

    PayOutRequestPage getPayOutRequests(
            @NonNull Integer pageIndex, @NonNull Integer pageSize,
            Integer companyId, Integer customerId, Integer payOutUserId,
            PayOutRequestStatus status, PayOutRequestFlag flag, String keyWord,
            String from, String to,
            Boolean orderDescending
    ) throws ApiErrorException, UnauthorizedUserException;

    PaymentTransaction putAcceptPaymentTransaction(Integer id, String remoteAddress, String userAgent)
        throws ApiErrorException, UnauthorizedUserException;

    PaymentTransaction putDeclinePaymentTransaction(Integer id, String remoteAddress, String userAgent)
        throws ApiErrorException, UnauthorizedUserException;

    PaymentTransaction putSyncPaymentTransaction(Integer id) throws ApiErrorException, UnauthorizedUserException;

    void putAcceptPayOutRequest(Integer id) throws ApiErrorException, UnauthorizedUserException;

    void deleteForDenyPayOutRequest(Integer id) throws ApiErrorException, UnauthorizedUserException;
}
