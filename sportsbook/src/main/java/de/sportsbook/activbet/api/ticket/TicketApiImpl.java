package de.sportsbook.activbet.api.ticket;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.sportsbook.activbet.api.BearerApi;
import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.ticket.entities.CashOutAnalytics;
import de.sportsbook.activbet.api.ticket.entities.Ticket;
import de.sportsbook.activbet.api.ticket.entities.Ticket.TicketPage;
import de.sportsbook.activbet.api.ticket.entities.TicketStatistics;
import de.sportsbook.activbet.api.ticket.entities.TicketStatistics.TicketStatisticsList;
import de.sportsbook.activbet.api.ticket.entities.TicketsRequest;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
@Slf4j
public class TicketApiImpl extends BearerApi implements TicketApi {

    @Autowired
    @Qualifier("frontendObjectMapper")
    private ObjectMapper objectMapper;


    /**
    * add delete flag to ticket
    * */
    @Override
    public void putTicketStatisticFlag(String ticketNumber) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("tickets", "statisticFlags", ticketNumber);
        try {
            put(uri.build(), null, String.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("put /tickets/statisticFlags", e);
            throw new ApiErrorException("put /tickets/statisticFlags", e, uri.toUriString(), "");
        }
    }

    /**
    * remove delte flag from ticket
    * */
    @Override
    public void deleteTicketStatisticFlag(String ticketNumber) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("tickets", "statisticFlags", ticketNumber);
        try {
            delete(uri.build(), String.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("put /tickets/statisticFlags", e);
            throw new ApiErrorException("put /tickets/statisticFlags", e, uri.toUriString(), "");
        }
    }

    /**
        request ticket statistics
     */
    @Override
    public List<TicketStatistics> postForTicketsStatistics(TicketsRequest request) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("tickets", "statistics");
        String body = "";
        try {
            body = objectMapper.writeValueAsString(request);
            System.out.println(body);
        } catch (Exception e) {
        }

        try {
            return post(uri.build(), request, TicketStatisticsList.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("post /tickets/statistics", e);
            throw new ApiErrorException("post /tickets/statistics", e, uri.toUriString(), body);
        }
    }

    /*
        request tickets
     */
    @Override
    public PageWrapper<Ticket> postForTickets(
            TicketsRequest request, @NonNull Integer pageIndex, @NonNull Integer pageSize
    ) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("tickets", "search");
        uri.queryParam("pageIndex", pageIndex);
        uri.queryParam("pageSize", pageSize);
        String body = "";
        try {
            body = objectMapper.writeValueAsString(request);
            //System.out.println(body);
        } catch (Exception e) {
        }

        try {
            return post(uri.build(), request, TicketPage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("post /tickets/statistics", e);
            throw new ApiErrorException("post /tickets/search", e, uri.toUriString(), body);
        }
    }

    /*
        delete ticket
     */
    @Override
    public Ticket deleteTicket(Integer id) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("evaluation", "tickets", String.valueOf(id));
        try {
            return delete(uri.build(), Ticket.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("delete /evaluation/tickets/" + id, e);
            throw new ApiErrorException("delete /evaluation/tickets/" + id, e, uri.toUriString(), null);
        }
    }

    /** cash out analytics */
    @Override
    public CashOutAnalytics getCashOutAnalytics(String from, String to) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("tickets", "cashout_analytics");
        uri.queryParam("from", from);
        uri.queryParam("to", to);

        try {
            return get(uri.build(), CashOutAnalytics.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /tickets/cashout_analytics", e);
            throw new ApiErrorException("get /tickets/cashout_analytics", e, uri.toUriString(), null);
        }
    }

}