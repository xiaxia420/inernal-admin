package de.sportsbook.activbet.api.customer.entities;

import lombok.Data;

@Data
public class CustomerAddress {
    private Integer countryId;
    private Integer timeZoneMinutes;
    private String custom;
    private String unitNumber;
    private String postalCode;
    private String sortingCode;
    private String province;
    private String provinceAbbreviation;
    private String locality;
    private String dependentLocality;
    private String doubleDependentLocality;
    private String urbanisation;
    private String village;
    private String parish;
    private String borough;
    private String colony;
    private String administrativeDistrict;
    private String cityDistrict;
    private String streetType;
    private String streetDirection;
    private String dependentStreet;
    private String streetName;
    private String pak;
    private String block;
    private String building;
    private String subBuilding;
    private String buildingNumber;
    private String houseNumber;
    private String apartment;
    private String floor;
    private String location;
}
