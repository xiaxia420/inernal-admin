package de.sportsbook.activbet.api.notification.entities;

import de.sportsbook.activbet.api.customer.entities.Currency;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class CashFlowElement {
    private String info;
    private ZonedDateTime insertTime;
    private BigDecimal amount;
    private Currency currency;
}
