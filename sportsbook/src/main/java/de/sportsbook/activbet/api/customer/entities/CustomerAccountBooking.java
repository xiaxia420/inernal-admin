package de.sportsbook.activbet.api.customer.entities;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.company.entities.Company;
import de.sportsbook.activbet.api.customer.enums.CustomerAccountBookingReason;
import de.sportsbook.activbet.api.customer.enums.CustomerAccountBookingType;
import de.sportsbook.activbet.api.system.entities.Relation;
import de.sportsbook.activbet.api.system.entities.SystemClient;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class CustomerAccountBooking {
    private Integer id;
    private ZonedDateTime insertTime;
    private CustomerAccountBookingReason reason;
    private CustomerAccountBookingType type;
    private String text;
    private BigDecimal value;
    private BigDecimal balance;
    private String externalReferenceId;
    private String externalReferenceType;
    private ActionCustomer insertUser;
    private Company company;
    private SystemClient client;
    private String remoteAddress;
    private String userAgent;
    private Integer currencyId;
    private Relation relation;

    public static class CustomerAccountBookingPage extends PageWrapper<CustomerAccountBooking> {
    }
}
