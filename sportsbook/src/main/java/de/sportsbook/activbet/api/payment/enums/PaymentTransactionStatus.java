package de.sportsbook.activbet.api.payment.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PaymentTransactionStatus implements FlagEnum {
    OPEN(0),
    ACCEPTED(1),
    DECLINED_BY_OPERATOR(3),
    DECLINED_BY_PROVIDER(4),
    PAID(11),
    CANCELLED(90),
    EXPIRED(91),
    ERROR(99);

    private final int flag;
}
