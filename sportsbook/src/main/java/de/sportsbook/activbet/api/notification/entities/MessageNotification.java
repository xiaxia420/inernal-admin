package de.sportsbook.activbet.api.notification.entities;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.ArrayList;

@Data
public class MessageNotification {
    private Integer unreadAnonymous;
    private Integer unreadCustomer;
    private Integer unread;
    private Integer progressedAnonymous;
    private Integer progressedCustomer;
    private Integer progressed;
    private Integer contactsActualMonth;
    private Integer contactsActualWeek;
    private Integer contactsYesterday;
    private Integer contactsToday;
    private ZonedDateTime lastContactInsertTime;
}
