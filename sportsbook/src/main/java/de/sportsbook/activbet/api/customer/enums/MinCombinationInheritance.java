package de.sportsbook.activbet.api.customer.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum MinCombinationInheritance implements FlagEnum {
    NONE(0),
    GLOBAL(1),
    SPORT(2),
    TOURNAMENT(3),
    GAME(4);

    private final int flag;
}
