package de.sportsbook.activbet.api.company;

import de.sportsbook.activbet.api.company.entities.CashFlow;
import de.sportsbook.activbet.api.company.entities.CashierReport;
import de.sportsbook.activbet.api.company.entities.Company;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;

import java.util.List;

public interface CompanyApi {
    List<Company> getCompanies(Integer parentId) throws ApiErrorException, UnauthorizedUserException;
    CashierReport postForCashierReports(Integer companyId, String from, String to) throws ApiErrorException, UnauthorizedUserException;
    //CashFlow getCashFlow(String from, Integer currencyId) throws ApiErrorException, UnauthorizedUserException;
}
