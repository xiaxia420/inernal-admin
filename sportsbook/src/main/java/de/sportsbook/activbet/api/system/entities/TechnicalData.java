package de.sportsbook.activbet.api.system.entities;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class TechnicalData {
    private Integer id;

    private ZonedDateTime insertTime;
    private Integer insertId;
    private String insertName;

    private Integer updateId;
    private ZonedDateTime updateTime;
    private String updateName;
}
