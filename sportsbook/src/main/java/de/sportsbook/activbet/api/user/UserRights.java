package de.sportsbook.activbet.api.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class UserRights {
    private Boolean hasAdministrationAccess;
    private Boolean hasDeveloperAccess;
    private Boolean hasAgentAccess;
}