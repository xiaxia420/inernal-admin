package de.sportsbook.activbet.api.customer.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CustomerMessageStatus implements FlagEnum {
    ALL(0),
    UN_READ(1),
    IN_PROGRESS(2),
    CLOSED(3);

    private final int flag;
}
