package de.sportsbook.activbet.api.customer.entities;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.app.entities.Language;
import de.sportsbook.activbet.api.company.entities.Company;
import de.sportsbook.activbet.api.customer.enums.CustomerAdvertisingUsage;
import de.sportsbook.activbet.api.customer.enums.CustomerType;
import de.sportsbook.activbet.api.system.entities.TechnicalData;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.EnumSet;

@Data
public class Customer {
    private Integer id;
    private Integer failedPasswordAttemptCount;
    private ZonedDateTime failedPasswordAttemptWindowStart;
    private ZonedDateTime lastLockedOutDate;
    private ZonedDateTime lastPasswordChangeDate;
    private CustomerTitle title;
    private Language language;
    private Company company;
    private Currency currency;
    private BigDecimal balance;
    private String insertIp;
    private String updateIp;
    private TechnicalData technicalData;
    private CustomerSettings customerSettings;
    private UserSettings userSettings;
    private CustomerMinCombinations minCombinations;
    private String username;
    private String nfcId;
    private String email;
    private String firstName;
    private String middleName;
    private String lastName;
    private ZonedDateTime birthday;
    private Integer rating;
    private CustomerType userType;
    private Boolean isWatched;
    private EnumSet<CustomerAdvertisingUsage> advertisingUsage;
    private String password;
    private Boolean forceChangePassword;
    private Integer sessionTimeout;
    private Boolean isApproved;
    private Boolean isLockedOut;
    private Integer companyId;
    private Integer titleId;
    private Integer languageId;
    private Integer affiliateId;
    private CustomerStatusInfo statusInfo;
    private CustomerAddress address;
    private CustomerMaxTurnovers customerMaxTurnovers;
    private Boolean isOnline;
    private Boolean acceptTermsAndConditions;
    private Boolean childCompaniesAccessible;
    private Integer roleCount;
    private Integer roleGroupCount;
    private String mobile;

    public static class CustomerList extends ArrayList<Customer> {
    }

    public static class CustomersPage extends PageWrapper<Customer> {

    }
}