package de.sportsbook.activbet.api.ticket;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.ticket.entities.CashOutAnalytics;
import de.sportsbook.activbet.api.ticket.entities.Ticket;
import de.sportsbook.activbet.api.ticket.entities.TicketStatistics;
import de.sportsbook.activbet.api.ticket.entities.TicketsRequest;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.NonNull;

import java.util.List;

public interface TicketApi {
    void putTicketStatisticFlag(String ticketNumber) throws ApiErrorException, UnauthorizedUserException;
    void deleteTicketStatisticFlag(String ticketNumber)throws ApiErrorException, UnauthorizedUserException;
    List<TicketStatistics> postForTicketsStatistics(TicketsRequest request) throws ApiErrorException, UnauthorizedUserException;
    PageWrapper<Ticket> postForTickets(TicketsRequest request, @NonNull Integer pageIndex, @NonNull Integer pageSize) throws ApiErrorException, UnauthorizedUserException;
    Ticket deleteTicket(Integer id) throws ApiErrorException, UnauthorizedUserException;
    CashOutAnalytics getCashOutAnalytics(String from, String to) throws ApiErrorException, UnauthorizedUserException;
}


