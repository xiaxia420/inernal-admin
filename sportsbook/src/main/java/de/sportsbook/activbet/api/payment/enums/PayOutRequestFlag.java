package de.sportsbook.activbet.api.payment.enums;

import de.sportsbook.serialization.BitFlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PayOutRequestFlag implements BitFlagEnum {
    NONE(0),
    NEED_RELEASE(1 << 0);

    private final int flag;
}
