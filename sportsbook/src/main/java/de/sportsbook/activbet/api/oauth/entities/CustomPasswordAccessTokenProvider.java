package de.sportsbook.activbet.api.oauth.entities;

import org.springframework.http.HttpHeaders;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Iterator;
import java.util.List;

/**
 * AccessTokenProvider that injects X-Forwarded-For and X-User-Agent headers for the OAuth Activity logging.
 */
public class CustomPasswordAccessTokenProvider extends ResourceOwnerPasswordAccessTokenProvider {

    private String ipAddress;
    private String userAgent;

    public CustomPasswordAccessTokenProvider(String ipAddress, String userAgent) {
        this.ipAddress = ipAddress;
        this.userAgent = userAgent;
    }

    @Override
    public OAuth2AccessToken refreshAccessToken(OAuth2ProtectedResourceDetails resource,
                                                OAuth2RefreshToken refreshToken, AccessTokenRequest request) throws
                                                                                                             UserRedirectRequiredException,
                                                                                                             OAuth2AccessDeniedException {
        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        form.add("grant_type", "refresh_token");
        form.add("refresh_token", refreshToken.getValue());
        return retrieveToken(request, resource, form, getHeaders());
    }


    @Override
    public OAuth2AccessToken obtainAccessToken(OAuth2ProtectedResourceDetails details,
                                               AccessTokenRequest request) throws UserRedirectRequiredException,
                                                                                  AccessDeniedException {

        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails) details;
        return retrieveToken(request, resource, getParametersForTokenRequest(resource, request), getHeaders());
    }

    /* Copied from ResourceOwnerPasswordAccessTokenProvider */
    private MultiValueMap<String, String> getParametersForTokenRequest(ResourceOwnerPasswordResourceDetails resource,
                                                                       AccessTokenRequest request) {

        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        form.set("grant_type", "password");

        form.set("username", resource.getUsername());
        form.set("password", resource.getPassword());
        form.putAll(request);

        if (resource.isScoped()) {

            StringBuilder builder = new StringBuilder();
            List<String> scope = resource.getScope();

            if (scope != null) {
                Iterator<String> scopeIt = scope.iterator();
                while (scopeIt.hasNext()) {
                    builder.append(scopeIt.next());
                    if (scopeIt.hasNext()) {
                        builder.append(' ');
                    }
                }
            }

            form.set("scope", builder.toString());
        }

        return form;

    }

    public HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Forwarded-For", ipAddress);
        headers.add("X-User-Agent", userAgent);
        return headers;
    }
}
