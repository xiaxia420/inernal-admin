package de.sportsbook.activbet.api.notification;

import de.sportsbook.activbet.api.notification.entities.CashFlowNotification;
import de.sportsbook.activbet.api.notification.entities.MessageNotification;
import de.sportsbook.activbet.api.notification.entities.PayOutRequestNotification;
import de.sportsbook.activbet.api.notification.entities.PaymentTransactionNotification;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;

public interface NotificationApi {
    MessageNotification getMessageNotification() throws ApiErrorException, UnauthorizedUserException;
    PayOutRequestNotification getPayOutRequestNotification() throws ApiErrorException, UnauthorizedUserException;
    PaymentTransactionNotification getPaymentTransactionNotification() throws ApiErrorException, UnauthorizedUserException;
    CashFlowNotification getCashFlowNotification(String from, Integer currencyId) throws ApiErrorException, UnauthorizedUserException;
}
