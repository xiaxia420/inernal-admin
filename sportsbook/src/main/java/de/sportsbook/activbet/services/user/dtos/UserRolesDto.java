package de.sportsbook.activbet.services.user.dtos;

import de.sportsbook.activbet.api.user.User;
import lombok.Data;

import java.util.List;

@Data
public class UserRolesDto {
    private Boolean isSuperUser = false;
    private Boolean allowAdminSportNavigation = false;
    private Boolean allowAdminSportRead = false;
    private Boolean allowAdminRegionRead = false;
    private Boolean allowCategoryRead = false;
    private Boolean allowTournamentRead = false;
    private Boolean allowAdminSeasonRead = false;
    private Boolean allowAdminVenueRead = false;
    private Boolean allowAdminTipModelRead = false;
    private Boolean allowAdminCustomerRead = false;
    private Boolean allowAdminTicket = false;
    private Boolean allowAdminRivalRead = false;
    private Boolean allowAdminCompanyRead = false;
    private Boolean allowAdminUserRead = false;
    private Boolean allowAdminClientRead = false;
    private Boolean allowAdminCurrencyRead = false;
    private Boolean allowAdminTitleRead = false;
    private Boolean allowAdminLanguageRead = false;
    private Boolean allowAdminRolesRead = false;
    private Boolean allowAdminRoleGroupsRead = false;

    public UserRolesDto(User user) {

        List<String> roles = user.getRoles();
        if (roles.contains(UserRoles.SuperUser)) {
            isSuperUser = true;
            allowAdminSportNavigation = true;
            allowAdminSportRead = true;
            allowAdminRegionRead = true;
            allowCategoryRead = true;
            allowTournamentRead = true;
            allowAdminSeasonRead = true;
            allowAdminVenueRead = true;
            allowAdminTipModelRead = true;
            allowAdminCustomerRead = true;
            allowAdminTicket = true;
            allowAdminRivalRead = true;
            allowAdminCompanyRead = true;
            allowAdminUserRead = true;
            allowAdminClientRead = true;
            allowAdminCurrencyRead = true;
            allowAdminTitleRead = true;
            allowAdminLanguageRead = true;
            allowAdminRolesRead = true;
            allowAdminRoleGroupsRead = true;
        } else {
            isSuperUser = false;
            allowAdminSportNavigation = roles.contains(UserRoles.Administration_Sport_Navigation);
            allowAdminSportRead = roles.contains(UserRoles.Administration_Sport_Read);
            allowAdminRegionRead = roles.contains(UserRoles.Administration_Region_Read);
            allowCategoryRead = roles.contains(UserRoles.Administration_Category_Read);
            allowTournamentRead = roles.contains(UserRoles.Administration_Tournament_Read);
            allowAdminSeasonRead = roles.contains(UserRoles.Administration_Season_Read);
            allowAdminVenueRead = roles.contains(UserRoles.Administration_Venue_Read);
            allowAdminTipModelRead = roles.contains(UserRoles.Administration_TipModel_Read);
            allowAdminCustomerRead = roles.contains(UserRoles.Administration_Customer_Read);
            allowAdminTicket = roles.contains(UserRoles.Administration_Ticket);
            allowAdminRivalRead = roles.contains(UserRoles.Administration_Rival_Read);
            allowAdminCompanyRead = roles.contains(UserRoles.Administration_Company_Read);
            allowAdminUserRead = roles.contains(UserRoles.Administration_User_Read);
            allowAdminClientRead = roles.contains(UserRoles.Administration_Client_Read);
            allowAdminCurrencyRead = roles.contains(UserRoles.Administration_Currency_Read);
            allowAdminTitleRead = roles.contains(UserRoles.Administration_Title_Read);
            allowAdminLanguageRead = roles.contains(UserRoles.Administration_Language_Read);
            allowAdminRolesRead = roles.contains(UserRoles.Administration_Roles_Read);
            allowAdminRoleGroupsRead = roles.contains(UserRoles.Administration_RoleGroups_Read);
        }
    }

}
