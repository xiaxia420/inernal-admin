package de.sportsbook.activbet.services.tickets;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.enums.CustomerType;
import de.sportsbook.activbet.api.system.entities.Filter;
import de.sportsbook.activbet.api.system.entities.FromTo;
import de.sportsbook.activbet.api.system.enums.FilterType;
import de.sportsbook.activbet.api.ticket.TicketApi;
import de.sportsbook.activbet.api.ticket.entities.CashOutAnalytics;
import de.sportsbook.activbet.api.ticket.entities.Ticket;
import de.sportsbook.activbet.api.ticket.entities.TicketStatistics;
import de.sportsbook.activbet.api.ticket.entities.TicketsRequest;
import de.sportsbook.activbet.api.ticket.enums.TicketActive;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.tickets.dtos.TicketDto;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TicketService {

    @Autowired
    private TicketApi ticketApi;

    public void setInternRemoveFlag(String ticketNumber) throws ApiErrorException, UnauthorizedUserException {
        ticketApi.putTicketStatisticFlag(ticketNumber);
    }

    public void removeInterRemoveFlag(String ticketNumber) throws ApiErrorException, UnauthorizedUserException {
        ticketApi.deleteTicketStatisticFlag(ticketNumber);
    }

    /**
        get ticket statistics
     */
    public List<TicketStatistics> requestAllTicketStatistics(
        Session session,
        String customerUsername,
        EnumSet<TicketActive> ticketActive,
        String ticketNumber,
        String insertFrom,
        String insertTo,
        BigDecimal stakeFrom,
        BigDecimal stakeTo,
        Integer currencyId
    ) throws ApiErrorException, UnauthorizedUserException {

        TicketsRequest request = this.getTicketsFilters(
            session, customerUsername, ticketActive, ticketNumber,
            insertFrom, insertTo, stakeFrom, stakeTo, currencyId
        );
        List<TicketStatistics> statistics = ticketApi.postForTicketsStatistics(request);

        // sort by companyId
        return statistics.stream().sorted(
            (m1, m2) -> m1.getCompanyId().intValue() - m2.getCompanyId()
        ).collect(Collectors.toList());
    }

    /**
    * get statistics only in shop
    * */
    public List<TicketStatistics> requestShopBasedTicketStatistics(
        Session session,
        String customerUsername,
        EnumSet<TicketActive> ticketActive,
        String ticketNumber,
        String insertFrom,
        String insertTo,
        BigDecimal stakeFrom,
        BigDecimal stakeTo,
        Integer currencyId
    ) throws ApiErrorException, UnauthorizedUserException {

        Filter filter = new Filter();
             filter.setType(FilterType.COMPANY);
        filter.setIsNot(false);
        filter.setProperty("customerUser.userType");
        filter.setValue(CustomerType.WEB_CASHIER.getFlag());

        TicketsRequest request = this.getTicketsFilters(
                session, customerUsername, ticketActive, ticketNumber,
                insertFrom, insertTo, stakeFrom, stakeTo, currencyId
        );
        request.getFilters().add(filter);

        return this.ticketApi.postForTicketsStatistics(request);
    }

    /**
    * get statistics of shop registered user
    * */
    public List<TicketStatistics> requestShopRegisteredTicketStatistics(
        Session session,
        String customerUsername,
        EnumSet<TicketActive> ticketActive,
        String ticketNumber,
        String insertFrom,
        String insertTo,
        BigDecimal stakeFrom,
        BigDecimal stakeTo,
        Integer currencyId
    ) throws ApiErrorException, UnauthorizedUserException {
        Filter filter = new Filter();
        //filter.setType(FilterType.Company);
        filter.setType(FilterType.COMPANY);
        filter.setIsNot(true);
        filter.setProperty("customerUser.userType");
        filter.setValue(CustomerType.WEB_CASHIER.getFlag());

        TicketsRequest request = this.getTicketsFilters(
                session, customerUsername, ticketActive, ticketNumber,
                insertFrom, insertTo, stakeFrom, stakeTo, currencyId
        );
        request.getFilters().add(filter);

        return this.ticketApi.postForTicketsStatistics(request);
    }

    private TicketsRequest getTicketsFilters(
        Session session,
        String customerUsername,
        EnumSet<TicketActive> ticketActive,
        String ticketNumber,
        String insertFrom,
        String insertTo,
        BigDecimal stakeFrom,
        BigDecimal stakeTo,
        Integer currencyId
    ) {
        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        String localFrom = LocaleUtils.getDateTimeForLocal(timezone, insertFrom);
        String localTo = LocaleUtils.getDateTimeForLocal(timezone, insertTo);

        TicketsRequest request = new TicketsRequest();
        request.setCurrencyId(currencyId);

        List<Filter> filters = new ArrayList<>();

        // customer username
        if (!StringUtils.isEmpty(customerUsername)) {
            Filter usernameFilter = new Filter();
            usernameFilter.setIsNot(false);
            //usernameFilter.setType(FilterType.String);
            usernameFilter.setType(FilterType.STRING);
            usernameFilter.setProperty("customerUser.username");
            usernameFilter.setValue(customerUsername);
            filters.add(usernameFilter);
        }

        // insert time
        if (!StringUtils.isEmpty(insertFrom) || !StringUtils.isEmpty(insertTo)) {
            Filter insertTimeFilter = new Filter();
            insertTimeFilter.setIsNot(false);
            //insertTimeFilter.setType(FilterType.DateTimeRange);
            insertTimeFilter.setType(FilterType.DATE_TIME_RANGE);
            FromTo fromTo = new FromTo();
            if (!StringUtils.isEmpty(insertFrom)) {
                fromTo.setFrom(localFrom);
            }
            if (!StringUtils.isEmpty(insertTo)) {
                fromTo.setTo(localTo);
            }
            insertTimeFilter.setProperty("insertTime");
            insertTimeFilter.setValue(fromTo);
            filters.add(insertTimeFilter);
        }

        // ticket active
        if (Objects.nonNull(ticketActive)) {
            Filter activeFilter = new Filter();
            activeFilter.setIsNot(false);
            //activeFilter.setType(FilterType.BitFlagEnum);
            activeFilter.setType(FilterType.BIT_FLAG_ENUM);
            activeFilter.setProperty("ticketActive");
            activeFilter.setValue(ticketActive.stream().collect(Collectors.toList()).get(0).getFlag());
            filters.add(activeFilter);
        }

        // ticket number
        if (!StringUtils.isEmpty(ticketNumber)) {
            Filter numberFilter = new Filter();
            numberFilter.setIsNot(false);
            //numberFilter.setType(FilterType.String);
            numberFilter.setType(FilterType.STRING);
            numberFilter.setProperty("number");
            numberFilter.setValue(ticketNumber);
            filters.add(numberFilter);
        }

        // stake from, to
        if (Objects.nonNull(stakeFrom) || Objects.nonNull(stakeTo)) {
            Filter stakeFilter = new Filter();
            stakeFilter.setIsNot(false);
            //stakeFilter.setType(FilterType.NumberRange);
            stakeFilter.setType(FilterType.NUMBER_RANGE);
            stakeFilter.setProperty("stake");
            FromTo fromTo = new FromTo();
            if (Objects.nonNull(stakeFrom)) {
                fromTo.setFrom(String.valueOf(stakeFrom));
            }
            if (Objects.nonNull(stakeTo)) {
                fromTo.setTo(String.valueOf(stakeTo));
            }
            stakeFilter.setValue(fromTo);
            filters.add(stakeFilter);
        }

        /*
        // without demo
        Filter demoFilter = new Filter();
        demoFilter.setIsNot(true);
        demoFilter.setType(FilterType.STRING);
        demoFilter.setProperty("customerCompany.id");
        demoFilter.setValue(5);
        filters.add(demoFilter);
         */

        request.setFilters(filters);
        return request;
    }

    /**
        tickets
    */
    public PageWrapper<Ticket> requestTickets(
            Session session,
            String customerUsername,
            EnumSet<TicketActive> ticketActive,
            String ticketNumber,
            String insertFrom,
            String insertTo,
            BigDecimal stakeFrom,
            BigDecimal stakeTo,
            Integer currencyId,
            Integer pageIndex,
            Integer pageSize
    ) throws ApiErrorException, UnauthorizedUserException {
        TicketsRequest request = this.getTicketsFilters(
            session,
            customerUsername,
            ticketActive,
            ticketNumber,
            insertFrom,
            insertTo,
            stakeFrom,
            stakeTo,
            currencyId
        );
        return ticketApi.postForTickets(request, pageIndex, pageSize);
    }

    /**
        tickets statistics
     */
    public List<TicketStatistics> requestTicketsStatistics(
        Session session,
        String customerUsername,
        EnumSet<TicketActive> ticketActive,
        String ticketNumber,
        String insertFrom,
        String insertTo,
        BigDecimal stakeFrom,
        BigDecimal stakeTo,
        Integer currencyId
    ) throws ApiErrorException, UnauthorizedUserException {
        TicketsRequest request = this.getTicketsFilters(
                session,
                customerUsername,
                ticketActive,
                ticketNumber,
                insertFrom,
                insertTo,
                stakeFrom,
                stakeTo,
                currencyId
        );
        List<TicketStatistics> statistics = ticketApi.postForTicketsStatistics(request);
        if (CollectionUtils.isEmpty(statistics)) {
            return new ArrayList<>();
        } else {
            return statistics;
        }
    }

    public TicketDto cancelTicket(Integer id) throws ApiErrorException, UnauthorizedUserException {
        Ticket ticket = ticketApi.deleteTicket(id);
        return new TicketDto(ticket);
    }

    /**
     * cash out statistics
     */
    public CashOutAnalytics requestTicketCashOutAnalytics(Session session, String from, String to) throws ApiErrorException, UnauthorizedUserException {
        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        String localFrom = LocaleUtils.getDateTimeForLocal(timezone, from);
        String localTo = LocaleUtils.getDateTimeForLocal(timezone, to);

        CashOutAnalytics cashOutAnalytics = ticketApi.getCashOutAnalytics(localFrom, localTo);
        return cashOutAnalytics;
    }


}
