package de.sportsbook.activbet.services.user.dtos;

public class UserRoles {

    public static String SuperUser = "SuperUser";
    public static String Administration_Sport_Navigation = "Administration_Sport_Navigation";
    public static String Administration_Sport_Read = "Administration_Sport_Read";
    public static String Administration_Region_Read = "Administration_Region_Read";
    public static String Administration_Category_Read = "Administration_Category_Read";
    public static String Administration_Tournament_Read = "Administration_Tournament_Read";
    public static String Administration_Season_Read = "Administration_Season_Read";
    public static String Administration_Venue_Read = "Administration_Venue_Read";
    public static String Administration_TipModel_Read = "Administration_TipModel_Read";
    public static String Administration_Customer_Read = "Administration_Customer_Read";
    public static String Administration_Ticket = "Administration_Ticket";
    public static String Administration_Rival_Read = "Administration_Rival_Read";
    public static String Administration_Company_Read = "Administration_Company_Read";
    public static String Administration_User_Read = "Administration_User_Read";
    public static String Administration_Client_Read = "Administration_Client_Read";
    public static String Administration_Currency_Read = "Administration_Currency_Read";
    public static String Administration_Title_Read = "Administration_Title_Read";
    public static String Administration_Language_Read = "Administration_Language_Read";
    public static String Administration_Roles_Read = "Administration_Roles_Read";
    public static String Administration_RoleGroups_Read = "Administration_RoleGroups_Read";

}
