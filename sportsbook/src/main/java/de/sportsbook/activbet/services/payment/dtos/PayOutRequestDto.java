package de.sportsbook.activbet.services.payment.dtos;

import de.sportsbook.activbet.api.payment.entities.PayOutRequest;
import de.sportsbook.activbet.api.system.entities.TechnicalData;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

@Data
public class PayOutRequestDto {
    private BigDecimal value;
    private String pinCode;
    private String expireTime;
    private String payOutRequestStatus;
    private String payOutRequestFlag;
    private Integer id;
    private Integer customerId;
    private String customerUsername;
    private String insertTime;
    private String updateTime;

    public PayOutRequestDto(PayOutRequest payOut) {
        this.value = payOut.getValue();
        this.pinCode = payOut.getPinCode();
        this.expireTime = LocaleUtils.getDateTimeForUser(payOut.getExpireTime());

        if (Objects.nonNull(payOut.getPayOutRequestStatus())) {
            this.payOutRequestStatus = payOut.getPayOutRequestStatus().name();
        }

        if (Objects.nonNull(payOut.getPayOutRequestFlag())) {
            this.payOutRequestFlag = payOut.getPayOutRequestFlag().name();
        }

        TechnicalData technicalData = payOut.getTechnicalData();
        if (Objects.nonNull(technicalData)) {
            this.id = technicalData.getId();
            this.insertTime = LocaleUtils.getDateTimeForUser(technicalData.getInsertTime());
            this.updateTime = LocaleUtils.getDateTimeForUser(technicalData.getUpdateTime());
        }

        if (Objects.nonNull(payOut.getCustomer())) {
            this.customerId = payOut.getCustomer().getId();
            this.customerUsername = payOut.getCustomer().getUsername();
        }
    }
}
