package de.sportsbook.activbet.services.app.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TimeZoneDto {

    private final String id;
    private final Integer offsetMinute;
    private final String name;

    public TimeZoneDto(TimeZone timeZone) {
        ZoneId zoneId = timeZone.toZoneId();
        this.id = zoneId.getId();
        /*
        String offset = LocalDateTime.now().atZone(zoneId).getOffset().getId();
        if (offset.equals("Z")) {
            offset = "";
        }
        this.offset = offset;
         */
        this.offsetMinute = timeZone.getOffset(new Date().getTime()) / 1000 / 60;
        Integer offset = offsetMinute / 60;
        this.name = zoneId.getId().replace("_", " ") + " GMT" + ((offset >= 0) ? "+" : "") + offset;
    }
}
