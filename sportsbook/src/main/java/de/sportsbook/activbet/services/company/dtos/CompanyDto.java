package de.sportsbook.activbet.services.company.dtos;

import de.sportsbook.activbet.api.company.entities.Company;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CompanyDto {
    private Integer id;
    private String shortcut;
    private String name1;
    private String name2;
    private Integer parentId;
    private Integer level;

    private List<CompanyDto> companies;

    public CompanyDto(Company company) {
        this.id = company.getId();
        this.shortcut = company.getShortcut();
        this.name1 = company.getName1();
        this.name2 = company.getName2();
        this.parentId = company.getParentId();
    }
}
