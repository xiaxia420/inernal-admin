package de.sportsbook.activbet.services.tickets.dtos;

import de.sportsbook.activbet.api.ticket.entities.TicketBetGameResult;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
public class TicketBetGameResultDto {
    private BigDecimal home;
    private BigDecimal away;
    //private List<Integer> playTimePeriodFlag;
    private String playTimePeriodFlag;

    public TicketBetGameResultDto(TicketBetGameResult result) {
        this.home = result.getHome();
        this.away = result.getAway();
        if (Objects.nonNull(result.getPlayTimePeriodFlag())) {

            //this.playTimePeriodFlag = result.getPlayTimePeriodFlag().stream().map(flag -> flag.getFlag()).collect(Collectors.toList());
            // only show last
            this.playTimePeriodFlag = result.getPlayTimePeriodFlag().stream().collect(Collectors.toList()).get(result.getPlayTimePeriodFlag().size()-1).name();
        }
    }
}
