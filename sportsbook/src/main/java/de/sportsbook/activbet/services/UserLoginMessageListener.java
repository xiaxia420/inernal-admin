package de.sportsbook.activbet.services;

import de.sportsbook.activbet.configuration.RedisSessionRegistry;
import de.sportsbook.activbet.configuration.WebSocketActionUtils;
import de.sportsbook.activbet.configuration.WebSocketSessionUtils;
import de.sportsbook.activbet.api.oauth.enums.LogoutReason;
import de.sportsbook.activbet.services.UserLoginMessageListener.UserLoginEvent;
import de.sportsbook.activbet.services.oauth.OauthService;
import de.sportsbook.activbet.services.user.UserService;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.websocket.actions.user.ForceLogoutAction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.api.listener.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class UserLoginMessageListener implements MessageListener<UserLoginEvent> {

    public static final String LOGIN_TOPIC = "login";

    private final RTopic<UserLoginEvent> topic;
    private final int listenerId;

    @Autowired
    private RedisSessionRegistry registry;

    @Autowired
    private WebSocketActionUtils webSocketActionUtils;

    @Autowired
    private WebSocketSessionUtils webSocketSessionUtils;

    @Autowired
    private OauthService oAuthService;

    @Autowired
    private UserService userService;

    @Autowired
    public UserLoginMessageListener(RedissonClient redisson) {
        topic = redisson.getTopic(LOGIN_TOPIC);
        listenerId = topic.addListener(this);
    }

    @PreDestroy
    public void removeListener() {
        topic.removeListener(listenerId);
    }

    @Override
    public void onMessage(String channel, UserLoginEvent loginEvent) {

        /*
        Set<String> existingUserSessionIds = registry.getAllSessionIds(loginEvent.getUsername());

        // Don't force logout the session that was just logged in
        existingUserSessionIds.stream().filter(s -> !s.equals(loginEvent.getSessionId())).forEach(sessionId -> {
            Session existingSession = SessionHolder.getSession(sessionId);

            if (existingSession == null) {
                log.info("Force logout was called on a session that didn't exist. User: {}. Cleaning up registry.",
                         loginEvent.getUsername());
                registry.removeSession(sessionId);
                return;
            }

            Map<String, Long> webSocketSessionUuids = existingSession.getAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS);
            log.info("Force logging out of user {} with session-id {}", loginEvent.getUsername(),
                     existingSession.getId());

            if (webSocketSessionUuids != null) {
                // Get the the websocket session ids that this node holds.
                List<String> wsSessionIds = webSocketSessionUuids.keySet()
                        .stream()
                        .map(webSocketSessionUtils::getWsSessionIdForWsUuid)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                if (!wsSessionIds.isEmpty()) {
                    webSocketActionUtils.broadcastToUser(wsSessionIds, new ForceLogoutAction());
                }
            }

            String remoteAddress = existingSession.getAttribute(SessionAttributes.REMOTE_ADDRESS);
            String userAgent = existingSession.getAttribute(SessionAttributes.USER_AGENT);

            userService.logout(existingSession, LogoutReason.FORCE_LOGOUT);

            //logoutLogService.logLogout(loginEvent.getUsername(), remoteAddress, userAgent, OauthApi.LogoutReason.FORCE_LOGOUT);
            oAuthService.logout(loginEvent.getUsername(), remoteAddress, userAgent, LogoutReason.FORCE_LOGOUT);
        });
         */
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserLoginEvent implements Serializable {
        private String username;
        private String sessionId;
    }

}
