package de.sportsbook.activbet.services.customer.dtos;

import de.sportsbook.activbet.api.company.entities.Company;
import de.sportsbook.activbet.api.customer.entities.ActionCustomer;
import de.sportsbook.activbet.api.customer.entities.CustomerAccountBooking;
import de.sportsbook.activbet.api.system.entities.RelationProperty;
import de.sportsbook.activbet.api.system.entities.SystemClient;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Data
public class CustomerAccountBookingDto {
    private Integer id;
    private String insertTime;
    private String bookingReason;
    private String bookingType;
    private String text;
    private BigDecimal balanceBefore;
    private BigDecimal value;
    private BigDecimal balance;
    private String externalReferenceId;
    private String externalReferenceType;
    private ActionCustomer insertUser;
    private Company company;
    private SystemClient systemClient;
    private String remoteAddress;
    private String userAgent;
    private String ticketNumber;

    public CustomerAccountBookingDto(CustomerAccountBooking booking) {
        this.id = booking.getId();
        this.insertTime = LocaleUtils.getDateTimeForUser(booking.getInsertTime());

        if (Objects.nonNull(booking.getReason())) {
            this.bookingReason = booking.getReason().name();
        }

        if (Objects.nonNull(booking.getType())) {
            this.bookingType = booking.getType().name();
        }

        this.text = booking.getText();
        this.value = booking.getValue();
        this.balance = booking.getBalance();

        if (Objects.nonNull(value) && Objects.nonNull(balance)) {
            this.balanceBefore = balance.subtract(value);
        }

        this.externalReferenceId = booking.getExternalReferenceId();
        this.externalReferenceType = booking.getExternalReferenceType();
        this.insertUser = booking.getInsertUser();
        this.company = booking.getCompany();
        this.remoteAddress = booking.getRemoteAddress();
        this.userAgent = booking.getUserAgent();

        if (Objects.nonNull(booking) && Objects.nonNull(booking.getRelation())
            && !CollectionUtils.isEmpty(booking.getRelation().getProperties()) )
        {
            List<RelationProperty> properties = booking.getRelation().getProperties();
            RelationProperty property = properties.stream().filter(p -> Objects.equals("number", p.getName())).findFirst().orElse(null);
            if (Objects.nonNull(property)) {
                this.ticketNumber = property.getValue();
            }
        }

    }
}
