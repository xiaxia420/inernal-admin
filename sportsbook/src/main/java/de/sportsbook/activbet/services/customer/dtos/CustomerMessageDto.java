package de.sportsbook.activbet.services.customer.dtos;

import de.sportsbook.activbet.api.customer.entities.ActionCustomer;
import de.sportsbook.activbet.api.customer.entities.CustomerMessage;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.util.Objects;

@Data
public class CustomerMessageDto {

    private Integer id;
    private String insertTime;
    private String updateTime;
    private String firstName;
    private String lastName;
    private String email;
    private String mobile;
    private String subject;
    private String content;
    private Boolean isAnonymous;

    private Integer customerId;
    private String customerUsername;

    private Integer processUserId;
    private String processUsername;

    private Integer closeUserId;
    private String closeUsername;

    private String messageStatus;

    public CustomerMessageDto(CustomerMessage customerContact) {
        this.id = customerContact.getId();
        this.insertTime = LocaleUtils.getDateTimeForUser(customerContact.getInsertTime());
        this.updateTime = LocaleUtils.getDateTimeForUser(customerContact.getUpdateTime());
        this.firstName = customerContact.getFirstName();
        this.lastName = customerContact.getLastName();
        this.email = customerContact.getEmail();
        this.subject = customerContact.getSubject();
        this.content = customerContact.getContent();
        this.isAnonymous = customerContact.getIsAnonymous();

        ActionCustomer requestUser = customerContact.getRequestUser();
        if (Objects.nonNull(requestUser)) {
            this.customerId = requestUser.getId();
            this.customerUsername = requestUser.getUsername();
        }

        ActionCustomer progressUser = customerContact.getProgressUser();
        if (Objects.nonNull(customerContact.getProgressUser())) {
            this.processUserId = progressUser.getId();
            this.processUsername = progressUser.getUsername();
        }

        ActionCustomer closeUser = customerContact.getCloseUser();
        if (Objects.nonNull(closeUser)) {
            this.closeUserId = closeUser.getId();
            this.closeUsername = closeUser.getUsername();
        }

        // OPEN / PROCEEDED / CLOSED
        this.messageStatus = "OPEN";
        if (Objects.nonNull(processUserId)) {
            this.messageStatus = "PROCEEDED";
        }
        if (Objects.nonNull(closeUserId)) {
            this.messageStatus = "CLOSED";
        }

    }
}
