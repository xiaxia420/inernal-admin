package de.sportsbook.activbet.services.system.dtos;

import de.sportsbook.activbet.api.system.entities.TechnicalData;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class TechnicalDataDto {

    private Integer id;

    private String insertTime;
    private Integer insertId;
    private String insertName;

    private Integer updateId;
    private String updateTime;
    private String updateName;

    public TechnicalDataDto(TechnicalData data) {
        this.id = data.getId();
        this.insertTime = LocaleUtils.getDateTimeForUser(data.getInsertTime());
        this.insertId = data.getInsertId();
        this.insertName = data.getInsertName();

        this.updateId = data.getUpdateId();
        this.updateTime = LocaleUtils.getDateTimeForUser(data.getUpdateTime());
        this.updateName = data.getUpdateName();
    }
}
