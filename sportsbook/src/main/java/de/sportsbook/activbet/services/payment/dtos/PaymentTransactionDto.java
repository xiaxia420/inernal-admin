package de.sportsbook.activbet.services.payment.dtos;

import de.sportsbook.activbet.api.customer.entities.ActionCustomer;
import de.sportsbook.activbet.api.payment.entities.PaymentChannel;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;
import org.apache.tomcat.jni.Local;

import java.math.BigDecimal;
import java.util.Objects;

@Data
public class PaymentTransactionDto {

    private Integer id;
    private String uniqueCode;
    private BigDecimal value;
    private BigDecimal fee;
    private BigDecimal bookingValue;
    private String direction;
    private String status;
    private String statusChangeTime;
    private String statusReason;

    private Integer customerId;
    private String customerUsername;

    private Integer paymentChannelId;
    private String paymentChannelName;

    private String remoteAddress;

    private String insertTime;

    private String externalReferenceId;

    public PaymentTransactionDto(PaymentTransaction transaction) {
        this.uniqueCode = transaction.getUniqueCode();
        this.value = transaction.getValue();
        this.fee = transaction.getFee();
        this.bookingValue = transaction.getBookingValue();
        if (Objects.nonNull(transaction.getDirection())) {
            this.direction = transaction.getDirection().name();
        }
        if (Objects.nonNull(transaction.getStatus())) {
            this.status = transaction.getStatus().name();
        }
        this.statusChangeTime = LocaleUtils.getDateTimeForUser(transaction.getStatusChangeTime());

        ActionCustomer customer = transaction.getCustomer();
        if (Objects.nonNull(customer)) {
            this.customerId = customer.getId();
            this.customerUsername = customer.getUsername();
        }

        PaymentChannel channel = transaction.getPaymentChannel();
        if (Objects.nonNull(channel)) {
            if (Objects.nonNull(channel.getTechnicalData())) {
                this.paymentChannelId = channel.getTechnicalData().getId();
            }
            this.paymentChannelName = channel.getName();
        }

        if (Objects.nonNull(transaction.getTechnicalData())) {
            this.id = transaction.getTechnicalData().getId();
            this.insertTime = LocaleUtils.getDateTimeForUser(transaction.getTechnicalData().getInsertTime());
        }

        this.remoteAddress = transaction.getRemoteAddress();
        this.externalReferenceId = transaction.getExternalReferenceId();
    }
}
