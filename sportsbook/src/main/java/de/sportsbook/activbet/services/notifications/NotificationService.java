package de.sportsbook.activbet.services.notifications;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.enums.CustomerType;
import de.sportsbook.activbet.api.notification.NotificationApi;
import de.sportsbook.activbet.api.notification.entities.CashFlowNotification;
import de.sportsbook.activbet.api.notification.entities.MessageNotification;
import de.sportsbook.activbet.api.notification.entities.PayOutRequestNotification;
import de.sportsbook.activbet.api.notification.entities.PaymentTransactionNotification;
import de.sportsbook.activbet.api.system.entities.Filter;
import de.sportsbook.activbet.api.system.entities.FromTo;
import de.sportsbook.activbet.api.system.enums.FilterType;
import de.sportsbook.activbet.api.ticket.TicketApi;
import de.sportsbook.activbet.api.ticket.entities.Ticket;
import de.sportsbook.activbet.api.ticket.entities.TicketStatistics;
import de.sportsbook.activbet.api.ticket.entities.TicketsRequest;
import de.sportsbook.activbet.api.ticket.enums.TicketActive;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.notifications.dtos.CashFlowNotificationDto;
import de.sportsbook.activbet.services.notifications.dtos.MessageNotificationDto;
import de.sportsbook.activbet.services.notifications.dtos.PayOutRequestNotificationDto;
import de.sportsbook.activbet.services.notifications.dtos.PaymentTransactionNotificationDto;
import de.sportsbook.activbet.services.tickets.dtos.TicketDto;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class NotificationService {

    @Autowired
    private TicketApi ticketApi;

    @Autowired
    private NotificationApi notificationApi;

    public MessageNotificationDto requestMessageNotification() throws ApiErrorException, UnauthorizedUserException {
        MessageNotification notification = notificationApi.getMessageNotification();
        return new MessageNotificationDto(notification);
    }

    public PayOutRequestNotificationDto requestPayOutRequestNotification() throws ApiErrorException, UnauthorizedUserException {
        PayOutRequestNotification notification = notificationApi.getPayOutRequestNotification();
        return new PayOutRequestNotificationDto(notification);
    }

    public PaymentTransactionNotificationDto requestPaymentTransactionNotification() throws ApiErrorException, UnauthorizedUserException {
        PaymentTransactionNotification notification = notificationApi.getPaymentTransactionNotification();
        return new PaymentTransactionNotificationDto(notification);
    }

    public CashFlowNotification requestCashFlowNotification(Session session, String from, Integer currencyId) throws ApiErrorException, UnauthorizedUserException {
        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        String localFrom = LocaleUtils.getDateTimeForLocal(timezone, from);

        CashFlowNotification notification = notificationApi.getCashFlowNotification(localFrom, currencyId);
        //return new CashFlowNotificationDto(notification);

        return notification;
    }
}
