package de.sportsbook.activbet.services.payment.dtos;

import de.sportsbook.activbet.api.payment.entities.PaymentProvider;
import lombok.Data;

import java.util.Objects;

@Data
public class PaymentProviderDto {
    private Integer id;
    private Integer paymentChannelCount;
    private String name;


    public PaymentProviderDto(PaymentProvider paymentProvider) {
        this.paymentChannelCount = paymentProvider.getPaymentChannelCount();
        this.name = paymentProvider.getName();
        if (Objects.nonNull(paymentProvider.getTechnicalData())) {
            this.id = paymentProvider.getTechnicalData().getId();
        }
    }
}
