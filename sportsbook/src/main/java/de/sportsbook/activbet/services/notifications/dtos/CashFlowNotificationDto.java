package de.sportsbook.activbet.services.notifications.dtos;

import de.sportsbook.activbet.api.notification.entities.CashFlowElement;
import de.sportsbook.activbet.api.notification.entities.CashFlowNotification;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;

@Data
public class CashFlowNotificationDto {
    // last ticket at
    //private String lastShopTicket;
    //private String lastOnlineTicketAt;
    private Integer lastShopTicketAtDiff;
    private Integer lastOnlineTicketAtDiff;

    // last pay in
    private Integer lastShopPayInAtDiff;
    private BigDecimal lastShopPayInAmount;
    private Integer lastOnlinePayInAtDiff;
    private BigDecimal lastOnlinePayInAmount;

    // last pay out
    private Integer lastShopPayOutAtDiff;
    private BigDecimal lastShopPayOutAmount;
    private Integer lastOnlinePayOutAtDiff;
    private BigDecimal lastOnlinePayOutAmount;

    // online balance
    private BigDecimal onlineBalanceByOnlineCustomer;
    private BigDecimal onlineBalanceByShopCustomer;
    private BigDecimal onlineBalance;

    private Integer currencyId;

    private Integer getDiff(ZonedDateTime d1, ZonedDateTime d2) {
        return Math.round (
            ( d2.toInstant().toEpochMilli() - d1.toInstant().toEpochMilli() ) / 1000 / 60
        );
    }

    public CashFlowNotificationDto(CashFlowNotification notification, ZonedDateTime lastTicketSubmitAt) {
        ZoneId zoneId = ZoneId.of("UTC");
        ZonedDateTime now = ZonedDateTime.now(zoneId);

        // last ticket
        if (Objects.nonNull(notification.getLastAnonymousTicketPayIn())) {
            //this.lastShopTicketAt = LocaleUtils.getDateTimeForUser(notification.getLastAnonymousTicketPayIn().getInsertTime());
            this.lastShopTicketAtDiff = this.getDiff(notification.getLastAnonymousTicketPayIn().getInsertTime(), now);
        }

        this.lastOnlineTicketAtDiff = this.getDiff(lastTicketSubmitAt, now);

        // last pay in
        if (Objects.nonNull(notification.getLastShopPayIn())) {
            //this.lastShopPayInAt = LocaleUtils.getDateTimeForUser(notification.getLastShopPayIn().getInsertTime());
            this.lastShopPayInAtDiff = this.getDiff(notification.getLastShopPayIn().getInsertTime(), now);
            this.lastShopPayInAmount = notification.getLastShopPayIn().getAmount();
        }
        if (Objects.nonNull(notification.getLastOnlinePayIn())) {
            //this.lastOnlinePayInAt = LocaleUtils.getDateTimeForUser(notification.getLastOnlinePayIn().getInsertTime());
            this.lastOnlinePayInAtDiff = this.getDiff(notification.getLastOnlinePayIn().getInsertTime(), now);
            this.lastOnlinePayInAmount = notification.getLastOnlinePayIn().getAmount();
        }

        /*
        CashFlowElement lastPayIn = notification.getLastOnlinePayIn();
        if (Objects.isNull(lastPayIn)) {
            lastPayIn = notification.getLastShopPayIn();
        } else {
            if (Objects.nonNull(notification.getLastShopPayIn())) {
                if (lastPayIn.getInsertTime().compareTo(notification.getLastShopPayIn().getInsertTime()) < 0) {
                    lastPayIn = notification.getLastShopPayIn();
                }
            }
        }
        if (Objects.nonNull(lastPayIn)) {
            this.lastPayInAt = LocaleUtils.getDateTimeForUser(lastPayIn.getInsertTime());
            this.lastPayInAmount = lastPayIn.getAmount();
        }
         */

        // last pay out
        if (Objects.nonNull(notification.getLastShopPayOut())) {
            //this.lastShopPayOutAt = LocaleUtils.getDateTimeForUser(notification.getLastShopPayOut().getInsertTime());
            this.lastShopPayOutAtDiff = this.getDiff(notification.getLastShopPayOut().getInsertTime(), now);
            this.lastShopPayOutAmount = notification.getLastShopPayOut().getAmount();
        }
        if (Objects.nonNull(notification.getLastOnlinePayOut())) {
            //this.lastOnlinePayOutAt = LocaleUtils.getDateTimeForUser(notification.getLastOnlinePayOut().getInsertTime());
            this.lastOnlinePayOutAtDiff = this.getDiff(notification.getLastOnlinePayOut().getInsertTime(), now);
            this.lastOnlinePayOutAmount = notification.getLastOnlinePayOut().getAmount();
        }
        /*
        CashFlowElement lastPayOut = notification.getLastOnlinePayOut();
        if (Objects.isNull(lastPayOut)) {
            lastPayOut = notification.getLastShopPayOut();
        } else {
            if (Objects.nonNull(notification.getLastShopPayOut())) {
                if (lastPayOut.getInsertTime().compareTo(notification.getLastShopPayOut().getInsertTime()) < 0) {
                    lastPayOut = notification.getLastShopPayOut();
                }
            }
        }
        if (Objects.nonNull(lastPayOut)) {
            this.lastPayOutAt = LocaleUtils.getDateTimeForUser(lastPayOut.getInsertTime());
            this.lastPayOutAmount = lastPayOut.getAmount();
        }
         */

        this.onlineBalanceByOnlineCustomer = notification.getOnlineBalanceByOnlineCustomer();
        this.onlineBalanceByShopCustomer = notification.getOnlineBalanceByShopCustomer();
        this.onlineBalance = notification.getOnlineBalance();
    }
}
