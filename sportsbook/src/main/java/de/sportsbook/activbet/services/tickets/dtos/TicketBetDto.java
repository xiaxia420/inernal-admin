package de.sportsbook.activbet.services.tickets.dtos;

import de.sportsbook.activbet.api.ticket.entities.TicketBet;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class TicketBetDto {
    private Integer id;
    private String gameStartTime;
    private String sportName;
    private String sportIconName;
    private String categoryName;
    private String categoryIconName;
    private String tournamentName;
    private String gameName;
    private List<String> rivalNames;
    private String tipName;
    private String oddName;
    private BigDecimal oddValue;
    private BigDecimal adjustedOddValue;
    private BigDecimal stake;
    private String gameStatus;
    private Boolean isBank;
    private Boolean isRefund;
    private Boolean isHalfLost;
    private Boolean isLost;
    private Boolean isHalfWon;
    private Boolean isWon;
    private Boolean isLive;
    private BigDecimal scoreHome;
    private BigDecimal scoreAway;
    private List<TicketBetGameResultDto> gameResults;

    public TicketBetDto(TicketBet bet) {
        this.id = bet.getId();
        this.gameStartTime = LocaleUtils.getDateTimeForUser(bet.getGameStartTime());
        this.sportName = bet.getSportName();
        this.sportIconName = bet.getSportIconName();
        this.categoryName = bet.getCategoryName();
        this.categoryIconName = bet.getCategoryIconName();
        this.tournamentName = bet.getTournamentName();
        this.gameName = bet.getGameName();
        this.rivalNames = bet.getRivalNames();
        this.tipName = bet.getTipName();
        this.oddName = bet.getOddName();
        this.oddValue = bet.getOddValue();
        this.adjustedOddValue = bet.getAdjustedOddValue();
        this.stake = bet.getStake();
        this.gameStatus = bet.getGameStatus();
        this.isBank = bet.getIsBank();
        this.isRefund = bet.getIsRefund();
        this.isHalfLost = bet.getIsHalfLost();
        this.isLost = bet.getIsLost();
        this.isHalfWon = bet.getIsHalfWon();
        this.isWon = bet.getIsWon();
        this.isLive = bet.getIsLive();
        this.scoreHome = bet.getScoreHome();
        this.scoreAway = bet.getScoreAway();

        if (!CollectionUtils.isEmpty(bet.getGameResults())) {
            this.gameResults = bet.getGameResults().stream().map(TicketBetGameResultDto::new).collect(Collectors.toList());
        }
    }

}
