package de.sportsbook.activbet.services.user;

import de.sportsbook.activbet.api.user.User;
import de.sportsbook.activbet.api.user.UserApi;
import de.sportsbook.activbet.configuration.RedisSessionRegistry;
import de.sportsbook.activbet.exceptions.*;
import de.sportsbook.activbet.api.oauth.enums.LogoutReason;
import de.sportsbook.activbet.services.UserLoginMessageListener.UserLoginEvent;
import de.sportsbook.activbet.services.oauth.OauthService;
import de.sportsbook.activbet.services.user.dtos.UserRolesDto;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.utils.LocaleUtils;
import de.sportsbook.activbet.services.UserLoginMessageListener;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.session.Session;
import org.springframework.session.SessionRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Objects;
import java.util.Set;

@Slf4j
@Service
public class UserService {

    @Autowired
    private RedisSessionRegistry sessionRegistry;

    @Autowired
    private SessionRepository sessionRepository;

    private RTopic<UserLoginEvent> loginTopic;

    @Autowired
    private RedisSessionRegistry registry;

    @Autowired
    private UserApi userApi;

    @Autowired
    private OauthService oauthService;

    @Autowired
    public UserService(RedissonClient redisson) {
        loginTopic = redisson.getTopic(UserLoginMessageListener.LOGIN_TOPIC);
    }

    public UserRolesDto login(Session session, String username, String password)
        throws LoginFailedException, InternalException, UserAlreadyLoggedInException
    {
        try {
            OAuth2AccessToken accessToken = oauthService.authenticate(
                    username,
                    password,
                    session.getAttribute(SessionAttributes.REMOTE_ADDRESS),
                    session.getAttribute(SessionAttributes.USER_AGENT)
            );

            session.removeAttribute(SessionAttributes.LOGIN_TRIES); // Reset the login tries on success
            SessionAttributes.setAuthentication(session, accessToken);
        } catch (OAuth2AccessDeniedException e) {
            log.debug("Error at login", e);
            if (e.getOAuth2ErrorCode().equals(OAuth2Exception.ACCESS_DENIED)) {
                Integer loginTries = session.getAttribute(SessionAttributes.LOGIN_TRIES);
                if (loginTries == null) {
                    loginTries = 0;
                }
                loginTries = loginTries + 1;
                session.setAttribute(SessionAttributes.LOGIN_TRIES, loginTries);
                if (loginTries > 19) {
                    throw new LoginFailedException("login.error.login_failed_retry_limit_warning.html");
                }
                throw new LoginFailedException("login.error.login_failed.html");
            }
            SessionAttributes.removeAuthentication(session);
            log.error("Error at login", e);
            throw new InternalException();
        }

        return confirmLogin(session, username);

        /*
        // check user login from other devices
        Set<String> sessionIds = sessionRegistry.getAllSessionIds(username);
        if (!sessionIds.isEmpty()) {
            if (sessionIds.stream().filter(sessionId -> {
                Session ss = SessionHolder.getSession(sessionId);
                if (Objects.isNull(ss)) {
                    registry.removeSession(sessionId);
                    return false;
                }

                if (StringUtils.isEmpty(ss.getAttribute(SessionAttributes.USERNAME))) {
                    registry.removeSession(sessionId);
                    return false;
                }

                return true;
            }).findAny().isPresent()) {
                // warn him and wait for confirmation. user is not login
                session.setAttribute(SessionAttributes.IS_AUTHENTICATED, false);
                throw new UserAlreadyLoggedInException();
            } else {
                return confirmLogin(session, username);
            }
        } else {
            // Else auto-confirm login
            return confirmLogin(session, username);
        }
         */
    }

    // it should not happen to throw out these exceptions because the process does not allow
    public UserRolesDto confirmLogin(Session session, String username) throws LoginFailedException {
        // get user
        User user = null;
        try {
            user = userApi.getMe();
        } catch(Exception e) {
            // remove login status
            SessionAttributes.removeAuthentication(session);
            throw new LoginFailedException("login.error.get_me.failed.html");
        }

        // check admin user
        Boolean isAdmin = false;
        if (Objects.nonNull(user.getRights())) {
            isAdmin = user.getRights().getHasAdministrationAccess() ||  user.getRights().getHasAgentAccess();
        }
        if (!isAdmin) {
            // remove login status
            SessionAttributes.removeAuthentication(session);
            throw new LoginFailedException("login.error.account_not_allowed");
        }

        // Broadcast user login
        loginTopic.publish(new UserLoginEvent(username, session.getId()));
        sessionRegistry.registerNewSession(username, session.getId());
        session.setAttribute(SessionAttributes.LOGIN_TIME, System.currentTimeMillis() / 1000); // UTC Seconds

        // from confirm login, username should not be overwritten
        if (Objects.nonNull(username)) {
            session.setAttribute(SessionAttributes.USERNAME, username);
        }

        session.setAttribute(SessionAttributes.IS_AUTHENTICATED, true);
        session.setAttribute(SessionAttributes.IS_ADMIN_USER, true);
        session.setAttribute(SessionAttributes.DEFAULT_LANGUAGE_ID, user.getLanguage());
        session.setAttribute(SessionAttributes.USER_COMPANY_ID, user.getCompanyId());
        session.setAttribute(SessionAttributes.USER_CURRENCY_ID, user.getCurrencyId());

        return new UserRolesDto(user);
    }

    public void logout(Session session, LogoutReason logoutReason) {
        // logout from session
        if (session == null) {
            log.debug("Session is null in logout!");
            return;
        }

        // logout from middleware
        String username = session.getAttribute(SessionAttributes.USERNAME);
        String remoteAddress = session.getAttribute(SessionAttributes.REMOTE_ADDRESS);
        String userAgent = session.getAttribute(SessionAttributes.USER_AGENT);
        oauthService.logout(username, remoteAddress, userAgent, logoutReason);

        // remove session
        String sessionId = session.getId();
        sessionRepository.delete(sessionId);
    }

    public String selectLocale(Session session, String locale) {
        String newLocale = LocaleUtils.validateLocale(locale);
        session.setAttribute(SessionAttributes.LOCALE, newLocale);
        return newLocale;
    }

    public String selectTimezone(Session session, String timezone) {
        String newTimezone = LocaleUtils.validateTimezone(timezone);
        session.setAttribute(SessionAttributes.TIMEZONE, newTimezone);
        return newTimezone;
    }

    public UserRolesDto getUserRoles(Session session) {
        Boolean isAuthenticated = SessionAttributes.isAuthenticated(session);
        if (Objects.nonNull(isAuthenticated) && isAuthenticated) {
            User user = userApi.getMe();
            return new UserRolesDto(user);
        }
        return null;
    }

}
