package de.sportsbook.activbet.services.app;

import de.sportsbook.activbet.api.app.AppApi;
import de.sportsbook.activbet.api.app.entities.Language;
import de.sportsbook.activbet.api.customer.entities.Currency;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.app.dtos.TimeZoneDto;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AppService {

    @Autowired
    private AppApi appApi;

    public List<Language> getLanguages() {
        try {
            return appApi.getLanguages();
        } catch (Exception e) {
            log.error("", e);
        }
        return new ArrayList<>();

        /*
        List<Language> languages = new ArrayList<>();
        LanguageDto en = new LanguageDto();
        en.setId(1);
        en.setName("English");
        en.setCode("en");

        LanguageDto de = new LanguageDto();
        de.setId(2);
        de.setName("Deutsch");
        de.setCode("de");

        LanguageDto zh = new LanguageDto();
        zh.setId(3);
        zh.setName("简体中文");
        zh.setCode("zh");

        languages.add(en);
        languages.add(de);
        languages.add(zh);
        return languages;
         */
    }

    public List<TimeZoneDto> getTimezones() {
        return LocaleUtils.getTimezoneIds().stream().map(TimeZone::getTimeZone).map(TimeZoneDto::new).collect(Collectors.toList());
    }

    public List<Currency> getCurrencies() {
        try {
            return appApi.getCurrencies();
        } catch (Exception e) {
            log.error("", e);
        }
        return new ArrayList<>();
    }

}
