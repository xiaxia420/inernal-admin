package de.sportsbook.activbet.controllers;

import de.sportsbook.activbet.configuration.BetagoWebProperties;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Slf4j
@ControllerAdvice
public class ErrorControllerAdvice {

    @Autowired
    private BaseController baseController;

    @Autowired
    private BetagoWebProperties properties;

    @ExceptionHandler(Throwable.class)
    public String exceptionHandler(Throwable e, Model model, HttpServletRequest request, HttpServletResponse response,
                                   Device device) {
        try {
            response.setStatus((e instanceof NoHandlerFoundException) ? 404 : 500);
            return baseController.get(model, request, response, device);
        } catch (Exception e1) {
            String uuid = UUID.randomUUID().toString();

            MDC.put("uuid", uuid);
            Marker internalServerErrorMarker = MarkerFactory.getMarker("INTERNAL_SERVER_ERROR");
            log.error(internalServerErrorMarker, "Internal Server Error", e);
            MDC.clear();

            response.setStatus(500);
            model.addAttribute("uuid", uuid);
            model.addAttribute("theme", properties.getTheme());
            return "error500";
        }
    }
}
