package de.sportsbook.activbet.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.sportsbook.activbet.Routes;
import de.sportsbook.activbet.api.oauth.enums.LogoutReason;
import de.sportsbook.activbet.configuration.BetagoWebProperties;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.react.React;
import de.sportsbook.activbet.services.user.UserService;
import de.sportsbook.activbet.state.ApplicationState;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.utils.LocaleUtils;
import de.sportsbook.activbet.utils.RequestUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.mobile.device.Device;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Map;

@Controller
@Slf4j
public class BaseController {

    public static final int ONE_YEAR = 60 * 60 * 24 * 365;
    public static final String LOCALE_COOKIE_NAME = "LOCALE";
    public static final String TIMEZONE_COOKIE_NAME = "TIMEZONE";
    @Autowired
    private ApplicationState applicationState;

    @Autowired
    private BetagoWebProperties properties;

    @Autowired
    private UserService userService;

    @Autowired
    private Environment env;

    @Autowired
    @Qualifier("frontendObjectMapper")
    private ObjectMapper objectMapper;

    @Autowired(required = false)
    private React react;

    @GetMapping(path = {
            Routes.ROOT,
            Routes.LOGIN
    })
    public String get(Model model, HttpServletRequest request, HttpServletResponse response, Device device) throws UnsupportedEncodingException {
        try {
            prepareResponse(model, request, response, device);
        } catch (UnauthorizedUserException e) {
            // Clear the session
            userService.logout(SessionHolder.getSession(false), LogoutReason.AUTOMATIC_LOGOUT);
            // Redirect to generate a new session
            //return "redirect:/login?next=" + UriUtils.encode(getUrl(request), "UTF-8");
            //String domainName = request.getScheme() + "://" + request.getServerName() +  ":" + request.getServerPort();
            //return "redirect:" + domainName + "/login?next=" + UriUtils.encode(getUrl(request), "UTF-8");

            String domainName = properties.getHostname();
            return "redirect:" + domainName + "/login?next=" + UriUtils.encode(getUrl(request), "UTF-8");

        }
        return "base";
    }

    @SneakyThrows
    @GetMapping(path = {
        Routes.TICKET,
        Routes.CUSTOMER
    })
    public String getAuthenticated(Model model, HttpServletRequest request, HttpServletResponse response, Device device) {
        try {
            Session session = SessionHolder.getSession(false);
            if (SessionAttributes.isUserAuthenticated(session)) {
                prepareResponse(model, request, response, device);
                return "base";
            }
        } catch (UnauthorizedUserException e) {
            // session expired, do nothing but redirect to login
        }

        //String domainName = request.getScheme() + "://" + request.getServerName() +  ":" + request.getServerPort();
        //return "redirect:" + domainName + "/login?next=" + UriUtils.encode(getUrl(request), "UTF-8");

        String domainName = properties.getHostname();
        return "redirect:" + domainName + "/login?next=" + UriUtils.encode(getUrl(request), "UTF-8");
    }

    private void prepareResponse(Model model, HttpServletRequest request, HttpServletResponse response, Device device) {
        model.addAttribute("webSocketPort", properties.getWebSocketPort());
        Session session = getSession(request);
        String locale = request.getParameter("locale");
        if (locale != null) {
            String newLocale = userService.selectLocale(session, locale);
            Cookie localeCookie = new Cookie(LOCALE_COOKIE_NAME, newLocale);
            localeCookie.setMaxAge(ONE_YEAR);
            response.addCookie(localeCookie);
        }

        String timezone = request.getParameter("timezone");
        if (timezone != null) {
            String newTimezone = userService.selectTimezone(session, timezone);
            Cookie timezoneCookie = new Cookie(TIMEZONE_COOKIE_NAME, newTimezone);
            timezoneCookie.setMaxAge(ONE_YEAR);
            response.addCookie(timezoneCookie);
        }

        Map<String, Object> state = this.applicationState.getApplicationState(request, session, device);
        try {
            model.addAttribute("state", objectMapper.writeValueAsString(state));
        } catch (JsonProcessingException e) {
            log.error("Error writing application state as JSON", e);
        }
        if (react != null) {
            // Server-side rendering
            model.addAttribute("reactResult", react.renderToString(state));
        }
        model.addAttribute("language", session.getAttribute(SessionAttributes.LOCALE));
        model.addAttribute("theme", properties.getTheme());
        model.addAttribute("env", env);
    }

    private Session getSession(HttpServletRequest request) {
        Session session = SessionHolder.getSession(true);
        String locale = null;
        String timezone = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(LOCALE_COOKIE_NAME)) {
                    locale = LocaleUtils.validateLocale(cookie.getValue());
                }
                if (cookie.getName().equals(TIMEZONE_COOKIE_NAME)) {
                    timezone = LocaleUtils.validateTimezone(cookie.getValue());
                }
            }
        }

        // Set to browser locale
        if (locale == null) {
            // english as default
            locale = "en";
        }
        // Set to Addis Ababa
        if (timezone == null) {
            timezone = properties.getDefaultTimezone();
        }

        SessionAttributes.setIfAbsent(session, SessionAttributes.LOCALE, locale);
        SessionAttributes.setIfAbsent(session, SessionAttributes.TIMEZONE, timezone);
        SessionAttributes.setIfAbsent(session, SessionAttributes.REMOTE_ADDRESS, RequestUtils.getRemoteAddress(request));
        SessionAttributes.setIfAbsent(session, SessionAttributes.USER_AGENT, RequestUtils.getUserAgent(request));

        return session;
    }

    private String getUrl(HttpServletRequest request) {
        String url = new UrlPathHelper().getPathWithinApplication(request);
        if (request.getQueryString() != null) {
            url += "?" + request.getQueryString();
        }
        return url;
    }

}
