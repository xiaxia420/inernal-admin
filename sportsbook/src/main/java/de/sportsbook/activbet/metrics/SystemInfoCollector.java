package de.sportsbook.activbet.metrics;

import de.sportsbook.activbet.metrics.SystemInfoService.MemoryInfo;
import de.sportsbook.activbet.metrics.SystemInfoService.NetworkInfo;
import de.sportsbook.activbet.metrics.SystemInfoService.ApplicationInfo;
import de.sportsbook.activbet.metrics.SystemInfoService.VolumeInfo;
import io.prometheus.client.Collector;
import io.prometheus.client.Collector.MetricFamilySamples.Sample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@Component
public class SystemInfoCollector extends Collector {

    private final SystemInfoService service;

    @Autowired
    public SystemInfoCollector(SystemInfoService service) {
        this.service = service;
        this.register();
    }

    @Override
    public List<MetricFamilySamples> collect() {
        List<MetricFamilySamples> metrics = new ArrayList<>();

        metrics.add(getCpu());
        metrics.add(getNetwork());
        metrics.add(getUptime());
        metrics.add(getMemory());
        metrics.add(getVolumes());
        metrics.add(getProcesses());

        return metrics;
    }

    private MetricFamilySamples getNetwork() {
        List<Sample> networkIfs = new ArrayList<>();

        for (Entry<String, NetworkInfo> entry : service.getNetwork().entrySet()) {
            NetworkInfo info = entry.getValue();

            networkIfs.add(new Sample("network_traffic",
                                      Arrays.asList("interface", "type"),
                                      Arrays.asList(entry.getKey(), "sent"),
                                      info.getBytesSent()));

            networkIfs.add(new Sample("network_traffic",
                                      Arrays.asList("interface", "type"),
                                      Arrays.asList(entry.getKey(), "recv"),
                                      info.getBytesRecv()));
        }

        return new MetricFamilySamples("network_traffic", Type.COUNTER, "", networkIfs);
    }

    private MetricFamilySamples getUptime() {
        return new MetricFamilySamples("uptime",
                                       Type.COUNTER,
                                       "",
                                       Collections.singletonList(new Sample("uptime",
                                                                            Collections.emptyList(),
                                                                            Collections.emptyList(),
                                                                            service.getUptime())));
    }

    public MetricFamilySamples getCpu() {
        List<Sample> cpus = service.getCpuLoad()
                .entrySet()
                .stream()
                .map(entry -> new Sample("cpu_load",
                                         Collections.singletonList("core"),
                                         Collections.singletonList(String.valueOf(entry.getKey())),
                                         entry.getValue()))
                .collect(Collectors.toList());

        return new MetricFamilySamples("cpu_load", Type.GAUGE, "", cpus);
    }

    public MetricFamilySamples getMemory() {
        MemoryInfo memory = service.getMemory();

        Sample total = new Sample("memory_usage",
                                  Collections.singletonList("type"),
                                  Collections.singletonList("total"),
                                  memory.getTotal());
        Sample available = new Sample("memory_usage",
                                      Collections.singletonList("type"),
                                      Collections.singletonList("available"),
                                      memory.getAvailable());
        Sample swapTotal = new Sample("memory_usage",
                                      Collections.singletonList("type"),
                                      Collections.singletonList("swap_total"),
                                      memory.getSwapTotal());
        Sample swapUsed = new Sample("memory_usage",
                                     Collections.singletonList("type"),
                                     Collections.singletonList("swap_used"),
                                     memory.getSwapUsed());

        return new MetricFamilySamples("memory_usage", Type.GAUGE, "", Arrays.asList(total, available, swapTotal, swapUsed));
    }

    public MetricFamilySamples getVolumes() {
        List<Sample> volumes = new ArrayList<>();

        for (Entry<String, VolumeInfo> entry : service.getVolumes().entrySet()) {
            VolumeInfo info = entry.getValue();

            volumes.add(new Sample("volume_usage",
                                   Arrays.asList("name", "type"),
                                   Arrays.asList(entry.getKey(), "total"),
                                   info.getTotalSpace()));

            volumes.add(new Sample("volume_usage",
                                   Arrays.asList("name", "type"),
                                   Arrays.asList(entry.getKey(), "useable"),
                                   info.getUseableSpace()));
        }

        return new MetricFamilySamples("volume_usage", Type.COUNTER, "", volumes);
    }

    public MetricFamilySamples getProcesses() {
        List<Sample> volumes = new ArrayList<>();

        for (ApplicationInfo info : service.getApplications()) {

            volumes.add(new Sample("app",
                                   Arrays.asList("name", "cmd", "type"),
                                   Arrays.asList(info.getName(), info.getCmd(), "cpu_load"),
                                   info.getCpuLoad()));

            volumes.add(new Sample("app",
                                   Arrays.asList("name", "cmd", "type"),
                                   Arrays.asList(info.getName(), info.getCmd(), "memory_usage"),
                                   info.getMemory()));

            volumes.add(new Sample("app",
                                   Arrays.asList("name", "cmd", "type"),
                                   Arrays.asList(info.getName(), info.getCmd(), "uptime"),
                                   info.getUptime()));

        }

        return new MetricFamilySamples("process", Type.COUNTER, "", volumes);
    }
}
