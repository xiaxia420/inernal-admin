package de.sportsbook.activbet.notifications;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NotificationAction extends Action {

    private String type = "notifications/SHOW_NOTIFICATION";
    private String category;
    private String level;
    private String message;

}
