package de.sportsbook.activbet.state;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.session.Session;

import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

public class SessionAttributes {

    /* Internal */
    public static final String REMOTE_ADDRESS = "remoteAddress";
    public static final String USER_AGENT = "userAgent";
    public static final String USERNAME = "username";
    public static final String IS_AUTHENTICATED = "is.authenticated";
    public static final String OAUTH_EXPIRES = "oauth.expires";
    public static final String OAUTH_ACCESS_TOKEN = "oauth.accessToken";
    public static final String OAUTH_REFRESH_TOKEN = "oauth.refreshToken";
    public static final String WEB_SOCKET_SESSION_UUIDS = "webSocketSessionUuids";
    public static final String LOGIN_TRIES = "loginTries";

    // default settings
    public static final String DEFAULT_LANGUAGE_ID = "defaultLanguageId";
    public static final String USER_COMPANY_ID = "userCompanyId";

    /* External */
    public static final String LOCALE = "locale";
    public static final String TIMEZONE = "timezone";
    public static final String LOGIN_TIME = "loginTime";
    public static final String IS_ADMIN_USER = "isAdminUser";
    public static final String USER_CURRENCY_ID = "userCurrencyId";

    //public static final String USER_ID = "userId";
    //public static final String CURRENCY = "currency";
    //public static final String CURRENCY_DECIMAL_DIGITS = "currencyDecimalDigits";
    //public static final String DEBUG_MODE = "debugMode";
    //public static final String EMAIL = "email";
    //public static final String ACCEPT_ODD_MODIFICATION = "acceptOddModification";
    //public static final String ACCEPT_TERMS_AND_CONDITIONS = "acceptTermsAndConditions";
    //public static final String FORCE_PASSWORD_CHANGE = "forcePasswordChange";
    //public static final String USER_PREFER_LANGUAGE = "userPreferLanguage";

    public static UserState toUserState(Session session) {
        UserState userState = new UserState();
        userState.setLocale(session.getAttribute(LOCALE));
        userState.setTimezone(session.getAttribute(TIMEZONE));
        userState.setAuthenticated(isUserAuthenticated(session));
        userState.setUsername(session.getAttribute(USERNAME));
        userState.setLoginTime(session.getAttribute(LOGIN_TIME));
        userState.setTimezoneOffsetMinutes(getTimezoneOffset(session));
        userState.setUserCurrencyId(session.getAttribute(USER_CURRENCY_ID));
        return userState;
    }

    public static boolean isAuthenticated(Session session) {
        return session != null && session.getAttribute(OAUTH_ACCESS_TOKEN) != null;
    }

    public static Boolean isUserAuthenticated(Session session) {
        if (session != null) {
            Boolean isAuthenticated = session.getAttribute(IS_AUTHENTICATED);
            if (Objects.nonNull(isAuthenticated)) {
                return true == isAuthenticated;
            }
        }
        return false;
    }

    public static void removeAuthentication(Session session) {
        session.removeAttribute(IS_AUTHENTICATED);
        session.removeAttribute(OAUTH_ACCESS_TOKEN);
        session.removeAttribute(OAUTH_REFRESH_TOKEN);
        session.removeAttribute(OAUTH_EXPIRES);
    }

    /*
    public static boolean isDebugMode(Session session) {
        if (session == null) {
            return false;
        }
        Boolean debugMode = session.getAttribute(DEBUG_MODE);
        return debugMode != null && debugMode;
    }
     */

    public static void setIfAbsent(Session session, String key, Object value) {
        if (session.getAttribute(key) == null) {
            session.setAttribute(key, value);
        }
    }

    public static void setAuthentication(Session session, OAuth2AccessToken accessToken) {
        session.setAttribute(SessionAttributes.OAUTH_EXPIRES,
                System.currentTimeMillis() / 1000 + accessToken.getExpiresIn()); // UTC Seconds);
        session.setAttribute(SessionAttributes.OAUTH_ACCESS_TOKEN, accessToken.getValue());
        session.setAttribute(SessionAttributes.OAUTH_REFRESH_TOKEN, accessToken.getRefreshToken().getValue());
    }

    public static Integer getTimezoneOffset(Session session) {
        String timezone = session.getAttribute(TIMEZONE);
        TimeZone userTimezone = TimeZone.getTimeZone(timezone);
        Integer offset = userTimezone.getOffset(new Date().getTime()) / 1000 / 60;
        return offset;
    }
}
