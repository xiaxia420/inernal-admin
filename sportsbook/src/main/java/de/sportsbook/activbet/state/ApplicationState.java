package de.sportsbook.activbet.state;

import de.sportsbook.activbet.configuration.BetagoWebProperties;
import de.sportsbook.activbet.configuration.MessagesResourceBundle;
import de.sportsbook.activbet.services.app.AppService;
import de.sportsbook.activbet.services.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.LiteDeviceResolver;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class ApplicationState {

    @Autowired
    private AppService appService;

    @Autowired
    private UserService userService;

    @Autowired
    private BetagoWebProperties webProperties;

    @Autowired
    private MessagesResourceBundle messagesResourceBundle;

    private UrlPathHelper urlHelper = new UrlPathHelper();

    /**
     * Builds the application state that is used by react. Builds the state depending on the current location.
     *
     * @param request
     * @param session
     * @param device  @return
     */
    public Map<String, Object> getApplicationState(HttpServletRequest request, Session session, Device device) {

        //urlActionService.process(request, session);
        //AntPathMatcher pathMatcher = new AntPathMatcher();
        //String path = urlHelper.getLookupPathForRequest(request);

        Map<String, Object> state = new HashMap<>();
        UserState userState = SessionAttributes.toUserState(session);
        userState.setUserRoles(userService.getUserRoles(session));

        Boolean isAuthenticated = SessionAttributes.isAuthenticated(session);
        if (isAuthenticated) {
            userState.setCurrencies(appService.getCurrencies());
        } else {
            userState.setCurrencies(new ArrayList<>());
        }

        state.put("user", userState);

        Map<String, Object> appState = new HashMap<>();
        state.put("app", appState);

        // image url
        appState.put("imageResourceUrl", webProperties.getImageResourceUrl());
        appState.put("barcodeTypes", webProperties.getBarcodeTypes());

        // For some initial rendering tweaks
        HashMap<String, Object> initialDeviceDetection = new HashMap<>();
        if (device == null) {
            // Try to resolve the device if it is null (could be the case
            // for ExceptionHandler requests)
            device = new LiteDeviceResolver().resolveDevice(request);
        }
        initialDeviceDetection.put("isDesktop", device.isNormal());
        initialDeviceDetection.put("isTablet", device.isTablet());
        initialDeviceDetection.put("isMobile", device.isMobile());
        appState.put("initialDeviceDetection", initialDeviceDetection);

        // i18n labels
        HashMap<String, Object> i18nState = new HashMap<>();
        appState.put("i18n", i18nState);
        i18nState.put("messages", messagesResourceBundle.getLabels("en"));
        //i18nState.put("currencies", appService.getCurrencies());
        //i18nState.put("languages", appService.getLanguages());

        return state;
    }

}

