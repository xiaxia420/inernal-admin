package de.sportsbook.activbet.state.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@JsonInclude(Include.NON_DEFAULT)
public class PreLiveSelection implements Serializable {

    private SportSelection sport;
    private String filter;
    private String keyWord;
    private Boolean showFavorite;
    private BigDecimal maxOdd;
    private BigDecimal minOdd;

}
