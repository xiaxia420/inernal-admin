package de.sportsbook.activbet.state;

import de.sportsbook.activbet.api.customer.entities.Currency;
import de.sportsbook.activbet.services.user.dtos.UserRolesDto;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Data
public class UserState {
    private String locale;
    private String timezone;
    private Integer timezoneOffsetMinutes;
    private String username;
    private Boolean authenticated;
    private Long loginTime;
    private UserRolesDto userRoles;
    private Integer userCurrencyId;
    private List<Currency> currencies;

    //private String email;
    //private Boolean isDebugMode;
    //private String currency;
    //private Integer currencyDecimalDigits;
    //private Boolean acceptTermsAndConditions;
    //private Boolean forcePasswordChange;
    //private Long automLogoutTime;

    //private Boolean isWebCashier;

    // Do not save the account info in the session and only
    // serialize it if its present.
    //@JsonInclude(Include.NON_DEFAULT)
    //private transient AccountInfoDto accountInfo;

}

