package de.sportsbook.activbet.state.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class SportSelection implements Serializable {

    private Integer id;
    private TipModelSelection tipSelection;
    private List<GameGroupSelection> selectedGameGroups = new ArrayList<>();

}
