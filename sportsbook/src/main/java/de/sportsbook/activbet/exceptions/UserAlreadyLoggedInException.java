package de.sportsbook.activbet.exceptions;

import lombok.Getter;

@Getter
public class UserAlreadyLoggedInException extends Exception {
    private final String message = "error.login.already_logged_in";
}
