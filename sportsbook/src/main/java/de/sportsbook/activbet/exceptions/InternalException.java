package de.sportsbook.activbet.exceptions;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class InternalException extends PayloadException {
    private final String message = "err.internal";
    public InternalException(String traceId) {
        addPayload("traceId", traceId);
    }
}
