package de.sportsbook.activbet.configuration;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.sportsbook.serialization.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.time.ZonedDateTime;
import java.util.EnumSet;

@Slf4j
@Configuration
public class BetagoRestClientConfiguration {

    @Bean
    @Primary // Use this one in case just an "ObjectMapper" is autowired
    // and another one is also present in the classpath
    public ObjectMapper objectMapper() {
        return objectMapperBuilder().build();
    }

    @Bean
    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
        return Jackson2ObjectMapperBuilder.json()
                .deserializerByType(ZonedDateTime.class, new ToUtcDeserializer())
                .deserializerByType(EnumSet.class, new BitFlagEnumDeserializer<>())
                // FlagEnum Deserialization handled via annotation in FlagEnum, see comment there.
                .serializerByType(ZonedDateTime.class, new ToUtcSerializer())
                .serializerByType(EnumSet.class, new BitFlagEnumSerializer())
                .serializerByType(FlagEnum.class, new FlagEnumSerializer<>())
                .serializationInclusion(Include.NON_NULL)
                .featuresToEnable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
                .featuresToDisable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                                   SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

}
