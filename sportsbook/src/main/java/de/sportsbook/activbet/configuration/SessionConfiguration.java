package de.sportsbook.activbet.configuration;

import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.config.annotation.web.http.SpringHttpSessionConfiguration;

@Configuration
public class SessionConfiguration extends SpringHttpSessionConfiguration {

    @Autowired
    private BetagoWebProperties webProperties;

    @Bean
    public CustomRedissionSessionRepository sessionRepository(RedissonClient redissonClient,
                                                              ApplicationEventPublisher eventPublisher,
                                                              BetagoConfiguration configuration) {
        // We're doing it ourselves and not via @EnableRedissonHttpSession to work around some quirks in the default
        // RedissonSessionRepository implementation.
        CustomRedissionSessionRepository repository = new CustomRedissionSessionRepository(redissonClient,
                                                                                           eventPublisher,
                                                                                           configuration.getRedisDatabase());
        if (webProperties.getSessionTimeoutSeconds() > 0) {
            repository.setDefaultMaxInactiveInterval(webProperties.getSessionTimeoutSeconds());
        }
        return repository;
    }

}
