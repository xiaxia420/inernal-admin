package de.sportsbook.activbet.configuration;

import de.sportsbook.activbet.state.SessionAttributes;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class WebSocketSessionUtils {

    @Autowired
    private RedissonClient redisson;

    /**
     * Keep a local map of global websocket-uuids, that identify websocket sessions
     * across all running instances to the local websocket session id that can be
     * used to send messages. We can't keep the local websocket session ids in the
     * sessions as they would result in conflicts if more than one application
     * is running and generating the same ids.
     */
    private ConcurrentHashMap<String, String> wsSessionUuidsToLocalWsIds = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, String> localWsIdsToWsSessionUuids = new ConcurrentHashMap<>();

    public String getWsSessionIdForWsUuid(String wsUuid) {
        return wsSessionUuidsToLocalWsIds.get(wsUuid);
    }

    private RLock getLock(Session session) {
        return redisson.getLock(session.getId() + ".websocket-connections");
    }

    public String addWebSocketSessionToSession(Session session, String wsSessionId) {
        RLock lock = getLock(session);
        try {
            lock.lock();
            Map<String, Long> sessionUuids = session.getAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS);

            if (sessionUuids == null) {
                sessionUuids = new HashMap<>();
            }

            String newUuid = UUID.randomUUID().toString();

            wsSessionUuidsToLocalWsIds.put(newUuid, wsSessionId);
            localWsIdsToWsSessionUuids.put(wsSessionId, newUuid);

            sessionUuids.put(newUuid, System.currentTimeMillis());

            session.setAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS, sessionUuids);

            return newUuid;
        } finally {
            lock.unlock();
        }
    }

    public String removeWebSocketSessionUuidFromSession(Session session, String wsSessionUuid) {
        if (session != null) {
            RLock lock = getLock(session);
            try {
                lock.lock();
                Map<String, Long> sessionUuids = session.getAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS);

                if (sessionUuids != null && wsSessionUuid != null) {
                    sessionUuids.remove(wsSessionUuid);

                    String wsSessionId = wsSessionUuidsToLocalWsIds.get(wsSessionUuid);
                    if (wsSessionId != null) {
                        localWsIdsToWsSessionUuids.remove(wsSessionId);
                    }
                    wsSessionUuidsToLocalWsIds.remove(wsSessionUuid);

                    session.setAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS, sessionUuids);
                }

                return wsSessionUuid;
            } finally {
                lock.unlock();
            }
        }
        return null;
    }

    public String removeWebSocketSessionFromSession(Session session, String wsSessionId) {
        if (session != null) {
            RLock lock = getLock(session);
            try {
                lock.lock();
                Map<String, Long> sessionUuids = session.getAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS);

                String wsSessionUuid = localWsIdsToWsSessionUuids.get(wsSessionId);

                if (sessionUuids != null && wsSessionUuid != null) {
                    sessionUuids.remove(wsSessionUuid);

                    localWsIdsToWsSessionUuids.remove(wsSessionId);
                    wsSessionUuidsToLocalWsIds.remove(wsSessionUuid);

                    session.setAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS, sessionUuids);
                }

                return wsSessionUuid;
            } finally {
                lock.unlock();
            }
        }
        return null;
    }
}
