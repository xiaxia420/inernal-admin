package de.sportsbook.activbet.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.sportsbook.activbet.configuration.CustomRedissionSessionCsrfTokenRepository.CustomCsrfToken;
import de.sportsbook.activbet.state.SessionHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.security.messaging.util.matcher.MessageMatcher;
import org.springframework.security.messaging.util.matcher.SimpMessageTypeMatcher;
import org.springframework.security.web.csrf.CsrfException;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.session.Session;

@Slf4j
public final class CustomCsrfChannelInterceptor extends ChannelInterceptorAdapter {
    private final MessageMatcher<Object> matcher = new SimpMessageTypeMatcher(SimpMessageType.CONNECT);

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        if (!matcher.matches(message)) {
            return message;
        }

        Session session = SessionHolder.getSession(false);
        if (session == null) {
            throw new MissingCsrfTokenException(null);
        }

        String csrfToken = SimpMessageHeaderAccessor.getFirstNativeHeader(CustomRedissionSessionCsrfTokenRepository.HEADER_NAME, message.getHeaders());

        if (csrfToken == null) {
            throw new CsrfException("CSRF Token missing");
        }

        String expectedToken = session.getAttribute(CustomRedissionSessionCsrfTokenRepository.ATTR_NAME);
        if (expectedToken == null) {
            throw new CsrfException("Session has no associated CSRF Token");
        }

        if (expectedToken.equals(csrfToken)) {
            return message;
        }
        try {
            log.error("Invalid CSRF Token (expected:{}) for Session {}", expectedToken, new ObjectMapper().writeValueAsString(session));
        } catch (JsonProcessingException e) {
            log.error("Error logging error", e);
        }
        throw new InvalidCsrfTokenException(new CustomCsrfToken(expectedToken), csrfToken);
    }
}