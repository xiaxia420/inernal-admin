package de.sportsbook.activbet.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.sportsbook.serialization.ToUtcDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.SimpleThreadScope;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.util.StringUtils;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.ResourceUrlEncodingFilter;
import org.springframework.web.servlet.resource.VersionResourceResolver;

import java.time.ZonedDateTime;
import java.util.List;

@Slf4j
@Configuration
public class BetagoConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private VersionProperties versionProperties;

    @Autowired
    private Environment env;

    @Autowired
    private BetagoWebProperties properties;

    Integer getRedisDatabase() {
        return Integer.valueOf(env.getProperty("spring.redis.database"));
    }

    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();

        String host = env.getProperty("spring.redis.host");
        String port = env.getProperty("spring.redis.port");
        String address = "redis://" + host + ":" + port;
        String password = env.getProperty("spring.redis.password");

        SingleServerConfig ssc = config.useSingleServer().setAddress(address).setDatabase(getRedisDatabase());
        if (StringUtils.hasText(password)) {
            ssc.setPassword(password);
        }

        return Redisson.create(config);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**")
                .addResourceLocations("classpath:/static/themes/" + properties.getTheme() + "/", "classpath:/static/")
                .setCachePeriod(60 * 60 * 24 * 365)
                .resourceChain(!env.acceptsProfiles("local"))
                .addResolver(new VersionResourceResolver().addFixedVersionStrategy(versionProperties.getRevision(), "/**"));
    }

    /**
     * Converts the resource-urls generated with <@spring:url>
     * to ones matching the resource-resolver version strategy.
     */
    @Bean
    public ResourceUrlEncodingFilter resourceUrlEncodingFilter() {
        return new ResourceUrlEncodingFilter();
    }

    @Bean
    public static BeanFactoryPostProcessor beanFactoryPostProcessor() {
        return new CustomScopeRegisteringBeanFactoryPostProcessor();
    }

    public static class CustomScopeRegisteringBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
        @Override
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
            beanFactory.registerScope("thread", new SimpleThreadScope());
        }
    }

    @Bean
    public DeviceResolverHandlerInterceptor deviceResolverHandlerInterceptor() {
        return new DeviceResolverHandlerInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(deviceResolverHandlerInterceptor());
    }

    @Bean
    public DeviceHandlerMethodArgumentResolver deviceHandlerMethodArgumentResolver() {
        return new DeviceHandlerMethodArgumentResolver();
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(deviceHandlerMethodArgumentResolver());
    }

    @Bean(name = "frontendObjectMapper")
    public ObjectMapper frontendObjectMapper() {
        return Jackson2ObjectMapperBuilder.json()
                .deserializerByType(ZonedDateTime.class, new ToUtcDeserializer())
                .featuresToEnable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
                .featuresToDisable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .build();
    }

    @Bean
    public MessagesResourceBundle messagesResourceBundle() {
        return new MessagesResourceBundle();
    }

}
