package de.sportsbook.activbet.configuration;

import de.sportsbook.activbet.state.SessionHolder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Slf4j
@Component
public class WebSocketEventListener {

    @Autowired
    private WebSocketSessionUtils webSocketSessionUtils;

    @EventListener
    public void onSessionConnected(SessionConnectEvent event) {

        Session session = SessionHolder.getSession(false);

        if (session == null) {
            // The user has no valid session (invalid request or stale)
            log.info("Invalid WebSocket connection without valid session connected.");
            return;
        }

        String wsSessionId = SimpMessageHeaderAccessor.getSessionId(event.getMessage().getHeaders());

        String wsSessionUuid = webSocketSessionUtils.addWebSocketSessionToSession(session, wsSessionId);
        if (event.getMessage() != null && event.getMessage().getPayload() != null) {
            MDC.put("message_payload", new String(event.getMessage().getPayload()));
        }
        log.debug("Adding WebSocketSession {} (UUID: {}) to HTTP Session", wsSessionId, wsSessionUuid);
        MDC.clear();
    }

    @EventListener
    public void onSessionDisconnect(SessionDisconnectEvent event) {
        Session session = SessionHolder.getSession(false);

        String wsSessionId = SimpMessageHeaderAccessor.getSessionId(event.getMessage().getHeaders());
        String wsSessionUuid = webSocketSessionUtils.removeWebSocketSessionFromSession(session, wsSessionId);

        if (event.getMessage() != null && event.getMessage().getPayload() != null) {
            MDC.put("message_payload", new String(event.getMessage().getPayload()));
        }
        MDC.put("close_status", String.valueOf(event.getCloseStatus()));
        log.debug("Removing WebSocketSession {} (UUID: {}) from HTTP Session", wsSessionId, wsSessionUuid);
        MDC.clear();
    }
    
}
