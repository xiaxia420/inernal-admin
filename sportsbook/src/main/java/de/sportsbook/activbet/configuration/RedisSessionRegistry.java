package de.sportsbook.activbet.configuration;

import de.sportsbook.activbet.api.oauth.enums.LogoutReason;
import de.sportsbook.activbet.services.oauth.OauthService;
import de.sportsbook.activbet.websocket.actions.ErrorAction;
import de.sportsbook.activbet.websocket.controllers.MessageControllerAdvice;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMap;
import org.redisson.api.RSetMultimap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.session.events.SessionDeletedEvent;
import org.springframework.session.events.SessionExpiredEvent;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Set;

@Slf4j
public class RedisSessionRegistry {

    private final RSetMultimap<String, String> userSessions;
    private final RMap<String, String> sessionUsers;

    /**
     * Keep a local map of sessionIds to connected webSocketSessionIds on this node
     * to be able to send SESSION_EXPIRED messages if the users session expires.
     **/
    private final MultiValueMap<String, String> sessionWebSocketSessionIds = new LinkedMultiValueMap<>();

    @Autowired
    private WebSocketActionUtils webSocketActionUtils;

    @Autowired
    private OauthService oAuthService;

    public RedisSessionRegistry(RedissonClient redisson) {
        userSessions = redisson.getSetMultimap("user-sessions");
        sessionUsers = redisson.getMap("session-users");
    }

    public void registerNewSession(String username, String sessionId) {
        userSessions.put(username, sessionId);
        sessionUsers.put(sessionId, username);
    }

    public void removeSession(String sessionId) {
        String username = sessionUsers.get(sessionId);
        if (username != null) {
            userSessions.remove(username, sessionId);
        }
        sessionUsers.remove(sessionId);
        sessionWebSocketSessionIds.remove(sessionId);
    }

    public Set<String> getAllSessionIds(String username) {
        return userSessions.getAll(username);
    }

    /*
        REDIS Keyspace Events must be properly configured for this to work.
        Minimum:
        redis-cli:
        ~ CONFIG SET notify-keyspace-events gxE
     */

    @EventListener
    public void handleSessionDeletedEvent(SessionDeletedEvent event) {
        removeSession(event.getSessionId());
    }

    @EventListener
    public void handleSessionExpiredEvent(SessionExpiredEvent event) {
        List<String> webSocketSessionIds = sessionWebSocketSessionIds.get(event.getSessionId());
        if (webSocketSessionIds != null) {
            ErrorAction errorAction = new ErrorAction();
            errorAction.setError(MessageControllerAdvice.SESSION_EXPIRED);
            webSocketActionUtils.broadcastToUser(webSocketSessionIds, errorAction);
        }

        String userId = sessionUsers.get(event.getSessionId());
        if (userId != null) {
            // Only send a logout for a session that is logged in.
            //logoutLogService.logLogout(userId, null, null, OauthApi.LogoutReason.SESSION_EXPIRED);
            oAuthService.logout(userId, null, null, LogoutReason.SESSION_EXPIRED);
        }
        removeSession(event.getSessionId());
    }

    public void addWebSocketSession(String sessionId, String webSocketSessionId) {
        // Comes from the WebSocketSessionRegistry
        sessionWebSocketSessionIds.add(sessionId, webSocketSessionId);
    }

    public void removeWebSocketSession(String sessionId, String webSocketSessionId) {
        sessionWebSocketSessionIds.remove(sessionId, webSocketSessionId);
    }

}