package de.sportsbook.activbet.configuration;

import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RTopic;
import org.redisson.api.listener.MessageListener;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.WebSocketHandlerDecorator;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class WebSocketSessionRegistry extends WebSocketHandlerDecorator implements MessageListener<String> {

    public static final String FORCE_CLOSE_WEBSOCKET_TOPIC = "force-close-websocket";
    public static final String FORCE_CLOSE_SUCCESSFUL_WEBSOCKET_TOPIC = "force-close-successful-websocket";

    private final WebSocketSessionUtils webSocketSessionUtils;
    private final RTopic<String> topic;
    private final RTopic<String> successTopic;
    private final RedisSessionRegistry redisSessionRegistry;
    private final int listenerId;

    private Map<String, WebSocketSession> sessions = new ConcurrentHashMap<>();

    public WebSocketSessionRegistry(WebSocketHandler delegate, WebSocketSessionUtils webSocketSessionUtils, Redisson redisson,
                                    RedisSessionRegistry redisSessionRegistry) {
        super(delegate);
        this.webSocketSessionUtils = webSocketSessionUtils;
        topic = redisson.getTopic(FORCE_CLOSE_WEBSOCKET_TOPIC);
        successTopic = redisson.getTopic(FORCE_CLOSE_SUCCESSFUL_WEBSOCKET_TOPIC);
        listenerId = topic.addListener(this);
        this.redisSessionRegistry = redisSessionRegistry;
    }

    @PreDestroy
    public void removeListener() {
        topic.removeListener(listenerId);
    }

    @Override
    public void onMessage(String channel, String wsSessionUuid) {
        String wsSessionId = webSocketSessionUtils.getWsSessionIdForWsUuid(wsSessionUuid);
        if (wsSessionId == null) {
            return;
        }

        WebSocketSession session = sessions.get(wsSessionId);
        if (session == null) {
            return;
        }

        log.info("Force closing websocket session {} (UUID: {})", wsSessionId, wsSessionUuid);
        try {
            session.close(CloseStatus.SERVICE_OVERLOAD);
            successTopic.publish(wsSessionUuid);
        } catch (IOException e) {
            log.error("Error force closing websocket session", e);
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        redisSessionRegistry.addWebSocketSession(getRedisSessionId(session), session.getId());
        sessions.put(session.getId(), session);
        super.afterConnectionEstablished(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        redisSessionRegistry.removeWebSocketSession(getRedisSessionId(session), session.getId());
        sessions.remove(session.getId());
        super.afterConnectionClosed(session, closeStatus);
    }

    private String getRedisSessionId(WebSocketSession session) {
        return (String) session.getAttributes().get("SPRING.SESSION.ID");
    }
}
