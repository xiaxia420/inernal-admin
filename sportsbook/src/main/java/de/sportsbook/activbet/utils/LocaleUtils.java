package de.sportsbook.activbet.utils;

import de.sportsbook.activbet.api.app.entities.Language;
import de.sportsbook.activbet.configuration.BetagoWebProperties;
import de.sportsbook.activbet.services.app.AppService;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.state.SessionHolder;
import org.apache.commons.codec.language.bm.Lang;
import org.springframework.session.Session;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LocaleUtils {

    /*
    public static Language getLanguage(String locale) {
        String browserLanguage = locale.split("(-|_)")[0];
        return getLanguageByCode(browserLanguage);
        //Session session = SessionHolder.getSafeSession();
        //Integer defaultLanguageId = session.getAttribute(SessionAttributes.DEFAULT_LANGUAGE_ID);
        //return getLanguageById(defaultLanguageId);
    }
     */

    /*
    public static Language getLanguageById(Integer languageId) {
        List<Language> languages = ContextUtils.getBean(AppService.class).getLanguages();
        return languages.stream().filter(language -> Objects.equals(language.getId(), languageId)).findFirst().orElse(null);
    }
     */

    /*
    public static Language getLanguageByCode(String code) {
        List<Language> languages = ContextUtils.getBean(AppService.class).getLanguages();
        Language lang = languages.stream().filter(language -> Objects.equals(language.getCode(), code)).findFirst().orElse(null);
        if (Objects.isNull(lang)) {
            if (CollectionUtils.isEmpty(languages)) {
                Language en = new Language();
                en.setId(1);
                en.setName("English");
                en.setCode("en");
                return en;
            } else {
                return languages.get(0);
            }
        } else {
            return lang;
        }
    }
     */

    /*
    public static Language getDefaultLanguage() {
        List<Language> languages = ContextUtils.getBean(CacheAllLanguageService.class).getAllCachedLanguages();
        // return en as default language
        return languages.stream().filter(lang -> Objects.equals("en", lang.getIso2())).findAny().get();
    }
    */

    /*
    public static Currency getDefaultCurrency(String languageCode) {
        return ContextUtils.getBean(CacheDefaultCurrenciesService.class).getCachedCurrency(languageCode);
    }
     */

    public static String validateLocale(String locale) {
        //return getLanguage(locale).getCode();
        if (!StringUtils.isEmpty(locale)) {
            if (locale.length() == 2) {
                return locale;
            }
        }
        return null;
    }

    public static String validateTimezone(String timezone) {
        return getTimezoneIds().stream()
                .filter(tz -> tz.equalsIgnoreCase(timezone))
                .findAny()
                .orElseGet(() -> ContextUtils.getBean(BetagoWebProperties.class).getDefaultTimezone());
    }

    /*
    public static Currency getCurrency(String currencyCode, String languageCode) {
        //
        return ContextUtils.getBean(CacheCurrencyService.class).getCachedCurrencies(currencyCode, languageCode);
    }
     */

    /*
    public static Currency getDefaultCurrency(Language language) {
        List<Currency> currencies = ContextUtils.getBean(CacheCurrencyService.class).getCachedCurrencies(language);
        return currencies.stream().filter(currency -> currency.getIsDefault()).findFirst().orElse(null);
    }
    */

    /*
    public static ZonedDateTime currentTimeUtc() {
        return ZonedDateTime.now(Clock.systemUTC());
    }
     */

    /*
    public static ZonedDateTime currentTimeForUser() {
        return ZonedDateTime.now(ZoneId.of(SessionHolder.getSafeSession().getAttribute(SessionAttributes.TIMEZONE)));
    }
     */


    public static String getDateTimeForUser(ZonedDateTime dateTime) {
        try {
            //return dateTime.withZoneSameInstant(ZoneId.of(SessionHolder.getSafeSession().getAttribute(SessionAttributes.TIMEZONE))).toLocalDateTime().toString() + "Z";
            return dateTime.withZoneSameInstant(ZoneId.of(SessionHolder.getSafeSession().getAttribute(SessionAttributes.TIMEZONE))).toLocalDateTime().toString();
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return "";
    }

    public static List<String> getTimezoneIds() {
        return Stream.of(TimeZone.getAvailableIDs())
                .filter(id -> id.matches("^(Africa|America|Asia|Atlantic|Australia|Europe|Indian|Pacific)/.*"))
                .collect(Collectors.toList());
    }

    public static DateTimeFormatter UtcZonedDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("UTC"));

    /*
    public static String getDateForUser(ZonedDateTime dateTime) {
        try {
            LocalDateTime local = dateTime.withZoneSameInstant(ZoneId.of(SessionHolder.getSafeSession().getAttribute(SessionAttributes.TIMEZONE))).toLocalDateTime();
            return local.toLocalDate().toString();
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return "";
    }
     */


    public static String getDateTimeForLocal(String timezone, String dateStr) {
        ZoneId zoneId;
        try {
            zoneId = ZoneId.of(timezone);
        } catch (Exception e) {
            zoneId = ZoneId.of("UTC");
        }

        if (!StringUtils.isEmpty(dateStr)) {
            dateStr = dateStr.replaceAll("Z", "");
            dateStr = dateStr.replaceAll("z", "");

            LocalDateTime localFromDate = LocalDateTime.parse(dateStr);
            ZonedDateTime zonedDateTime = ZonedDateTime.of(localFromDate, zoneId);
            String str = zonedDateTime.format(UtcZonedDateTimeFormatter);
            return str;
        } else {
            return null;
        }
    }

}
