package de.sportsbook.activbet.utils;

import com.moodysalem.TimezoneMapper;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Slf4j
public class RequestUtils {

    public static String getRemoteAddress(HttpServletRequest request) {
        String address = request.getHeader("X-Forwarded-For");
        if (address == null) {
            address = request.getRemoteAddr();
        }
        return address;
    }

    public static String getUserAgent(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        if (Objects.nonNull(userAgent)) {
            if (userAgent.length() > 1024) {
                userAgent = userAgent.substring(0, 1024);
            }
        }
        return userAgent;
    }

    public static String getTimeZone(HttpServletRequest request) {
        String timezone = null;

        try {
            // Coming from the Apache with GeoIP addon
            String latHeader = request.getHeader("GEOIP_LATITUDE");
            String lonHeader = request.getHeader("GEOIP_LONGITUDE");

            if (latHeader != null && lonHeader != null) {
                double lat = Double.parseDouble(latHeader);
                double lon = Double.parseDouble(lonHeader);

                timezone = TimezoneMapper.tzNameAt(lat, lon);
            }
        } catch (Exception e) {
            log.warn("Could not get user timezone from geo ip location headers", e);
        }

        if (timezone == null) {
            // Fallback if the TimezoneMapper didn't work.
            timezone = "Europe/Berlin";
        }

        return timezone;
    }
}
