package de.sportsbook.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.time.ZonedDateTime;

/**
 * Helper class to deserialize LocalDateTimes to ZonedDateTimes with UTC timezone.
 */
public class ToUtcDeserializer extends JsonDeserializer<ZonedDateTime> {

    @Override
    public ZonedDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {

        String stringValue = p.getValueAsString();

        // avoid empty string
        if (StringUtils.hasText(stringValue)) {
            if (!stringValue.matches(".*([+-][0-2]\\d:[0-5]\\d|Z)$")) {
                // The time does not contain a timezone, handle as UTC
                stringValue += "Z";
            }
            return ZonedDateTime.parse(stringValue);
        }
        return null;
    }

}
