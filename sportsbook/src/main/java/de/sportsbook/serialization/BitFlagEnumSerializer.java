package de.sportsbook.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.EnumSet;

public class BitFlagEnumSerializer extends JsonSerializer<EnumSet> {

    @SuppressWarnings("unchecked")
    @Override
    public void serialize(EnumSet value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(BitFlagEnum.toFlag(value));
    }
}
