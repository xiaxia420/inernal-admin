const {resolve} = require('path');
const webpack = require("webpack");
const WebpackConfig = require("webpack-config").Config;
const CircularDependencyPlugin = require("circular-dependency-plugin");
const {getBabelLoaderRule} = require("./webpack.utils");

module.exports = new WebpackConfig()
    .extend("./webpack.config.base.js")
    .merge({
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: [{
                        loader: "style-loader" // creates style nodes from JS strings
                    }, {
                        loader: "css-loader", // translates CSS into CommonJS
                        options: {
                            sourceMap: true
                        }
                    }, {
                        loader: "sass-loader", // compiles Sass to CSS
                        options: {
                            sourceMap: true
                        }
                    }]
                },
                {
                    test: /\.(png|jpg|gif|svg|ttf|woff|woff2|eot)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {}
                        }
                    ]
                },
                getBabelLoaderRule(false)
            ]
        },
        entry: {
            main: [
                "./jsx/styles.js",

                "react-hot-loader/patch",
                // activate HMR for React

                "./jsx/client.dev.js"
                // the entry point of our app
            ]
        },

        // disable devtool
        devtool: "eval",

        devServer: {
            host: "0.0.0.0",
            // noInfo: true,
            //port: 8888,
            port: 9988,
            historyApiFallback: true,

            //inline: true,
            hotOnly: true,
            // hot: true,
            // enable HMR on the server

            overlay: true,
            // Show an error overlay on errors

            contentBase: resolve(__dirname, 'js/bundle'),
            // match the output path

            publicPath: "/resources/js/bundle/",
            // match the output `publicPath`

            proxy: {
                "/messages": {
                    target: "ws://127.0.0.1:8082",
                    ws: true
                },
                "/**": {
                    target: "http://127.0.0.1:8082"
                }
            }
        },
        output: {
            path: resolve(__dirname, 'js/bundle'),
            filename: "[name].bundle.js",
            publicPath: "/resources/js/bundle/"
        },
        plugins: [
            new webpack.NamedModulesPlugin(),
            // prints more readable module names in the browser console on HMR updates

            // Split node_modules into vendor bundle
            new webpack.optimize.CommonsChunkPlugin({
                name: "vendor",
                minChunks: function (module) {
                    // this assumes your vendor imports exist in the node_modules directory
                    return module.context && module.context.indexOf('node_modules') !== -1;
                }
            }),

            new webpack.DefinePlugin({
                '__PRODUCTION__': JSON.stringify(false)
            }),

            new CircularDependencyPlugin({
                exclude: /node_modules/,
                failOnError: true
            }),

            new webpack.HotModuleReplacementPlugin()
        ]
    });