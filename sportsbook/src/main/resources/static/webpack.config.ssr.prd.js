const webpack = require("webpack");
const WebpackConfig = require("webpack-config").Config;

module.exports = new WebpackConfig()
    .extend("./webpack.config.ssr.base.js")
    .merge({
        plugins: [
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify('production'),
                '__PRODUCTION__': JSON.stringify(true)
            })
        ]
    });