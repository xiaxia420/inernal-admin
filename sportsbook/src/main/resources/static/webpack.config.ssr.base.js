const {resolve} = require('path');
const webpack = require("webpack");
const WebpackConfig = require("webpack-config").Config;
const {getBabelLoaderRule} = require("./webpack.utils");

module.exports = new WebpackConfig().merge({
    entry: './jsx/ssr.js',
    target: 'node',
    output: {
        path: resolve(__dirname, '..'),
        filename: 'ssr.min.js'
    },
    module: {
        rules: [
            getBabelLoaderRule(true),
            {
                /* Hide the fake-window variable from the velocity_react package
                 * as the shim relies on the non-presence to skip loading the velocity
                 * package that uses dom-specific behaviour that does not work on the
                 * server */
                loader: 'imports-loader?window=>undefined',
                test: require.resolve("velocity-react/lib/velocity-animate-shim")
            }
        ]
    },
    plugins: [
        // Only load necessary momentjs locales
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|en/),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true
            },
            compress: {
                warnings: false,
                screw_ie8: true
            },
            comments: false
        })
    ]
});