const webpack = require("webpack");
const WebpackConfig = require("webpack-config").Config;
const {getBabelLoaderRule} = require("./webpack.utils");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    filename: "[name].css"
});

module.exports = new WebpackConfig()
    .extend("./webpack.config.client.base.js")
    .merge({
        module: {
            rules: [
                {
                    test: /\.(png|jpg|gif|svg|ttf|woff|woff2|eot)$/,
                    use: [
                        {
                            loader: 'file-loader'
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: extractSass.extract({
                        use: [
                            {
                                loader: "css-loader",
                                options: {
                                    sourceMap: false,
                                    minimize: true
                                }
                            },
                            {
                                loader: "postcss-loader"
                            },
                            {
                                loader: "sass-loader",
                                options: {
                                    sourceMap: false
                                }
                            }
                        ]
                    })
                },
                getBabelLoaderRule(false)
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify('production'),
                '__PRODUCTION__': JSON.stringify(false)
            }),
            extractSass
        ],

        // disable devtool
        devtool: "source-map"
    });