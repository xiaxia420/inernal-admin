import React, {Component} from "react";
import ReactDOM from "react-dom";
import Root from "./components/Root";
import {store, history} from "./setup";
import {AppContainer} from "react-hot-loader";

const render = () => {
    ReactDOM.render(
        <AppContainer>
            <Root store={store} history={history}/>
        </AppContainer>,
        document.getElementById('app')
    );
};

render();

// Hot Module Replacement API
if (module.hot) {
    module.hot.accept('./components/Root', () => {
        render();
    });
}