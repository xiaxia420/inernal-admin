import {IntlProvider, addLocaleData} from "react-intl";
import {connect} from "react-redux";
import en from "react-intl/locale-data/en";
import de from "react-intl/locale-data/de";
import zh from "react-intl/locale-data/zh";
import tr from "react-intl/locale-data/tr";
import it from "react-intl/locale-data/it";
import ru from "react-intl/locale-data/ru";
import es from "react-intl/locale-data/es";
import am from "react-intl/locale-data/am";
import el from "react-intl/locale-data/el";
import fr from "react-intl/locale-data/fr";
import React, {Component} from "react";

/* Add possible locales here */
addLocaleData(en);
addLocaleData(de);
addLocaleData(zh);
addLocaleData(tr);
addLocaleData(it);
addLocaleData(ru);
addLocaleData(es);
addLocaleData(am);
addLocaleData(el);
addLocaleData(fr);

class I18NProvider extends Component {

    render() {
        const {messages, children} = this.props;
        let {locale} = this.props;

        if (locale === "en") {
            locale = "en-GB";
        }

        return (
            <IntlProvider locale={ locale } messages={ messages }>
                { children }
            </IntlProvider>
        )
    }

}

function mapStateToProps(state) {
    const {locale, timezone} = state.user;
    const {messages} = state.app.i18n;
    return {
        locale,
        messages,
        timezone
    }
}

export default connect(mapStateToProps)(I18NProvider);