import React, {Component} from "react";
import {connect} from "react-redux";
import {Alert, Button, Card, CardBody, CardTitle, Col, Row} from "reactstrap";
import {requestPayOutRequestNotification} from "../../../actions/notification";
import Indicator from "../../Module/Indicator";

class PayOutRequestNotification extends Component {

    componentDidMount() {
        // refresh every 10 minute
        this.requestNotification = () => {
            this.props.requestPayOutRequestNotification()
                .then(
                    () => {
                        setTimeout(() => {
                            if (this.requestNotification) {
                                this.requestNotification();
                            }
                        }, 10 * 60 * 1000);
                    }
                )
        };
        this.requestNotification();
    }

    componentWillUnmount() {
        delete this.requestNotification;
    }

    render() {
        const {isLoading, notification} = this.props;
        let message = "";
        let isHighlight = false;

        if (isLoading) {
            message = "loading ...";
        } else {
            if (notification) {
                isHighlight = (notification.needReleaseCount > 0);
                message = `${notification.needReleaseCount} shop pay out requests, total ${notification.needReleaseVolume} Birr to be released`;
            }
        }

        return (
            <div>
                |
                <span style={{paddingLeft: "10px", paddingRight: "10px"}}>
                    <a className={isHighlight ? "alert-info" : ""} href="/account/payout_requests">
                        {message}
                    </a>
                </span>
                |
            </div>
        )
    }
}

function mapStateToProps(state) {
    const {isLoading, notification} = state.notification.payOutRequestNotification;
    return {
        isLoading, notification
    }
}

const mapDispatchToProps = {
    requestPayOutRequestNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(PayOutRequestNotification);
