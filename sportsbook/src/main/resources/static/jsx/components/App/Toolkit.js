import React, {Component} from "react";
import classNames from "classnames";
import {FormattedDate} from "react-intl";

export function getPopupDimensions(width, height) {
    const wLeft = window.screenLeft || window.screenX;
    const wTop = window.screenTop || window.screenY;

    const left = wLeft + window.innerWidth / 2 - width / 2;
    const top = wTop + window.innerHeight / 2 - height / 2;

    return 'width=' + width + ',height=' + height + ',top=' + top + ',left=' + left;
}

export const ContainerFluidSpacious = ({className, children}) => (
    <div className={classNames("container-fluid-spacious", className)}>
        {children}
    </div>
);

export function validateInt(value) {
    return value.replace(/[^0-9]/, "").substr(0, 9);
}

export function formatUserDate(date) {
    if (!date) {
        return "";
    }

    return (<FormattedDate value={date}
                   day="2-digit"
                   month="2-digit"
                   year="numeric"
                   hour="2-digit"
                   minute="2-digit"
                   second="2-digit"
                   hour12={false}
    />);
}

export const Currency = ({value, currencyIso = null, decimalDigits = 2}) => {
    if (typeof value !== "number") {
        return null;
    }

    return value.toLocaleString("de", {
        //style: "currency",
        //currency: currencyIso ? currencyIso : "EUR",
        minimumFractionDigits: decimalDigits,
        maximumFractionDigits: decimalDigits,
    })
};

export const Percent = ({value}) => {
    if (typeof value !== "number") {
        return null;
    }
    return (value / 100).toLocaleString("de", {
        style: "percent",
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    })
};

export const prepareStake = (stake) => {
    if (!stake) {
        return null;
    }
    stake = stake.toString();

    if (stake.length > 9) {
        stake = stake.substring(0, 9);
    }

    stake = stake.replace(",", ".");
    stake = stake.replace(/[^\d.-]/g, '');

    if (isNaN(stake)) {
        return; // undefined as special value to keep the current stake
    }

    return stake;
};

export const Odd = ({value, isWon, isLost}) => {
    if (typeof value !== "number") {
        return null;
    }

    const formattedValue = value.toLocaleString("de", {minimumFractionDigits: 2, maximumFractionDigits: 2});
    if (isWon) {
        return <span className="text-success">{formattedValue}</span>
    }
    if (isLost) {
        return <span className="text-danger">{formattedValue}</span>
    }

    return formattedValue;
};

export const getSystemsText = (ticket) => {
    let text = "";
    if (ticket.systems) {
        if (ticket.systems.length > 0) {
            if (ticket.systems[0].bankCount > 0) {
                text = `${ticket.systems[0].bankCount}B + `
            }

            ticket.systems.forEach(
                (system, i) => {
                    text = text + system.minSystem + (i < ticket.systems.length-1 ? "," : "");
                }
            );
            text = text + " of " + ticket.systems[0].maxSystem
        }

    }
    return text;
};

export const timeDifferenceInMinutes = (dt1, dt2) => {
    const diffMs = dt2 - dt1;
    //return Math.round(((diffMs % 86400000) % 3600000) / 60000);
    return Math.round(diffMs / 1000 / 60 );


};
