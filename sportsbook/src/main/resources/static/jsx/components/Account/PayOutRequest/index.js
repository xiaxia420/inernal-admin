import React, {Component} from "react";
import {ContainerFluidSpacious, Currency, formatUserDate} from "../../App/Toolkit";
import {connect} from "react-redux";
import {requestCompanyListIfNeeded} from "../../../actions/company";
import {requestAllWebCashiersIfNeeded} from "../../../actions/customer";
import {requestPayOutRequests, resetPayOutRequestsFilter, clearPayOutRequests,
    requestAcceptPayOutRequest, requestDenyPayOutRequest} from "../../../actions/payment";
import {requestPayOutRequestNotification} from "../../../actions/notification";
import {showGeneralError} from "../../../actions/app";
import {
    Button,
    ButtonGroup,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    InputGroup, InputGroupAddon, InputGroupText,
    Row,
    UncontrolledDropdown, UncontrolledTooltip
} from "reactstrap";
import CompaniesDropDown from "../../Company/CompaniesDropDown";
import CashiersDropDown from "./CashiersDropDown";
import PaymentDirectionDropDown from "../PaymentTransaction/PaymentDirectionDropDown";
import {DateTimePicker} from "react-widgets";
import PageListPagination from "../../App/PageListPagination";
import Indicator from "../../Module/Indicator";
import {parse} from "qs";

export const PAY_OUT_REQUEST_STATUS = [
    {id: 0, name: "OPEN"},
    {id: 1, name: "PAID_OUT"},
    {id: 91, name: "CANCELLED_BY_USER"},
    {id: 92, name: "CANCELLED_BY_TIMEOUT"}
];

export const PAY_OUT_REQUEST_FLAG = [
    {id: 0, name: "NONE"},
    {id: 1, name: "NEED_RELEASE"},
];

class PayOutRequest extends Component {

    state = {
        isHandling : false
    };

    componentDidMount() {
        this.props.requestCompanyListIfNeeded();
        this.props.requestAllWebCashiersIfNeeded();

        /*
        http://localhost:9988/account/payout_requests?username=xiaxi421&popup
        */
        const search = this.props.location;
        if (search) {
            const query = parse(location.search.substr(1));
            const username = query.username;
            if (username) {
                const {payOutRequestsFilter} = this.props;
                this.props.resetPayOutRequestsFilter(
                    {
                        ...payOutRequestsFilter,
                        customerUsername: username
                    }
                ).then(
                    () => {
                        const {pageIndex, pageSize} = this.props.payOutRequestPagination;
                        this.props.requestPayOutRequests(pageIndex, pageSize);
                    }
                );
            }
        }
    }

    onCompanyChange = (id) => {
        const {payOutRequestsFilter} = this.props;
        this.props.resetPayOutRequestsFilter(
            {
                ...payOutRequestsFilter,
                companyId: id
            }
        )
    };

    onCashierChange = (userId) => {
        const {payOutRequestsFilter} = this.props;
        this.props.resetPayOutRequestsFilter(
            {
                ...payOutRequestsFilter,
                payOutUserId: userId
            }
        )
    };

    onCustomerUsernameChange = (e) => {
        const {payOutRequestsFilter} = this.props;
        this.props.resetPayOutRequestsFilter(
            {
                ...payOutRequestsFilter,
                customerUsername: e.target.value
            }
        )
    };

    onPayOutStatusChange = (id) => {
        const {payOutRequestsFilter} = this.props;
        this.props.resetPayOutRequestsFilter(
            {
                ...payOutRequestsFilter,
                status: id
            }
        )
    };

    onPayOutRequestFlagChange = (id) => {
        const {payOutRequestsFilter} = this.props;
        this.props.resetPayOutRequestsFilter(
            {
                ...payOutRequestsFilter,
                flag: id
            }
        )
    };

    onFromChange = (from) => {
        const {payOutRequestsFilter} = this.props;
        this.props.resetPayOutRequestsFilter(
            {
                ...payOutRequestsFilter,
                from: from
            }
        )
    };

    onToChange = (to) => {
        const {payOutRequestsFilter} = this.props;
        this.props.resetPayOutRequestsFilter(
            {
                ...payOutRequestsFilter,
                to: to
            }
        )
    };

    searchPayOutRequests = () => {
        const {pageIndex, pageSize} = this.props.payOutRequestPagination;
        this.props.requestPayOutRequests(pageIndex, pageSize);
    };

    clearPayOutRequests = () => {
        this.props.clearPayOutRequests();
    };

    onChangePageAction = (pageIndex, pageSize) => {
        this.props.requestPayOutRequests(pageIndex, pageSize);
    };

    acceptPayOutRequest = (id) => {
        this.setState({
            isHandling: true
        });
        this.props.requestAcceptPayOutRequest(id).then(
            () => {
                this.setState({
                    isHandling: false
                });
                this.searchPayOutRequests();
                this.props.requestPayOutRequestNotification();
            }
        ).catch(
            (error) => {
                this.setState({
                    isHandling: false
                });
                this.props.showGeneralError(error);
            }
        )
    };

    denyPayOutRequest = (id) => {
        this.setState({
            isHandling: true
        });
        this.props.requestDenyPayOutRequest(id).then(
            () => {
                this.setState({
                    isHandling: false
                });
                this.searchPayOutRequests();
                this.props.requestPayOutRequestNotification();
            }
        ).catch(
            (error) => {
                this.setState({
                    isHandling: false
                });
                this.props.showGeneralError(error);
            }
        )
    };

    render() {
        const {payOutRequests, payOutRequestsFilter, companyList, cashierList, payOutRequestPagination} = this.props;
        const {isLoading, payOuts} = payOutRequests;
        const {companyId, payOutUserId, customerUsername, status, from, to, flag} = payOutRequestsFilter;
        const {isHandling} = this.props;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-titles">
                                <h3 className="dashhead-title">Pay Out Requests</h3>
                            </div>

                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* companies */}
                                <div className="ml-3">
                                    <CompaniesDropDown id={companyId}
                                                       addPlaceholder={true}
                                                       placeholder="Select company"
                                                       list={companyList}
                                                       listLoading={isLoading}
                                                       selectionChanged={this.onCompanyChange}/>

                                </div>

                                {/* cashier */}
                                <div className="ml-3">
                                    <CashiersDropDown id={payOutUserId}
                                                       addPlaceholder={true}
                                                       placeholder="Select cashier"
                                                       list={cashierList}
                                                       listLoading={isLoading}
                                                       selectionChanged={this.onCashierChange}/>

                                </div>



                                {/* status */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={status}
                                                              addPlaceholder={true}
                                                              placeholder="Select status"
                                                              list={PAY_OUT_REQUEST_STATUS}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onPayOutStatusChange}/>
                                </div>

                                {/* release flag */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={flag}
                                                              addPlaceholder={true}
                                                              placeholder="Select flag"
                                                              list={PAY_OUT_REQUEST_FLAG}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onPayOutRequestFlagChange}
                                    />
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* keyword */}

                                {/* customer username */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="icon icon-magnifying-glass"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Customer username"
                                               value={customerUsername}
                                               onChange={this.onCustomerUsernameChange}
                                        />
                                    </InputGroup>
                                </div>

                                {/* from */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={from} format="L LT" placeholder="From" onChange={this.onFromChange}/>
                                </div>

                                {/* to */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={to} format="L LT" placeholder="To" onChange={this.onToChange}/>
                                </div>

                                <div className="ml-3">
                                    <Button color="primary"
                                            disabled={isLoading}
                                            onClick={this.searchPayOutRequests}
                                    >
                                        Search
                                    </Button>
                                </div>

                                <div className="ml-3">
                                    <Button color="dark"
                                            onClick={this.clearPayOutRequests}
                                    >
                                        Clear
                                    </Button>
                                </div>

                            </div>
                        </div>
                    </Col>
                </Row>

                {
                    true == isLoading &&
                    <div className="text-center mt-5">
                        <Indicator animate className="loading-indicator"/>
                        <div className="dashhead-subtitle mt-3">Loading</div>
                    </div>
                }

                {
                    payOuts && payOuts.length > 0 &&
                    <Row className="mt-3">
                        <Col>
                            <table className="table table-striped">
                                <thead className="thead-xs">
                                <tr>
                                    <th>Id</th>
                                    <th>C. Id</th>
                                    <th>Username</th>
                                    <th>Value</th>
                                    <th>PIN</th>
                                    <th>Status</th>
                                    <th>Expire At</th>
                                    <th>Insert At</th>
                                    <th>Update At</th>
                                    <th style={{width: "1rem"}}>Release</th>

                                    <th style={{width: "1rem"}}>
                                        {/* Accept Button */}
                                    </th>

                                    <th style={{width: "1rem"}}>
                                        {/* Deny Button */}
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                {
                                    payOuts && payOuts.map(payOut =>
                                    {
                                        const {id, customerId, customerUsername, value, pinCode, payOutRequestStatus, payOutRequestFlag,
                                            expireTime, insertTime, updateTime} = payOut;
                                        const needRelease = (payOutRequestFlag == "NEED_RELEASE");

                                        return (
                                            <tr key={id} style={{color: needRelease ? "#7fffd4":null}}>
                                                <td><span>{id}</span></td>
                                                <td><span>{customerId}</span></td>
                                                <td><span>{customerUsername}</span></td>
                                                <td><span><Currency value={value} /></span></td>
                                                <td><span>{pinCode}</span></td>
                                                <td><span>{payOutRequestStatus}</span></td>
                                                <td><span>{formatUserDate(expireTime)}</span></td>
                                                <td><span>{formatUserDate(insertTime)}</span></td>
                                                <td>{formatUserDate(updateTime)}</td>

                                                <td>{needRelease ? <i className="icon icon-check"/> : null}</td>

                                                <td className="p-1 text-center">
                                                {
                                                    needRelease &&
                                                    <Button id={"Accept" + id}
                                                            className="p-1"
                                                            color="outline-primary"
                                                            style={{width: "2rem", height: "2rem"}}
                                                            size="sm"
                                                            disabled={true == isHandling}
                                                            onClick={() => {this.acceptPayOutRequest(id)}}
                                                    >
                                                        A
                                                        <UncontrolledTooltip placement="right" target={"Accept" + id}>
                                                            Transfer money
                                                        </UncontrolledTooltip>
                                                    </Button>
                                                }
                                                </td>

                                                <td className="p-1 text-center">
                                                {
                                                    needRelease &&
                                                    <Button id={"Deny" + id}
                                                            className="p-1"
                                                            color="outline-primary"
                                                            style={{width: "2rem", height: "2rem"}}
                                                            size="sm"
                                                            disabled={true == isHandling}
                                                            onClick={() => {this.denyPayOutRequest(payOut.id)}}
                                                    >
                                                        D
                                                        <UncontrolledTooltip placement="right" target={"Deny" + payOut.id}>
                                                            Decline
                                                        </UncontrolledTooltip>
                                                    </Button>
                                                }
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        </Col>
                    </Row>
                }

                {
                    payOuts && payOuts.length > 0 &&
                    <Row>
                        <PageListPagination pageList={payOutRequestPagination}
                                            isLoading={isLoading}
                                            changePageAction={this.onChangePageAction}
                        />
                    </Row>
                }

            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {payOutRequests, payOutRequestsFilter, payOutRequestPagination} = state.payment;
    const {companyList} = state.company;
    const {allCashiers} = state.customer;

    let cashierList = allCashiers;
    const {companyId} = payOutRequestsFilter;
    if (companyId && allCashiers) {
        cashierList = allCashiers.filter(c => c.companyId == companyId);
    }

    return {
        payOutRequests, payOutRequestsFilter, payOutRequestPagination, companyList, cashierList
    };
}

const mapDispatchToProps = {
    requestCompanyListIfNeeded, requestAllWebCashiersIfNeeded, resetPayOutRequestsFilter, requestPayOutRequests, clearPayOutRequests,
    requestAcceptPayOutRequest, requestDenyPayOutRequest, showGeneralError, requestPayOutRequestNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(PayOutRequest);