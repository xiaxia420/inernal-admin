import React, {Component} from "react";
import {connect} from "react-redux";
import {Col} from "reactstrap";
import {Currency} from "../../App/Toolkit";
import Indicator from "../../Module/Indicator";

class CashierReportTable extends Component {

    getCount = (ele) => {
        if (ele) {
            if ( (ele.count !== null) && (typeof ele.count !== "undefined") ) {
                return ele.count;
            }
        }
        return "";
    };

    getValue = (ele) => {
        if (ele) {
            return ele.value;
        }
        return "";
    };

    renderReport = (report, marginTop) => {
        if (!report) {
            return null;
        }

        const {balanceStart, balanceEnd, totalUnpaid, sports, customerBookings, cashierBookings} = report;

        return [
            <table className="table table-striped" key={report.name + "1"} style={{marginTop: marginTop}}>
                <thead className="thead-xs">
                    <tr>
                        <th colSpan={5} className="text-light text-center">{report.name}</th>
                    </tr>
                    <tr>
                        <th className="border-right">Balance Start</th>
                        <th className="border-right">Balance End</th>
                        <th className="border-right">Balance Diff</th>
                        <th className="border-right">Unpaid Count</th>
                        <th>Unpaid</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td className="border-right border-bottom"><Currency value={balanceStart} /></td>
                        <td className="border-right border-bottom"><Currency value={balanceEnd} /></td>
                        <td className="border-right border-bottom"><Currency value={balanceEnd - balanceStart} /></td>
                        <td className="border-right border-bottom">{this.getCount(totalUnpaid)}</td>
                        <td className="border-bottom"><Currency value={this.getValue(totalUnpaid)} /></td>
                    </tr>
                </tbody>
            </table>,
            <table className="table table-striped" key={report.name + "2"} style={{marginTop: "-1rem"}}>
                <thead className="thead-xs">
                    <tr>
                        <th colSpan={11} className="text-center">Sport Betting</th>
                    </tr>
                    <tr>
                        <th className="border-right">Inserted#</th>
                        <th className="border-right">Inserted$</th>
                        <th className="border-right">Cancelled#</th>
                        <th className="border-right">Cancelled$</th>
                        <th className="border-right">Cash Out#</th>
                        <th className="border-right">Cash Out$</th>
                        <th className="border-right">Paid Out#</th>
                        <th className="border-right">Paid Out$</th>
                        <th className="border-right">Unpaid#</th>
                        <th className="border-right">Unpaid$</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td className="border-right border-bottom">{sports ? this.getCount(sports.inserted) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={sports ? this.getValue(sports.inserted) : ""} /></td>
                        <td className="border-right border-bottom">{sports ? this.getCount(sports.cancelled) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={sports ? this.getValue(sports.cancelled) : ""} /></td>
                        <td className="border-right border-bottom">{sports ? this.getCount(sports.cashedOut) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={sports ? this.getValue(sports.cashedOut) : ""} /></td>
                        <td className="border-right border-bottom">{sports ? this.getCount(sports.payedOut) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={sports ? this.getValue(sports.payedOut) : ""} /></td>
                        <td className="border-right border-bottom">{sports ? this.getCount(sports.unpaid) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={sports ? this.getValue(sports.unpaid) : ""} /></td>
                        <td className="border-bottom"><Currency value={sports ? this.getValue(sports.balance) : ""} /></td>
                    </tr>
                </tbody>
            </table>,

            <table className="table table-striped" key={report.name + "3"} style={{marginTop: "-1rem"}}>
                <thead className="thead-xs">
                    <tr>
                        <th colSpan={5} className="text-center">Customer Booking</th>
                        <th colSpan={1}></th>
                        <th colSpan={5} className="text-center">Cashier Booking</th>
                    </tr>
                    <tr>
                        <th className="border-right">Pay In#</th>
                        <th className="border-right">Pay In$</th>
                        <th className="border-right">Pay Out#</th>
                        <th className="border-right">Pay Out$</th>
                        <th className="border-right">Balance</th>

                        <th className="border-right"></th>

                        <th className="border-right">Pay In#</th>
                        <th className="border-right">Pay In$</th>
                        <th className="border-right">Pay Out#</th>
                        <th className="border-right">Pay Out$</th>
                        <th className="border-right">Balance</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td className="border-right border-bottom">{customerBookings ? this.getCount(customerBookings.payIn) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={customerBookings ? this.getValue(customerBookings.payIn) : ""} /></td>
                        <td className="border-right border-bottom">{customerBookings ? this.getCount(customerBookings.payOut) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={customerBookings ? this.getValue(customerBookings.payOut) : ""} /></td>
                        <td className="border-right border-bottom"><Currency value={customerBookings ? this.getValue(customerBookings.balance) : ""} /></td>

                        <td className="border-right border-bottom" style={{width: "1px"}}></td>

                        <td className="border-right border-bottom">{cashierBookings ? this.getCount(cashierBookings.payIn) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={cashierBookings ? this.getValue(cashierBookings.payIn) : ""} /></td>
                        <td className="border-right border-bottom">{cashierBookings ? this.getCount(cashierBookings.payOut) : ""}</td>
                        <td className="border-right border-bottom"><Currency value={cashierBookings ? this.getValue(cashierBookings.payOut) : ""} /></td>
                        <td className="border-bottom"><Currency value={cashierBookings ? this.getValue(cashierBookings.balance) : ""} /></td>
                    </tr>
                </tbody>
            </table>
        ]
    };

    render() {
        const {reports} = this.props;
        return (
            <Col>
                {
                    this.renderReport(reports, "0rem")
                }
                {
                    reports &&
                    reports.cashiers.map(
                        report => {return this.renderReport(report, "5rem")}
                    )
                }
            </Col>
        )
    }
}

function mapStateToProps(state) {
    const {webCashierReports} = state.company;
    const {reports} = webCashierReports;
    return {
        reports
    };
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(CashierReportTable);