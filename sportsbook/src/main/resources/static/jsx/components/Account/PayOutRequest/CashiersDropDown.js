import PropTypes from 'prop-types';
import React, {Component} from "react";
import {Button} from "reactstrap";
import DropdownList from 'react-widgets/lib/DropdownList';

class Company { }
Company.propTypes = {
    id: PropTypes.number,
    shortcut: PropTypes.string,
    name1: PropTypes.string,
};

const defaultPlaceholder = "all companies";

export default class CompaniesDropDown extends Component {
    render() {
        const addPlaceholder = this.props.addPlaceholder ? this.props.addPlaceholder : false;

        let placeholder = this.props.placeholder
            ? this.props.placeholder
            : addPlaceholder ? defaultPlaceholder : null;

        const style = this.props.style ? this.props.style : {width: '14rem'};

        const onChange = (e) => {
            this.props.selectionChanged(e.userId);
        };

        const onClear = () => {
            this.props.selectionChanged(null);
        };

        return(
            <div className="d-flex">
                <DropdownList
                    style = {style}
                    busy={this.props.listLoading}
                    caseSensitive={false}
                    minLength={2}
                    filter="contains"
                    //data={this.props.list ? this.props.list.toJS() : []}
                    data={this.props.list ? this.props.list : []}
                    valueField="userId"
                    value={this.props.id}
                    textField={item => item && item.username}
                    placeholder={placeholder}
                    onChange={onChange}
                />
                <Button
                    color="secondary"
                    size="sm"
                    onClick={onClear}>X
                </Button>
            </div>
        );
    };
}

CompaniesDropDown.propTypes = {
    companyId: PropTypes.number,
    style: PropTypes.object,
    //list: PropTypes.arrayOf(Company),
    listLoading: PropTypes.bool,
    addPlaceholder: PropTypes.bool,
    placeholder: PropTypes.string,
    selectionChanged: PropTypes.func,
};
