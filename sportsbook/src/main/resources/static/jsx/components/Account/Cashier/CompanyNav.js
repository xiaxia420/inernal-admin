import React, {Component} from "react";
import {connect} from "react-redux";
import {
    requestCompaniesIfNeeded,
    selectWebCashierCompany,
    clearWebCashierCompany
} from "../../../actions/company";
import {Card, Collapse} from "reactstrap";
import classNames from "classnames";

class CompanyNav extends Component {

    state = {
        openCompanies : []
    };

    componentDidMount() {
        this.props.requestCompaniesIfNeeded().then(
            () => {
                const {companies} = this.props;
                this.pushCompany(companies);
            }
        );
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const {companies} = this.props;
        const next = nextProps.companies;
        if (!companies && next) {
            this.pushCompany(next);
        }
    }

    pushCompany = (companies) => {
        if ((companies !== null) && (companies.length > 0)) {
            companies.forEach(company => {
                let {openCompanies} = this.state;
                if (!openCompanies.includes(company.id)) {
                    openCompanies.push(company.id);
                    this.setState({
                        openCompanies: openCompanies
                    })
                }
                this.pushCompany(company.companies);
            })
        }
    };

    toggleCompany = (companyId) => {
        let {openCompanies} = this.state;

        if (!openCompanies.includes(companyId)) {
            // add company to open list
            openCompanies.push(companyId);
            this.setState({
                openCompanies: openCompanies
            })
        } else {
            // remove company from open list
            for (let i = 0; i < openCompanies.length; i++) {
                if (companyId == openCompanies[i]) {
                    openCompanies.splice(i, 1);
                    this.setState({
                        openCompanies: openCompanies
                    });
                    break;
                }
            }
        }
    };

    recursiveRenderCompany = (companyId, companies) => {
        const {openCompanies} = this.state;
        const {webCashierCompanyId} = this.props;

        return (
            (companies !== null) && (companies.length > 0) ?
            <Collapse isOpen={openCompanies.includes(companyId)}>
                <ul className="nav nav-stacked nav-bordered flex-column">
                    {
                        companies.map(company =>
                            <li className="nav-item" key={company.id}>
                                <div className={classNames("nav-link d-flex cursor-pointer", {"active": webCashierCompanyId == company.id})}
                                     onClick={() => {this.onCompanyClick(company)}}
                                     style={{paddingLeft: company.level + "rem"}}
                                >
                                    {company.shortcut}

                                    <span className="text-muted ml-auto mr-3 ml-3 align-self-center"></span>

                                    {
                                        (company.companies !== null) && (company.companies.length > 0) &&
                                        <a className="cursor-pointer" onClick={() => this.toggleCompany(company.id)}>
                                            <i
                                                className={classNames("icon", {
                                                    "icon-chevron-down": !openCompanies.includes(company.id),
                                                    "icon-chevron-up": openCompanies.includes(company.id)
                                                })}
                                            />
                                        </a>
                                    }
                                </div>
                                {this.recursiveRenderCompany(company.id, company.companies)}
                            </li>
                        )
                    }
                </ul>
            </Collapse>
                :
            <span></span>
        )
    };

    onCompanyClick = (company) => {
        const {webCashierCompanyId} = this.props;

        if (company.companies && company.companies.length > 0) {
            // company has children, open children list
            this.toggleCompany(company.id)
        } else {
            if (company.id == webCashierCompanyId) {
                this.props.onClear();
                this.props.clearWebCashierCompany();
            } else {
                // company does not have children, open cashier report
                this.props.selectWebCashierCompany(company.id).then(
                    () => {
                        //this.props.requestWebCashierReports();
                        this.props.onCompanyClick();
                    }
                )
            }
        }
    };

    render() {
        const {companies, webCashierCompanyId} = this.props;
        const {openCompanies} = this.state;

        return (
            <Card key="navigation-content" color="dark" className="p-3 mt-3">
            {
                (companies !== null) && (companies.length > 0) &&
                <ul className="nav nav-bordered nav-stacked flex-column">
                {
                    companies.map(company =>
                        <li className="nav-item" key={company.id}>
                            <div className={classNames("nav-link d-flex cursor-pointer", {"active": webCashierCompanyId == company.id})}
                                 onClick={() => {this.onCompanyClick(company)}}
                                 style={{paddingLeft: "0rem"}}
                            >
                                {company.shortcut}

                                <span className="text-muted ml-auto mr-3 ml-3 align-self-center"></span>

                                <a className="cursor-pointer" onClick={ () => this.toggleCompany(company.id)}>
                                    <i
                                        className={classNames("icon", {
                                            "icon-chevron-down": !openCompanies.includes(company.id),
                                            "icon-chevron-up": openCompanies.includes(company.id)
                                        })}
                                    />
                                </a>
                            </div>

                            {this.recursiveRenderCompany(company.id, company.companies)}
                        </li>
                    )
                }
                </ul>
            }
            </Card>
        )
    }
}

function mapStateToProps(state) {
    const {companies, webCashierReportsFilter, webCashierCompanyId} = state.company;
    return {
        companies, webCashierReportsFilter, webCashierCompanyId
    };
}

const mapDispatchToProps = {
    requestCompaniesIfNeeded, selectWebCashierCompany, clearWebCashierCompany
};

export default connect(mapStateToProps, mapDispatchToProps)(CompanyNav);