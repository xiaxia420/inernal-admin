import React, {Component} from "react";
import {ContainerFluidSpacious, Currency, formatUserDate} from "../../App/Toolkit";
import {connect} from "react-redux";
import {
    requestPaymentProvidersIfNeeded, requestPaymentChannelsIfNeeded,
    resetPaymentTransactionsFilter, requestPaymentTransactions, clearPaymentTransactions,
    requestAcceptPaymentTransaction, requestDenyPaymentTransaction, requestSyncPaymentTransaction
} from "../../../actions/payment";
import {requestPaymentTransactionNotification} from "../../../actions/notification";
import {
    Button,
    Col,
    Row,
    InputGroup,
    InputGroupAddon,
    InputGroupText, UncontrolledTooltip
} from 'reactstrap';
import PaymentProviderDropDown from "./PaymentProviderDropDown";
import PaymentChannelDropDown from "./PaymentChannelDropDown";
import PaymentDirectionDropDown from "./PaymentDirectionDropDown";
import {DateTimePicker} from "react-widgets";
import Indicator from "../../Module/Indicator";
import PageListPagination from "../../App/PageListPagination";
import {parse} from "qs";
import {showGeneralError} from "../../../actions/app";

export const PAYMENT_TRANSACTION_DIRECTIONS = [
    {id: 1, name: "DEPOSIT"},
    {id: 2, name: "WITHDRAW"}
];

export const PAYMENT_TRANSACTION_STATUS = [
    {id: 0, name: "OPEN"},
    {id: 1, name: "ACCEPTED"},
    {id: 3, name: "DECLINED_BY_OPERATOR"},
    {id: 4, name: "DECLINED_BY_PROVIDER"},
    {id: 11, name: "PAID"},
    {id: 90, name: "CANCELLED"},
    {id: 91, name: "EXPIRED"},
    {id: 99, name: "ERROR"}
];

class PaymentTransaction extends Component {

    state = {
        isHandlingTransaction: false,
        syncIds: []
    };

    componentDidMount() {
        this.props.requestPaymentProvidersIfNeeded();
        this.props.requestPaymentChannelsIfNeeded();

        /*
        http://localhost:9988/customer/payment_transactions?username=xiaxi421&popup
        */
        const search = this.props.location;
        if (search) {
            const query = parse(location.search.substr(1));
            const username = query.username;
            if (username) {
                const {paymentTransactionsFilter} = this.props;
                this.props.resetPaymentTransactionsFilter(
                    {
                        ...paymentTransactionsFilter,
                        customerUsername: username
                    }
                ).then(
                    () => {
                        const {pageIndex, pageSize} = this.props.paymentTransactionsPagination;
                        this.props.requestPaymentTransactions(pageIndex, pageSize);
                    }
                );
            }
        }
    }

    onPaymentDirectionChange = (id) => {
        const {paymentTransactionsFilter} = this.props;
        this.props.resetPaymentTransactionsFilter(
            {
                ...paymentTransactionsFilter,
                direction: id
            }
        );
    };

    onPaymentProviderChange = (id) => {
        const {paymentTransactionsFilter} = this.props;
        this.props.resetPaymentTransactionsFilter(
            {
                ...paymentTransactionsFilter,
                paymentProviderId: id
            }
        );
    };

    onPaymentChannelChange = (id) => {
        const {paymentTransactionsFilter} = this.props;
        this.props.resetPaymentTransactionsFilter(
            {
                ...paymentTransactionsFilter,
                paymentChannelId: id
            }
        );
    };

    onCustomerUsernameChange = (e) => {
        const {paymentTransactionsFilter} = this.props;
        this.props.resetPaymentTransactionsFilter(
            {
                ...paymentTransactionsFilter,
                customerUsername: e.target.value
            }
        );
    };

    onPaymentStatusChange = (id) => {
        const {paymentTransactionsFilter} = this.props;
        this.props.resetPaymentTransactionsFilter(
            {
                ...paymentTransactionsFilter,
                status: id
            }
        );
    };

    onFromChange = (from) => {
        const {paymentTransactionsFilter} = this.props;
        this.props.resetPaymentTransactionsFilter(
            {
                ...paymentTransactionsFilter,
                from: from
            }
        );
    };

    onToChange = (to) => {
        const {paymentTransactionsFilter} = this.props;
        this.props.resetPaymentTransactionsFilter(
            {
                ...paymentTransactionsFilter,
                to: to
            }
        );
    };

    searchPaymentTransactions = () => {
        const {pageIndex, pageSize} = this.props.paymentTransactionsPagination;
        this.props.requestPaymentTransactions(pageIndex, pageSize);
        this.setState({
            syncIds: []
        })
    };

    clearPaymentTransactions = () => {
        this.props.clearPaymentTransactions();
    };

    onChangePageAction = (pageIndex, pageSize) => {
        this.props.requestPaymentTransactions(pageIndex, pageSize);
    };

    acceptPaymentTransaction = (id) => {
        this.setState({
            isHandlingTransaction: true,
        });

        this.props.requestAcceptPaymentTransaction(id).then(
            () => {
                this.setState({
                    isHandlingTransaction: false

                });
                this.searchPaymentTransactions();
                this.props.requestPaymentTransactionNotification();
            }
        ).catch(
            (error) => {
                this.setState({
                    isHandlingTransaction: false
                });
                this.props.showGeneralError(error);
            }
        )
    };

    denyPaymentTransaction = (id) => {
        this.setState({
            isHandlingTransaction: true
        });
        this.props.requestDenyPaymentTransaction(id).then(
            () => {
                this.setState({
                    isHandlingTransaction: false
                });
                this.searchPaymentTransactions();
                this.props.requestPaymentTransactionNotification();
            }
        ).catch(
            (error) => {
                this.setState({
                    isHandlingTransaction: false
                });
                this.props.showGeneralError(error);
            }
        )
    };

    addId = (id) => {
        let {syncIds} = this.state;
        if (syncIds.indexOf(id) === -1) {
            syncIds.push(id);
            this.setState(syncIds);
        }
    };

    removeId = (id) => {
        let {syncIds} = this.state;
        const index = syncIds.indexOf(id);
        if (index > -1) {
            syncIds.splice(index, 1);
        }
        this.setState(syncIds);
    };

    syncPaymentTransaction = (id) => {
        //this.setState({
        //    isHandlingTransaction: true
        //});

        this.addId(id);
        this.props.requestSyncPaymentTransaction(id).then(
            () => {
                //this.setState({
                //    isHandlingTransaction: false
                //});
                this.searchPaymentTransactions();
                this.removeId(id);
                //this.props.requestPaymentTransactionNotification();
            }
        ).catch(
            (error) => {
                this.removeId(id);
                //this.setState({
                //    isHandlingTransaction: false
                //});
                // do not show error
                //this.props.showGeneralError(error);
            }
        )
    };

    render() {
        const {paymentProviders, paymentChannels, paymentTransactionsFilter, paymentTransactionsPagination, paymentTransactions} = this.props;
        const {paymentProviderId, paymentChannelId, customerUsername, direction, status, from, to} = paymentTransactionsFilter;
        const {isLoading, transactions} = paymentTransactions;
        const {isHandlingTransaction, syncIds} = this.state;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-titles">
                                <h3 className="dashhead-title">Payment Provider Transactions</h3>
                            </div>

                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* direction */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={direction}
                                                              addPlaceholder={true}
                                                              placeholder="Select direction"
                                                              list={PAYMENT_TRANSACTION_DIRECTIONS}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onPaymentDirectionChange}/>
                                </div>

                                {/* status */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={status}
                                                              addPlaceholder={true}
                                                              placeholder="Select status"
                                                              list={PAYMENT_TRANSACTION_STATUS}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onPaymentStatusChange}/>
                                </div>

                                {/* provider */}
                                <div className="ml-3">
                                    <PaymentProviderDropDown id={paymentProviderId}
                                                             addPlaceholder={true}
                                                             placeholder="Select provider"
                                                             list={paymentProviders}
                                                             listLoading={isLoading}
                                                             selectionChanged={this.onPaymentProviderChange}/>
                                </div>

                                {/* channel */}
                                <div className="ml-3">
                                    <PaymentChannelDropDown id={paymentChannelId}
                                                            addPlaceholder={true}
                                                            placeholder="Select channel"
                                                            list={paymentChannels}
                                                            listLoading={isLoading}
                                                            selectionChanged={this.onPaymentChannelChange}/>
                                </div>

                            </div>
                        </div>
                    </Col>
                </Row>

                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* customer username */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="icon icon-magnifying-glass"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Customer username"
                                               value={customerUsername}
                                               onChange={this.onCustomerUsernameChange}
                                        />
                                    </InputGroup>
                                </div>

                                {/* from */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={from} format="L LT" placeholder="From" onChange={this.onFromChange}/>
                                </div>

                                {/* to */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={to} format="L LT" placeholder="To" onChange={this.onToChange}/>
                                </div>

                                <div className="ml-3">
                                    <Button color="primary"
                                            disabled={isLoading}
                                            onClick={this.searchPaymentTransactions}
                                    >
                                        Search
                                    </Button>

                                    <Button color="dark"
                                            onClick={this.clearPaymentTransactions}
                                    >
                                        Clear
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>

                {
                    true == isLoading &&
                    <div className="text-center">
                        <Indicator animate className="loading-indicator"/>
                        <div className="dashhead-subtitle mt-3">Loading</div>
                    </div>
                }

                <Row className="mt-3">
                    {
                        transactions && transactions.length > 0 &&
                        <Col>
                            <table className="table table-striped">
                                <thead className="thead-xs">
                                <tr>
                                    <th>Id</th>
                                    <th>C. Id</th>
                                    <th>Username</th>
                                    <th>Channel</th>
                                    <th>Type</th>
                                    <th>Value</th>
                                    <th>Booking</th>
                                    <th>Fee</th>
                                    <th>Status</th>
                                    {/* <th>Reason</th> */}
                                    <th>Code</th>
                                    <th>Insert At</th>
                                    <th>Changed At</th>
                                    <th>External Id</th>

                                    <th style={{width: "1rem"}}>
                                        {/* Accept Button */}
                                    </th>

                                    <th style={{width: "1rem"}}>
                                        {/* Deny Button */}
                                    </th>

                                </tr>
                                </thead>

                                {
                                    transactions &&
                                    <tbody>
                                    {
                                        transactions.map(transaction =>
                                        {
                                            const {id, customerId, customerUsername, paymentChannelName, direction,
                                                value, bookingValue, fee, status, uniqueCode, insertTime, statusChangeTime,
                                                remoteAddress, externalReferenceId
                                            } = transaction;
                                            const needRelease = status == "OPEN" && direction == "WITHDRAW";
                                            const needSync = ( (status == "OPEN") || (status == "ACCEPTED") ) && (syncIds.indexOf(id) == -1);

                                            return (
                                                <tr key={id} style={{color: needRelease ? "#7fffd4":null}}>
                                                    <td>{id}</td>
                                                    <td>{customerId}</td>
                                                    <td>{customerUsername}</td>
                                                    <td>{paymentChannelName}</td>
                                                    <td>{direction}</td>
                                                    <td><Currency value={value} /></td>
                                                    <td><Currency value={bookingValue} /></td>
                                                    <td><Currency value={fee} /></td>
                                                    <td>{status}</td>
                                                    <td>{uniqueCode}</td>
                                                    <td>{formatUserDate(insertTime)}</td>
                                                    <td>{formatUserDate(statusChangeTime)}</td>
                                                    <td>{externalReferenceId}</td>

                                                    <td className="p-1 text-center">
                                                        {
                                                            needRelease &&
                                                            <Button id={"Accept" + transaction.id}
                                                                    className="p-1"
                                                                    color="outline-primary"
                                                                    style={{width: "2rem", height: "2rem"}}
                                                                    size="sm"
                                                                    disabled={true == isHandlingTransaction}
                                                                    onClick={() => {this.acceptPaymentTransaction(transaction.id)}}
                                                            >
                                                                A
                                                                <UncontrolledTooltip placement="right" target={"Accept" + transaction.id}>
                                                                    Transfer money
                                                                </UncontrolledTooltip>
                                                            </Button>
                                                        }
                                                    </td>

                                                    <td className="p-1 text-center">
                                                        {
                                                            needRelease &&
                                                            <Button id={"Deny" + transaction.id}
                                                                    className="p-1"
                                                                    color="outline-primary"
                                                                    style={{width: "2rem", height: "2rem"}}
                                                                    size="sm"
                                                                    disabled={true == isHandlingTransaction}
                                                                    onClick={() => {this.denyPaymentTransaction(transaction.id)}}
                                                            >
                                                                D
                                                                <UncontrolledTooltip placement="right" target={"Deny" + transaction.id}>
                                                                    Decline
                                                                </UncontrolledTooltip>
                                                            </Button>
                                                        }

                                                        {
                                                            needSync &&
                                                            <Button id={"Sync" + transaction.id}
                                                                    className="p-1"
                                                                    color="outline-primary"
                                                                    style={{width: "2rem", height: "2rem"}}
                                                                    size="sm"
                                                                    disabled={true == isHandlingTransaction}
                                                                    onClick={() => {this.syncPaymentTransaction(transaction.id)}}
                                                            >
                                                                SY
                                                                <UncontrolledTooltip placement="right" target={"Sync" + transaction.id}>
                                                                    Sync
                                                                </UncontrolledTooltip>
                                                            </Button>
                                                        }

                                                    </td>
                                                </tr>
                                            )
                                        }
                                        )
                                    }
                                    </tbody>
                                }
                            </table>
                        </Col>
                    }
                </Row>

                {
                    transactions && transactions.length > 0  &&
                    <Row>
                        <PageListPagination pageList={paymentTransactionsPagination}
                                            isLoading={isLoading}
                                            changePageAction={this.onChangePageAction}
                        />
                    </Row>
                }
            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {paymentProviders, paymentChannels, paymentTransactions, paymentTransactionsFilter, paymentTransactionsPagination} = state.payment;

    const {paymentProviderId, direction} = paymentTransactionsFilter;
    let channels = paymentChannels;

    // filter after direction
    if (direction) {
        const paymentDirection = PAYMENT_TRANSACTION_DIRECTIONS.find(d => d.id == direction);
        if (paymentDirection) {
            channels = paymentChannels.filter(c => c.paymentDirection == paymentDirection.name);
        }
    }

    // filter after provider
    if (paymentProviderId && paymentChannels) {
        channels = paymentChannels.filter(c => c.paymentProviderId == paymentProviderId);
    }

    return {
        paymentProviders, paymentChannels: channels, paymentTransactionsFilter, paymentTransactionsPagination, paymentTransactions
    };
}

const mapDispatchToProps = {
    requestPaymentProvidersIfNeeded, requestPaymentChannelsIfNeeded, resetPaymentTransactionsFilter, requestPaymentTransactions,
    clearPaymentTransactions, requestAcceptPaymentTransaction, requestDenyPaymentTransaction, requestSyncPaymentTransaction,
    showGeneralError, requestPaymentTransactionNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentTransaction);