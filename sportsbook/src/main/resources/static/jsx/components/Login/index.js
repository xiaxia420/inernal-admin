import React, {Component} from "react";
import {connect} from "react-redux";
import {login, abortLogin, confirmLogin} from "../../actions/user";
import {FormattedMessage} from "react-intl";
import {replace} from "react-router-redux";
import {Alert, Button, Card, CardBody, CardTitle, Col, Row} from "reactstrap";
import Indicator from "../Module/Indicator";
import ConfirmLoginModal from "./ConfirmLoginModal";

class Login extends Component {

    state = {
        username: "",
        password: "",
        errorCode: null
    };

    componentWillMount() {
        if (this.props.authenticated) {
            this.props.replace(`/`);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.authenticated && nextProps.authenticated) {
            this.props.replace(`/`);
        }
    }

    onUsernameChange = (e) => {
        this.setState({
            username: e.target.value
        });
    };

    onPasswordChange = (e) => {
        this.setState({
            password: e.target.value
        });
    };

    handleKeypress = (e) => {
        if (e.key === "Enter") {
            this.submitLogin();
        }
    };

    submitLogin = () => {
        const {username, password} = this.state;
        if (username && password) {
            if (username.length > 0 && password.length > 0) {
                this.props.login(username, password).catch(
                    ({error}) => {
                        this.setState({
                            errorCode: error
                        })
                    }
                );
            }
        }
    };

    dismissError = () => {
        this.setState({
            errorCode: null
        })
    };

    render() {
        const {errorCode} = this.state;
        const {isLoginLoading, showLoginConfirmation, imageResourceUrl} = this.props;
        const {confirmLogin, abortLogin} = this.props;

        return (
            <div className="container-fluid-spacious">
                <Row className="justify-content-center mt-5">
                    <Col sm="6" lg="4" xl="3">
                        <Card body color="dark">
                            <CardBody>
                                <CardTitle className="text-center mb-4" tag="h4">
                                    <img src={imageResourceUrl + "logo.png"} />
                                </CardTitle>

                                <span>
                                    <input type="text"
                                           onChange={this.onUsernameChange}
                                           onKeyPress={this.handleKeypress}
                                           placeholder="Username"
                                           className="form-control"
                                    />

                                    <p/>

                                    <input type="password"
                                           onChange={this.onPasswordChange}
                                           onKeyPress={this.handleKeypress}
                                           placeholder="Password"
                                           className="form-control"
                                    />

                                    <br/>

                                    <Button block color="success" onClick={this.submitLogin}>
                                        {/*<i className="icon icon-lock"/>*/}
                                        Login
                                    </Button>

                                    {
                                        (true == isLoginLoading) &&
                                        <Indicator animate className="loading-indicator"/>
                                    }

                                    {
                                        errorCode &&
                                        <Alert color="danger" toggle={this.dismissError}>
                                            <FormattedMessage id={errorCode} />
                                        </Alert>
                                    }
                                </span>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                {
                    (true == showLoginConfirmation) &&
                    <ConfirmLoginModal
                        abortLogin={abortLogin}
                        confirmLogin={confirmLogin}
                    />
                }

            </div>
        );
    }
}

function mapStateToProps(state) {
    const {authenticated, showLoginConfirmation, isLoginLoading} = state.user;
    const {imageResourceUrl} = state.app;
    return {
        authenticated, showLoginConfirmation, isLoginLoading, imageResourceUrl
    };
}

const mapDispatchToProps = {
    replace,
    login, abortLogin, confirmLogin
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
