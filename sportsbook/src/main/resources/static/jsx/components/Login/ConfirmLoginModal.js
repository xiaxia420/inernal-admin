import React, {Component} from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, FormGroup, Row, Col, Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";

class ConfirmLoginModal extends Component {

    render() {
        const {confirmLogin, abortLogin} = this.props;

        return (
            <Modal keyboard fade
                   size="sm"
                   backdrop="static"
                   toggle={ () => {} }
                   isOpen={true}
            >

                <ModalHeader toggle={this.props.abortLogin}>
                    Confirm Login
                </ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col>
                                Confirm Login
                            </Col>
                        </Row>
                    </ModalBody>

                <ModalFooter>
                    <Button color="secondary"
                            className="cursor-pointer mr-auto"
                            onClick={confirmLogin}
                    >
                        Confirm
                    </Button>

                    <Button color="secondary"
                            className="cursor-pointer"
                            onClick={abortLogin}
                    >
                        Abort
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }
}

export default ConfirmLoginModal;
