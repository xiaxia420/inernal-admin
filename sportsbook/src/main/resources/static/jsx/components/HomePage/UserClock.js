import React, {Component} from "react";
import {FormattedDate} from "react-intl";

class UserClock extends Component {

    componentDidMount() {
        this.interval = setInterval(() => this.forceUpdate(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const {timezoneOffsetMinutes} = this.props;
        let date = new Date();
        const now = Date.now() + (timezoneOffsetMinutes + date.getTimezoneOffset()) * 60 * 1000;
        let gmt = "GMT" + ((timezoneOffsetMinutes >= 0) ? "+" : "") + timezoneOffsetMinutes / 60;

        return (
            <span>
                <FormattedDate value={now}
                               day="2-digit"
                               month="2-digit"
                               year="numeric"
                               hour="2-digit"
                               minute="2-digit"
                               second="2-digit"
                               hour12={false}
                />
                &nbsp;
                {gmt}
            </span>
        )
    }
}

export default UserClock;