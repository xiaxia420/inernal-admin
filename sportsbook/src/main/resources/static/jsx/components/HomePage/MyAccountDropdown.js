import React, {Component} from "react";
import {DropdownItem, DropdownMenu, DropdownToggle, Nav, UncontrolledDropdown} from "reactstrap";
import {connect} from "react-redux";
import {logout} from "../../actions/user";
import {userLanguageSelector} from "../../selectors/app";
import {RouterDropdownItem} from "./RouterNavItem";

class MyAccountDropdown extends Component {

    onLogout= () => {
        this.props.logout();
    };

    render() {
        const {username, location} = this.props;
        const {pathname} = location;

        return (
            <Nav navbar>
                <UncontrolledDropdown nav className="ml-4 mr-3">
                    <DropdownToggle tag="a" caret className="cursor-pointer nav-link">
                        {username}
                    </DropdownToggle>

                    <DropdownMenu right>
                        <RouterDropdownItem to="/user/config" pathname={pathname}>Config</RouterDropdownItem>

                        <DropdownItem onClick={this.onLogout} className="cursor-pointer">
                            <span className="text-danger">Logout</span>
                        </DropdownItem>
                    </DropdownMenu>

                </UncontrolledDropdown>
            </Nav>
        );
    }
}

function mapStateToProps(state, props) {
    const {username} = state.user;
    const {languages} = state.app.i18n;
    const userLanguage = userLanguageSelector(state, props);

    return {
        username,
        languages,
        userLanguage
    };
}

const mapDispatchToProps = {
    logout
};

export default connect(mapStateToProps, mapDispatchToProps)(MyAccountDropdown);