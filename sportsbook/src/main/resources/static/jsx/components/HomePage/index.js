import React, {Component} from "react";
import {connect} from "react-redux";
import Navigation from "./Navigation";
import {Alert, NavbarToggler, Collapse, Col, Navbar, Row} from "reactstrap";
import {requestLanguagesIfNeeded, requestCurrenciesIfNeeded, requestTimezonesIfNeeded} from "../../actions/i18n"
import {Link} from "react-router";
import {Logo} from "../App/Icons";
import {ContainerFluidSpacious, getPopupDimensions} from "../App/Toolkit";
import MyAccountDropdown from "./MyAccountDropdown";
import UserClock from "./UserClock";
import Indicator from "../Module/Indicator";
import {dismissGeneralError} from "../../actions/app";
import {parse} from "qs";
import MessageNotification from "../Notification/MessageNotification/index";
import PaymentTransactionNotification from "../Notification/PaymentTransactionNotification/index";
import PayOutRequestNotification from "../Notification/PayOutRequestNotification/index";
import CashFlowNotification from "../Notification/CashFlowNotification/index";
import OnlineBalanceNotification from "../Notification/CashFlowNotification/OnlineBalanceNotification";

class HomePage extends Component {

    componentDidMount() {
        this.props.requestLanguagesIfNeeded();
        this.props.requestCurrenciesIfNeeded();
        this.props.requestTimezonesIfNeeded();
    }

    render() {
        const {userRoles, timezoneOffsetMinutes, children, location, generalError, imageResourceUrl} = this.props;
        const {pathname} = location;

        // don't show navigation in popup
        let isPopup = false;
        if (location.search) {
            const query = parse(location.search.substr(1));
            const entries = Object.keys(query);
            if (entries.includes("popup")) {
                isPopup = true;
            }
        }

        return (
            <span>
                {
                    !isPopup &&
                    <Navbar key="navbar" dark className="bg-dark" expand="xs">

                        <Link to="/" className="navbar-brand">
                            {/* <Logo className="navbar-logo"/> */}

                            <img src={imageResourceUrl + "logo.png"}
                                height="20px"
                            />
                        </Link>

                        <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />

                        <Collapse isOpen={false} navbar>
                            <Navigation
                                pathname={pathname}
                                userRoles={userRoles}
                            />

                            <Indicator animate={false} className="polling-indicator ml-auto text-muted"/>

                            <UserClock
                                timezoneOffsetMinutes={timezoneOffsetMinutes}
                            />

                            {/*<a className="cursor-pointer text-muted" onClick={this.openNewWindow}><i className="icon icon-popup"/></a>*/}

                            <MyAccountDropdown
                                location={location}
                            />
                        </Collapse>
                    </Navbar>
                }

                <ContainerFluidSpacious>
                    <Row>
                        <OnlineBalanceNotification />
                        <MessageNotification />
                        <PaymentTransactionNotification />
                        <PayOutRequestNotification />
                    </Row>
                    <Row>
                        <CashFlowNotification />
                    </Row>
                </ContainerFluidSpacious>

                {
                    generalError &&
                    <ContainerFluidSpacious key="error">
                        <Row className="mt-3">
                            <Col>
                                <Alert color="danger" toggle={this.props.dismissGeneralError}>
                                    <h4 className="alert-heading">{generalError.error}</h4>
                                    {generalError.trace}
                                </Alert>
                            </Col>
                        </Row>
                    </ContainerFluidSpacious>
                }

                {
                    children
                }
            </span>
        );
    }

}

function mapStateToProps(state, props) {
    const {userRoles, timezoneOffsetMinutes} = state.user;
    const {generalError, imageResourceUrl} = state.app;

    return {
        userRoles, timezoneOffsetMinutes, generalError, imageResourceUrl
    };
}

const mapDispatchToProps = {
    requestLanguagesIfNeeded,
    requestCurrenciesIfNeeded,
    requestTimezonesIfNeeded,
    dismissGeneralError
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
