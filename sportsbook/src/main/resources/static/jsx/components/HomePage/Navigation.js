import React, { Component } from "react";
import { Nav, DropdownItem } from "reactstrap";
import { RouterDropdownItem, RouterNavItem } from "./RouterNavItem";
import RouterNavDropdown from "./RouterNavDropdown";

class Navigation extends Component {


    isLevel1Active = (path) => {
        const {pathname} = this.props;
        return pathname.startWith(path);
    };

    isLeve2Active = (path) => {
        const {pathname} = this.props;
        return pathname == path;
    };

    render() {
        const {
            isSuperUser,
            allowAdminSportNavigation,
            allowAdminSportRead,
            allowAdminRegionRead,
            allowCategoryRead,
            allowTournamentRead,
            allowAdminSeasonRead,
            allowAdminVenueRead,
            allowAdminTipModelRead,
            allowAdminCustomerRead,
            allowAdminTicket,
            allowAdminRivalRead,
            allowAdminCompanyRead,
            allowAdminUserRead,
            allowAdminClientRead,
            allowAdminCurrencyRead,
            allowAdminTitleRead,
            allowAdminLanguageRead,
            allowAdminRolesRead,
            allowAdminRoleGroupsRead
        } = this.props.userRoles;

        const {pathname} = this.props;

        return (
            <div>
                <Nav navbar>
                    <RouterNavDropdown to="/ticket" label="Ticket" pathname={pathname}>
                        <RouterDropdownItem to="/ticket/statistics" pathname={pathname}>Ticket Statistics</RouterDropdownItem>
                        <RouterDropdownItem to="/ticket/cash_out_analytics" pathname={pathname}>Cash Out Analytics</RouterDropdownItem>
                        <RouterDropdownItem to="/ticket/tickets" pathname={pathname}>Tickets</RouterDropdownItem>
                        <RouterDropdownItem to="/ticket/add_flag" pathname={pathname}>Add Flag</RouterDropdownItem>
                        <RouterDropdownItem to="/ticket/remove_flag" pathname={pathname}>Remove Flag</RouterDropdownItem>
                    </RouterNavDropdown>

                    {/*
                    <RouterNavDropdown to="/company" label="Company" pathname={pathname}>
                        <RouterDropdownItem to="/company/shops" pathname={pathname}>Shops</RouterDropdownItem>
                    </RouterNavDropdown>
                    */}

                    <RouterNavDropdown to="/account" label="Account" pathname={pathname}>
                        <RouterDropdownItem to="/account/cashier" pathname={pathname}>Cashiers Report</RouterDropdownItem>
                        <RouterDropdownItem to="/account/cashier_book_account" pathname={pathname}>Cashiers Book Account</RouterDropdownItem>
                        <RouterDropdownItem to="/account/payment_transactions" pathname={pathname}>Payment Provider Transactions</RouterDropdownItem>
                        <RouterDropdownItem to="/account/payout_requests" pathname={pathname}>Shop Pay Out Requests</RouterDropdownItem>
                        {/*<RouterDropdownItem to="/account/customer" pathname={pathname}>Customer Account</RouterDropdownItem>*/}
                    </RouterNavDropdown>

                    <RouterNavDropdown to="/customer" label="Customer" pathname={pathname}>
                        <RouterDropdownItem to="/customer/customers" pathname={pathname}>Customer Data</RouterDropdownItem>
                        <RouterDropdownItem to="/customer/account_bookings" pathname={pathname}>Customer Account Bookings</RouterDropdownItem>
                        <RouterDropdownItem to="/customer/forgot_password" pathname={pathname}>Reset Password Request</RouterDropdownItem>
                        <RouterDropdownItem to="/customer/messages" pathname={pathname}>Customer Messages</RouterDropdownItem>
                    </RouterNavDropdown>
                </Nav>
            </div>
        );
    }
}

export default Navigation;