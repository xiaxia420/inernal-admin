import React from "react"
import classNames from "classnames";

const Indicator = ({animate, className}) => (
    <div className={className}>
        <i className={classNames("icon icon-circle faa-burst", {
            "animated": animate
        })}/>
    </div>
);

export default Indicator;