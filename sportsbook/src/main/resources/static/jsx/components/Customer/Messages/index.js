import React, {Component} from "react";
import {ContainerFluidSpacious, formatUserDate} from "../../App/Toolkit";
import {
    Button,
    Col,
    Row,
    UncontrolledTooltip
} from 'reactstrap';
import {resetCustomerMessagesFilter, requestCustomerMessages, updateCustomerMessage, closeCustomerMessage, clearCustomerMessages} from "../../../actions/customer";
import {requestMessageNotification} from "../../../actions/notification";
import {connect} from "react-redux";
import PageListPagination from "../../App/PageListPagination";
import Indicator from "../../Module/Indicator";
import {DateTimePicker} from "react-widgets";
import PaymentDirectionDropDown from "../../Account/PaymentTransaction/PaymentDirectionDropDown";
import {parse} from "qs";

export const CUSTOMER_MESSAGE_STATUS = [
    {id: 0, name: "ALL"},
    {id: 1, name: "UN_READ"},
    {id: 2, name: "IN_PROGRESS"},
    {id: 3, name: "CLOSED"},
];

class Messages extends Component {

    state = {
        isHandling: false
    };

    componentDidMount() {
        /*
        http://localhost:9988/customer/messages?username=xiaxi421&popup
        */
        const search = this.props.location;
        if (search) {
            const query = parse(location.search.substr(1));
            const username = query.username;
            if (username) {
                const {customerMessagesFilter} = this.props;
                this.props.resetCustomerMessagesFilter(
                    {
                        ...customerMessagesFilter,
                        customerUsername: username
                    }
                ).then(
                    () => {
                        const {pageIndex, pageSize} = this.props.customerMessagesPagination;
                        this.props.requestCustomerMessages(pageIndex, pageSize);
                    }
                );
            }
        }
    }

    onFromChange = (from) => {
        const {customerMessagesFilter} = this.props;
        this.props.resetCustomerMessagesFilter(
            {
                ...customerMessagesFilter,
                from: from
            }
        );
    };

    onToChange = (to) => {
        const {customerMessagesFilter} = this.props;
        this.props.resetCustomerMessagesFilter(
            {
                ...customerMessagesFilter,
                from: to
            }
        );
    };

    onMessageStatusChange = (id) => {
        const {customerMessagesFilter} = this.props;
        this.props.resetCustomerMessagesFilter(
            {
                ...customerMessagesFilter,
                messageStatus: id
            }
        );
    };

    processMessage = (id) => {
        this.setState({
            isHandling: true
        });
        this.props.updateCustomerMessage(id).then(
            () => {
                this.setState({
                    isHandling: false
                });
                this.searchCustomerMessages();
                this.props.requestMessageNotification();
            }
        )
    };

    closeMessage = (id) => {
        this.setState({
            isHandling: true
        });
        this.props.closeCustomerMessage(id).then(
            () => {
                this.setState({
                    isHandling: false
                });
                this.searchCustomerMessages();
                this.props.requestMessageNotification();
            }
        )
    };

    searchCustomerMessages = () => {
        const {pageIndex, pageSize} = this.props.customerMessagesPagination;
        this.props.requestCustomerMessages(pageIndex, pageSize);
    };

    onChangePageAction = (pageIndex, pageSize) => {
        console.log("i am here", pageIndex, pageSize);
        this.props.requestCustomerMessages(pageIndex, pageSize);
    };

    clearCustomerMessages = () => {
        this.props.clearCustomerMessages();
    };

    render() {
        const {isLoading, messages, customerMessagesPagination} = this.props;
        const {from, to, messageStatus} = this.props.customerMessagesFilter;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-titles">
                                <h3 className="dashhead-title">Customer Messages</h3>
                            </div>

                            <div className="dashhead-toolbar d-flex align-items-center">

                                {/* from */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={from} format="L LT" placeholder="From" onChange={this.onFromChange}/>
                                </div>

                                {/* to */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={to} format="L LT" placeholder="To" onChange={this.onToChange}/>
                                </div>

                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={messageStatus}
                                                              addPlaceholder={true}
                                                              placeholder="Message Status"
                                                              list={CUSTOMER_MESSAGE_STATUS}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onMessageStatusChange}/>
                                </div>

                                <div className="ml-3">
                                    <Button color="primary"
                                            disabled={isLoading}
                                            onClick={this.searchCustomerMessages}
                                    >
                                        Search
                                    </Button>

                                    <Button color="dark"
                                            onClick={this.clearCustomerMessages}
                                    >
                                        Clear
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>

                {
                    true == isLoading &&
                    <div className="text-center mt-5">
                        <Indicator animate className="loading-indicator"/>
                        <div className="dashhead-subtitle mt-3">Loading</div>
                    </div>
                }

                {
                    messages && messages.length > 0 &&
                    <Row className="mt-3">
                        <Col>
                            <table className="table table-striped">
                                <thead className="thead-xs">
                                <tr>
                                    <th className="pl-3" style={{width: "4rem"}}>Id</th>
                                    <th>Insert Time</th>
                                    <th>Update Time</th>
                                    <th>Customer Id</th>
                                    <th>Customer Username</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th style={{width: "30rem"}}>Content</th>
                                    <th>Status</th>

                                    <th style={{width: "1rem"}}>
                                        {/* Process */}
                                    </th>

                                    <th style={{width: "1rem"}}>
                                        {/* Close */}
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                {
                                    messages.map(message =>
                                        {
                                            const {
                                                id, insertTime, updateTime, firstName, lastName, customerId,
                                                customerUsername, email, mobile, content, messageStatus
                                            } = message;
                                            return (
                                                <tr key={id}>
                                                    <td>{id}</td>
                                                    <td>{formatUserDate(insertTime)}</td>
                                                    <td>{formatUserDate(updateTime)}</td>
                                                    <td>{customerId}</td>
                                                    <td>{customerUsername}</td>
                                                    <td>{firstName}</td>
                                                    <td>{lastName}</td>
                                                    <th>{email}</th>
                                                    <th>{mobile}</th>
                                                    <th>{content}</th>
                                                    <th>{messageStatus}</th>

                                                    <td className="p-1 text-center">
                                                        { messageStatus == "OPEN" &&
                                                        <Button id={"ProcessMessage" + id}
                                                                className="p-1"
                                                                color="outline-primary"
                                                                style={{width: "2rem", height: "2rem"}}
                                                                size="sm"
                                                                onClick={() => {this.processMessage(id)}}
                                                        >
                                                            P
                                                            <UncontrolledTooltip placement="right" target={"ProcessMessage" + id}>
                                                                Process Message <br/>{id}
                                                            </UncontrolledTooltip>
                                                        </Button>
                                                        }
                                                    </td>

                                                    <td className="p-1 text-center">
                                                        { (messageStatus == "OPEN" || messageStatus == "PROCEEDED") &&
                                                        <Button id={"CloseMessage" + id}
                                                                className="p-1"
                                                                color="outline-primary"
                                                                style={{width: "2rem", height: "2rem"}}
                                                                size="sm"
                                                                onClick={() => this.closeMessage(id)}
                                                        >
                                                            C
                                                            <UncontrolledTooltip placement="right" target={"CloseMessage" + id}>
                                                                Close Message <br/>{id}
                                                            </UncontrolledTooltip>
                                                        </Button>
                                                        }
                                                    </td>
                                                </tr>
                                            )
                                        }
                                    )
                                }
                                </tbody>

                            </table>
                        </Col>
                    </Row>
                }

                {
                    messages && messages.length > 0 &&
                    <Row>
                        <PageListPagination pageList={customerMessagesPagination}
                                            loading={isLoading}
                                            changePageAction={this.onChangePageAction}/>
                    </Row>
                }

                {
                    null == messages && <div>No record</div>
                }

            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {customerMessagesFilter, customerMessagesPagination, customerMessages} = state.customer;
    const {isLoading, messages} = customerMessages;

    return {
        customerMessagesFilter, customerMessagesPagination, isLoading, messages
    };
}

const mapDispatchToProps = {
    resetCustomerMessagesFilter, requestCustomerMessages, updateCustomerMessage, closeCustomerMessage, clearCustomerMessages,
    requestMessageNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(Messages);