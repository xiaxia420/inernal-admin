import React, {Component} from "react";
import {ContainerFluidSpacious} from "../../App/Toolkit";
import {Button, ButtonGroup, Col, DropdownItem, DropdownMenu, DropdownToggle, Row, UncontrolledDropdown} from "reactstrap";
import ForgotPasswordTable from "./ForgotPasswordTable";
import {requestForgotPasswordLogs} from "../../../actions/customer";
import {connect} from "react-redux";


class ForgotPasswordLog extends Component {

    componentDidMount() {
        this.refreshLogs();
    }

    refreshLogs = () => {
        const {forgotPasswordPagination} = this.props;
        const pageIndex = forgotPasswordPagination ? forgotPasswordPagination.pageIndex : 0;
        const pageSize = forgotPasswordPagination ? forgotPasswordPagination.pageSize : 25;
        this.props.requestForgotPasswordLogs(pageIndex, pageSize);
    };

    render() {
        const {forgotPasswordLogs} = this.props;
        const {isLoading} = forgotPasswordLogs;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col>
                        <div className="dashhead flextable">

                            <div className="flextable-item align-top">
                                <h3 className="dashhead-title">Reset Password</h3>
                            </div>

                            <div className="flextable-item flextable-primary align-top">
                                <div className="ml-3 mr-3 d-flex">
                                </div>
                            </div>

                            <div className="flextable-item align-top">
                                <Button className="cursor-pointer"
                                        color={isLoading ? "dark" : "primary"}
                                        disabled={isLoading}
                                        onClick={this.refreshLogs}
                                >
                                    {isLoading? "..." : "Refresh"}
                                </Button>
                            </div>
                        </div>
                    </Col>
                </Row>

                <ForgotPasswordTable />

            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {forgotPasswordLogs, forgotPasswordPagination} = state.customer;
    return {
        forgotPasswordLogs,
        forgotPasswordPagination
    }
}

const mapDispatchToProps = {
    requestForgotPasswordLogs
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordLog);