import React, {Component} from "react";
import {Col, Row} from "reactstrap";
import {connect} from "react-redux";
import PageListPagination from "../../App/PageListPagination";
import {requestForgotPasswordLogs} from "../../../actions/customer";
import {formatUserDate} from "../../App/Toolkit";
import Indicator from "../../Module/Indicator";

class ForgotPasswordTable extends Component {

    onChangePageAction = (pageIndex, pageSize) => {
        this.props.requestForgotPasswordLogs(pageIndex, pageSize);
    };

    render() {
        const {forgotPasswordLogs, forgotPasswordPagination} = this.props;
        const {isLoading, logs} = forgotPasswordLogs;

        return (
            <div>
                <Row className="mt-3">
                    <Col>
                        <table className="table table-striped tickets-table">
                            <thead className="thead-xs">
                                <tr>
                                    <th className="pl-3">id</th>
                                    <th className="pl-3">Insert Time</th>
                                    <th className="pl-3">IP</th>
                                    <th className="pl-3">Message</th>
                                </tr>
                            </thead>

                            {
                            (true == isLoading) ?
                            <tbody>
                                <tr>
                                    <td colSpan={11} className="text-center">
                                        <Indicator animate className="loading-indicator mt-3"/>
                                        <div className="dashhead-subtitle mt-3">Loading</div>
                                    </td>
                                </tr>
                            </tbody>
                                :
                            <tbody>
                            {
                                logs.map(log =>
                                    <tr key={log.id} className="ticket-row animated flash">
                                        <td className="pl-3">
                                            {log.id}
                                        </td>
                                        <td className="pl-3">
                                            {formatUserDate(log.insertTime)}
                                        </td>
                                        <td className="pl-3">
                                            {log.remoteAddress}
                                        </td>
                                        <td className="pl-3">
                                            <span style={{textDecoration: log.isDeprecated ? "line-through" : "", color: log.isDeprecated ? "#a9a9a9" : "#7fffd4"}}>
                                                {log.message}
                                            </span>
                                        </td>
                                    </tr>
                                )
                            }
                            </tbody>
                            }
                        </table>
                    </Col>
                </Row>

                <Row>
                    <PageListPagination pageList={forgotPasswordPagination}
                                        isLoading={isLoading}
                                        changePageAction={this.onChangePageAction}
                    />
                </Row>

            </div>
        )
    }
}

function mapStateToProps(state) {
    const {forgotPasswordLogs, forgotPasswordPagination} = state.customer;
    return {
        forgotPasswordLogs,
        forgotPasswordPagination
    }
}

const mapDispatchToProps = {
    requestForgotPasswordLogs
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordTable);