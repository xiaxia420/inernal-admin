import React, {Component} from "react";
import {connect} from "react-redux";
import QrScanner from "../QrScanner";
import AnimateInOut from "../../../modules/AnimateInOut";
import {addTicketStatisticFlag, removeTicketStatisticFlag} from "../../../actions/user";
import {ContainerFluidSpacious} from "../../App/Toolkit";
import {Button, Card, FormGroup, Row, Col} from "reactstrap";

class Tickets extends Component {

    state = {
        showScanner: true,
        searchKeyword: "",
        error: null,
        info: null
    };

    onSearchKeywordChange = (e) => {
        let inputValue = e.target.value;
        this.setState({
            searchKeyword : inputValue,
            error: null,
            info: null
        });
    };

    handleScan = (scanResult) => {
        // https://www.activbet.com/check/ticket/256992

        this.setState(
            {
                searchKeyword: "",
                error: null,
                info: null
            },
            () => {
                if (scanResult) {
                    if (scanResult.includes("/check/ticket/")) {
                        let results = scanResult.split("/check/ticket/");
                        if (results.length > 0) {
                            const ticketNumber = results[results.length - 1];
                            if (ticketNumber) {
                                this.onUpdateTicketStatisticFlag(ticketNumber);
                            }
                        }
                    }
                }
            }
        );
    };

    onUpdateClick = () => {
        const {searchKeyword} = this.state;
        if (searchKeyword) {
            if (searchKeyword.length > 0) {
                this.onUpdateTicketStatisticFlag(searchKeyword);
            }
        }
    };

    onCLear = () => {
        this.setState({
            searchKeyword: "",
            error: null,
            info: null
        })
    };

    onUpdateTicketStatisticFlag = (ticketNumber) => {
        const {flag} = this.props;
        let updateFlag = this.props.removeTicketStatisticFlag;
        if (flag) {
            updateFlag = this.props.addTicketStatisticFlag;
        }

        updateFlag(ticketNumber).then(
            () => {
                this.setState({
                    searchKeyword: "",
                    error: null,
                    info: ticketNumber + (flag ? " : flag is added." : " : flag is removed.")
                })
            }
        ).catch(
            (error) => {
                let errorMessage = "unknown";
                if (error) {
                    errorMessage = error.error;
                    if (error.payload) {
                        errorMessage = errorMessage + " | " + error.payload.description;
                    }
                }

                this.setState({
                    searchKeyword: ticketNumber,
                    error: ticketNumber + " : " + errorMessage,
                    info: null
                })
            }
        )
    };

    onKeyDown = (event) => {
        // enter to trigger search
        if (event.keyCode == 13) {
            this.onUpdateClick();
            event.preventDefault();
        }

        // esc
        if (event.keyCode == 27) {
            this.onCLear();
            event.preventDefault();
        }
    };

    render() {
        const {showScanner, searchKeyword, error, info} = this.state;
        const {flag, webcamEnabled} = this.props;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col style={{flex: '0 0 25rem'}}>
                        <Card key="navigation-filter" color="dark" className="pt-3 pl-3 pr-3">
                            <FormGroup>
                                <Button block color="primary cursor-pointer"
                                        onClick={() => {this.setState({showScanner: !showScanner})}}
                                >
                                    {
                                        (true == showScanner) ? "Hide Scanner" : "Show Scanner"
                                    }
                                </Button>
                            </FormGroup>

                            <FormGroup>
                                {
                                    webcamEnabled && showScanner &&
                                    <div className="c__navside">
                                        <QrScanner
                                            onScanReady={
                                                (result) => {
                                                    this.handleScan(result);
                                                }
                                            }
                                        />
                                    </div>
                                }
                            </FormGroup>
                        </Card>
                    </Col>

                    <Col>
                        <div className="mt-2">
                            <h6>
                                <span title="">
                                    {flag ? "Add flag" : "Remove flag"}
                                </span>
                            </h6>

                            <input id="search_ticket"
                                name="search_ticket"
                                className="form-control"
                                type="text"
                                placeholder=""
                                onChange={this.onSearchKeywordChange}
                                value={searchKeyword}
                                onKeyDown={this.onKeyDown}
                            />

                            <div role="group" className="btn-group" >
                                <Button color="primary"
                                        onClick={this.onUpdateClick}
                                >
                                            {flag ? "Add" : "Remove"}
                                </Button>

                                <Button color="primary"
                                        onClick={this.onCLear}>
                                    Clear
                                </Button>

                            </div>

                            {error &&
                            <div className="text-danger">{error}</div>
                            }

                            {info &&
                            <div className="text-success">{info}</div>
                            }

                        </div>

                    </Col>


                </Row>
            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state, props) {
    const {webcamEnabled} = state.app;
    return {
        webcamEnabled
    }
}

const mapDispatchToProps = {
    addTicketStatisticFlag, removeTicketStatisticFlag
};

export default connect(mapStateToProps, mapDispatchToProps)(Tickets);