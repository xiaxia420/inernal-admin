import React, {Component} from "react";
import {connect} from "react-redux";
import {Col, Row} from "reactstrap";
import Indicator from "../../Module/Indicator";
import {Percent, Currency} from "../../App/Toolkit";
import classNames from "classnames";

class TicketStatisticsTable extends Component {

    renderStatistic = (statistic, type, showBorder) => {
        if (!statistic) {
            return null;
        }

        const {
            companyName1, companyName2, count, payIn, stakeFee, stake, openCount, openPayIn, cancelledCount, cancelledPayIn,
            evaluatedCount, evaluatedPayIn, win, winFee, payOut,
            revenue, revenuePercent, cashFlow, cashFlowPercent, companyId
        } = statistic;

        // payIn = stakeFee + stake
        const isWarningPayIn = (Math.abs(payIn - stakeFee - stake) > 1);
        // evaluated, win = fee + pay out
        const isWarningPayOut = (Math.abs(win- winFee - payOut) > 1);

        return (
            <tr key={companyId + type}>
                {
                    (type == "ALL") &&
                    <td className={classNames("border-right", {"border-bottom": showBorder})}>{companyName1} {companyName2} - ALL</td>
                }
                {
                    (type == "SHOP_BASED") &&
                    <td className={classNames("border-right", {"border-bottom": showBorder})}>{companyName1} {companyName2} - Shop</td>
                }
                {
                    (type == "SHOP_REGISTERED") &&
                    <td className={classNames("border-right", {"border-bottom": showBorder})}>{companyName1} {companyName2} - Online</td>
                }

                <td className={classNames({"border-bottom": showBorder})}>{count}</td>
                <td className={classNames({"border-bottom": showBorder})} style={{color: isWarningPayIn ? "#8B0000" : null}}><Currency value={payIn} /></td>
                <td className={classNames({"border-bottom": showBorder})} style={{color: isWarningPayIn ? "#8B0000" : null}}><Currency value={stakeFee} /></td>
                <td className={classNames("border-right", {"border-bottom": showBorder})} style={{color: isWarningPayIn ? "#8B0000" : null}}><Currency value={stake} /></td>


                <td className={classNames({"border-bottom": showBorder})}>{openCount}</td>
                <td className={classNames({"border-bottom": showBorder})}><Currency value={openPayIn}/></td>
                <td className={classNames("border-right", {"border-bottom": showBorder})}>
                    {
                        openPayIn && payIn && (payIn > 0) &&
                        <Percent value={openPayIn / payIn * 100}/>
                    }
                </td>

                <td className={classNames({"border-bottom": showBorder})}>{cancelledCount}</td>
                <td className={classNames("border-right", {"border-bottom": showBorder})}><Currency value={cancelledPayIn}/></td>

                <td className={classNames({"border-bottom": showBorder})}>{evaluatedCount}</td>
                <td className={classNames({"border-bottom": showBorder})}><Currency value={evaluatedPayIn}/></td>
                <td className={classNames({"border-bottom": showBorder})} style={{color: isWarningPayOut ? "#8B0000" : null}}><Currency value={win}/></td>
                <td className={classNames({"border-bottom": showBorder})} style={{color: isWarningPayOut ? "#8B0000" : null}}><Currency value={winFee}/></td>
                <td className={classNames("border-right", {"border-bottom": showBorder})} style={{color: isWarningPayOut ? "#8B0000" : null}}><Currency value={payOut}/></td>

                <td className={classNames({"border-bottom": showBorder})}><Currency value={revenue}/></td>
                <td className={classNames("border-right", {"border-bottom": showBorder})}><Percent value={revenuePercent}/></td>

                <td className={classNames({"border-bottom": showBorder})}><Currency value={cashFlow}/></td>
                <td className={classNames({"border-bottom": showBorder})}><Percent value={cashFlowPercent}/></td>
            </tr>
        );
    };

    render() {
        const {statistics} = this.props;
        return (
            <Row className="mt-3">
                <Col>
                    {
                        statistics.map( (statistic, i) =>
                            <table className="table table-striped" key={i}>
                                <thead className="thead-xs">
                                <tr>
                                    <th colSpan={1}></th>
                                    <th colSpan={4} className="text-center">ALL</th>
                                    <th colSpan={3} className="text-center">OPEN</th>
                                    <th colSpan={2} className="text-center">CANCELLED</th>
                                    <th colSpan={5} className="text-center">EVALUATED</th>
                                    <th colSpan={2} className="text-center">HOLD</th>
                                    <th colSpan={2} className="text-center">CASHFLOW</th>
                                </tr>

                                <tr>
                                    <th className="border-right" style={{width: "10rem"}}>COMPANY</th>

                                    <th>#</th>
                                    <th>PAY-IN</th>
                                    <th>FEE</th>
                                    <th className="border-right">STAKE</th>

                                    <th>#</th>
                                    <th>PAY-IN</th>
                                    <th className="border-right">%</th>

                                    <th>#</th>
                                    <th className="border-right">PAY-IN</th>

                                    <th>#</th>
                                    <th>PAY-IN</th>
                                    <th>WIN</th>
                                    <th>Win-TAX</th>
                                    <th className="border-right">PAY-OUT</th>

                                    <th>HOLD</th>
                                    <th className="border-right">%</th>

                                    <th>CASH</th>
                                    <th>%</th>

                                </tr>
                                </thead>

                                <tbody>

                                {[
                                    this.renderStatistic(statistic.allStatistic, "ALL", !statistic.shopBasedStatistic && !statistic.shopRegisteredStatistic),
                                    this.renderStatistic(statistic.shopBasedStatistic, "SHOP_BASED", !statistic.shopRegisteredStatistic),
                                    this.renderStatistic(statistic.shopRegisteredStatistic, "SHOP_REGISTERED", true)
                                ]}
                                </tbody>
                            </table>

                        )
                    }
                </Col>
            </Row>
        )
    }
}

export default TicketStatisticsTable;