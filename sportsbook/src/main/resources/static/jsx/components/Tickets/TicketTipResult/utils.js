import Immutable from "immutable";

export function splitEndAndPeriods(results) {

    //const endResults = results.filter(x => x.playTimePeriodFlag >= 33554431);
    const endResults = results.filter(x => x.playTimePeriodFlag == "FULL_TIME");
    const endResult = endResults.reduce((p, v) => p.playTimePeriodFlag > v.playTimePeriodFlag ? p : v, Immutable.fromJS({home:"–", away:"–"}));

    const periodResults = results
        .filter(r => r.playTimePeriodFlag !== endResult.playTimePeriodFlag)
        .sort((a, b) => a.playTimePeriodFlag > b.playTimePeriodFlag ? 1 : -1);

    return {endResult, periodResults};
}