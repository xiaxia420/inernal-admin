import React from "react";
import {splitEndAndPeriods} from "./utils";

const GridTicketTipResult = ({results}) => {

    const {endResult, periodResults} = splitEndAndPeriods(results);
    // Don't show the result of the second period in soccer (only half-time and end-result)
    return (
        <table className="results-table table-sm">
            <tbody>
            <tr>
                <td>{endResult.home}</td>
                {periodResults.map(result =>
                    <td key={result.playTimePeriodFlag}>{result.home}</td>
                )}
            </tr>
            <tr className="c__account-bets__game-row">
                <td>{endResult.away}</td>
                {periodResults.map(result => <td key={result.playTimePeriodFlag}>{result.away}</td>)}
            </tr>
            </tbody>
        </table>
    )

};

export default GridTicketTipResult;