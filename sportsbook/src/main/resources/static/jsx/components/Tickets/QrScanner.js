import React, {Component} from "react";
import PropTypes from 'prop-types';
import QrReader from 'react-qr-reader';

class QrScanner extends Component {

    state = {
        isScanning: false
    };

    handleScan = (data) => {
        const {isScanning} = this.state;
        if (false == isScanning) {
            if (data) {
                this.setState({
                        isScanning: true
                    }, () => {
                        this.props.onScanReady(data);
                        setTimeout(() => {
                            this.setState({isScanning: false});
                    }, 600);
                    }
                );
            }
        }
    };

    handleError = (error) => {
        // do nothing right now
    };

    handleLoad = (data) => {
        // do nothing right now
    };

    render() {
        const {isScanning} = this.state;

        return (
            <span>
                <div style={{display: isScanning ? "none" : "inline"}}>
                    <QrReader
                        delay={200}
                        resolution={900}
                        //style={{width: "100%"}}
                        showViewFinder={true}
                        legacyMode={false}
                        onError={this.handleError}
                        onScan={this.handleScan}
                        onLoad={this.handleLoad}
                    />
                </div>

                <div style={{display: isScanning ? "flex" : "none", justifyContent: "center", alignItems: "center", width: "100%"}}>
                    <img
                        src={"https://fra1.digitaloceanspaces.com/activbet/webapp-static-resources/activbet/printer/logo.png"}
                    />
                </div>
            </span>

        )
    }
}

QrScanner.propTypes = {
    onScanReady: PropTypes.func.isRequired
};


export default QrScanner;