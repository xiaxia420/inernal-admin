import React, {Component} from "react";
import {ContainerFluidSpacious, prepareStake} from "../App/Toolkit";
import {
    Button,
    ButtonGroup,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    InputGroup, InputGroupAddon, InputGroupText,
    Row,
    UncontrolledDropdown
} from "reactstrap";
import {connect} from "react-redux";
import {requestTickets, resetTicketsFilter, clearTickets, openTicketEditor} from "../../actions/ticket";
import PaymentDirectionDropDown from "../Account/PaymentTransaction/PaymentDirectionDropDown";
import {DateTimePicker} from "react-widgets";
import TicketDetailTable from "./TicketDetailTable/index";
import PageListPagination from "../App/PageListPagination";
import TicketStatisticsTable from "./TicketStatistics/TicketStatisticsTable";
import TicketEvaluation from "./TicketEvaluation"
import Indicator from "../Module/Indicator";
import {parse} from "qs";
import IdNameDropDown from "../Module/IdNameDropDown";
import {selectUserCurrency} from "../../actions/user";

export const TICKET_ACTIVES = [
    {id: 0, name: "NONE"},
    {id: 1 << 0, name: "OPEN"},
    {id: 1 << 1, name: "REFUND"},
    {id: 1 << 2, name: "LOST"},
    {id: 1 << 3, name: "WON"},
    {id: 1 << 4, name: "CANCELLED"},
    {id: 1 << 5, name: "EVALUATED"},
    {id: 1 << 6, name: "PAYOUT_READY"},
    {id: 1 << 7, name: "PAID_OUT"},
    {id: 1 << 8, name: "CASHOUT"},
    {id: 1 << 9, name: "REQUEST"},
    {id: 1 << 10, name: "PAYMENT_ERROR"},
    {id: 1 << 11, name: "NOT_VISIBLE_TO_CUSTOMER"},
];

class Tickets extends Component {

    componentDidMount() {
        /*
        http://localhost:9988/ticket/tickets?username=xiaxi421&popup
        */
        const search = this.props.location;
        if (search) {
            const query = parse(location.search.substr(1));
            const username = query.username;
            if (username) {
                const {ticketsFilter} = this.props;
                this.props.resetTicketsFilter(
                    {
                        ...ticketsFilter,
                        customerUsername: username
                    }
                ).then(
                    () => {
                        const {pageIndex, pageSize} = this.props.ticketsPagination;
                        this.props.requestTickets(pageIndex, pageSize);
                    }
                );
            }
        }
    }

    onTicketActiveChange = (id) => {
        const {ticketsFilter} = this.props;
        this.props.resetTicketsFilter({
            ...ticketsFilter,
            ticketActive: id
        });
    };

    onCustomerUsernameChange = (e) => {
        const {ticketsFilter} = this.props;
        this.props.resetTicketsFilter({
            ...ticketsFilter,
            customerUsername: e.target.value
        });
    };

    onTicketNumberChange = (e) => {
        const {ticketsFilter} = this.props;
        this.props.resetTicketsFilter({
            ...ticketsFilter,
            ticketNumber: e.target.value
        });
    };

    onInsertFromChange = (from) => {
        const {ticketsFilter} = this.props;
        this.props.resetTicketsFilter({
            ...ticketsFilter,
            insertFrom: from
        });
    };

    onInsertToChange = (to) => {
        const {ticketsFilter} = this.props;
        this.props.resetTicketsFilter({
            ...ticketsFilter,
            insertTo: to
        });
    };

    onStakeFromChange = (e) => {
        const {ticketsFilter} = this.props;
        this.props.resetTicketsFilter({
            ...ticketsFilter,
            stakeFrom: prepareStake(e.target.value)
        });
    };

    onStakeToChange = (e) => {
        const {ticketsFilter} = this.props;
        this.props.resetTicketsFilter({
            ...ticketsFilter,
            stakeTo: prepareStake(e.target.value)
        });
    };

    onCurrencyChange = (id) => {
        this.props.selectUserCurrency(id);
    };

    onShowBetChange = (e) => {
        const {ticketsFilter} = this.props;
        this.props.resetTicketsFilter({
            ...ticketsFilter,
            showBets: e.target.checked
        });
    };

    requestTickets = () => {
        const {pageIndex, pageSize} = this.props.ticketsPagination;
        this.props.requestTickets(pageIndex, pageSize);
    };

    clearTickets = () => {
        this.props.clearTickets();
    };

    onChangePageAction = (pageIndex, pageSize) => {
        this.props.requestTickets(pageIndex, pageSize);
    };

    render() {
        const {ticketsPagination, ticketsTotalStatistic, currencies, userCurrencyId} = this.props;
        const {tickets, isLoading} = this.props.tickets;
        const {ticketActive, customerUsername, ticketNumber, insertFrom, insertTo, stakeFrom, stakeTo, showBets} = this.props.ticketsFilter;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-titles">
                                <h3 className="dashhead-title">Tickets</h3>
                            </div>

                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* show all bet */}
                                <div style={{paddingTop: "0.5rem", paddingLeft: "1rem"}}>
                                    <div className="checkbox-slider--b-flat">
                                        <label>
                                            <input
                                                onChange={this.onShowBetChange}
                                                checked={showBets}
                                                className="form-control"
                                                type="checkbox"
                                            />
                                            <span>{(true == showBets) ? "Dismiss Bets" : "Show Bets"}</span>
                                        </label>
                                    </div>
                                </div>

                                {/* select currency */}
                                <div className="ml-3">
                                    <IdNameDropDown
                                        id={userCurrencyId}
                                        addPlaceholder={true}
                                        placeholder="Currency"
                                        list={currencies}
                                        listLoading={isLoading}
                                        selectionChanged={this.onCurrencyChange}
                                        showDeleteButton={false}
                                    />
                                </div>

                                {/* customerUsername */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Customer username"
                                               value={customerUsername}
                                               onChange={this.onCustomerUsernameChange}
                                        />
                                    </InputGroup>
                                </div>

                                {/* ticketActive */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={ticketActive}
                                                              addPlaceholder={true}
                                                              placeholder="Select status"
                                                              list={TICKET_ACTIVES}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onTicketActiveChange}/>
                                </div>

                                {/* ticketNumber */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Ticket number"
                                               value={ticketNumber}
                                               onChange={this.onTicketNumberChange}
                                        />
                                    </InputGroup>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* insertFrom */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={insertFrom} format="L LT" placeholder="Insert from" onChange={this.onInsertFromChange}/>
                                </div>

                                {/* insertTo */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={insertTo} format="L LT" placeholder="Insert to" onChange={this.onInsertToChange}/>
                                </div>

                                {/* stakeFrom */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Stake from"
                                               value={stakeFrom}
                                               onChange={this.onStakeFromChange}
                                        />
                                    </InputGroup>
                                </div>

                                {/* stakeTo */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Stake to"
                                               value={stakeTo}
                                               onChange={this.onStakeToChange}
                                        />
                                    </InputGroup>
                                </div>

                                <div className="ml-3">
                                    <Button color="primary"
                                            disabled={isLoading}
                                            onClick={this.requestTickets}
                                    >
                                        Search
                                    </Button>

                                    <Button color="dark"
                                            onClick={this.clearTickets}
                                    >
                                        Clear
                                    </Button>
                                </div>

                            </div>
                        </div>
                    </Col>
                </Row>

                {
                    (true == isLoading) &&
                    <div className="text-center">
                        <Indicator animate className="loading-indicator mt-3"/>
                        <div className="dashhead-subtitle mt-3">Loading</div>
                    </div>
                }


                {
                    ticketsTotalStatistic &&
                    <TicketStatisticsTable
                        statistics={ [{allStatistic: ticketsTotalStatistic}] }
                    />
                }

                {/* tickets */}
                {
                    tickets && (tickets.length > 0) &&
                    <TicketDetailTable
                        tickets={tickets}
                        showBets={showBets}
                        openTicketEditor={this.props.openTicketEditor}
                    />
                }

                {/* pagination */}
                {
                    tickets && (tickets.length > 0) &&
                    <Row>
                        <PageListPagination pageList={ticketsPagination}
                                            isLoading={isLoading}
                                            changePageAction={this.onChangePageAction}
                        />
                    </Row>

                }

                {
                    null == tickets &&
                    <div>No record</div>
                }

                <TicketEvaluation />

            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {ticketsFilter, tickets, ticketsPagination, ticketsTotalStatistic} = state.ticket;
    const {currencies, userCurrencyId} = state.user;
    return {
        ticketsFilter, tickets, ticketsPagination, ticketsTotalStatistic,
        currencies, userCurrencyId
    };
}

const mapDispatchToProps = {
    requestTickets, resetTicketsFilter, clearTickets, openTicketEditor,
    selectUserCurrency
};

export default connect(mapStateToProps, mapDispatchToProps)(Tickets);