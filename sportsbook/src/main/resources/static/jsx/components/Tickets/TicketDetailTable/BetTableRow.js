import React, {Component} from "react";
import classNames from "classnames";

import TicketTipResult from "../TicketTipResult";
import {formatUserDate, Odd} from "../../App/Toolkit";
import {SportIcon} from "../../App/Icons";

class BetTableRow extends Component {
    render() {
        const {bet} = this.props;
        return (
            <tr>
                <td style={{width: "8rem"}} className="pl-3">
                    {formatUserDate(bet.gameStartTime)}
                </td>

                <td style={{width: "1em"}}>
                    <SportIcon iconName={bet.sportIconName} className="tickets-sport-icon"/>
                </td>

                <td style={{width: "4%"}}>
                    {bet.sportName}
                </td>

                <td style={{width: "1em"}}>
                    <span className={`sprite__navside-flag__${bet.categoryIconName}`} />
                </td>

                <td style={{width: "8%"}} className="pl-0">
                    {bet.categoryName}
                </td>

                <td style={{width: "10%"}}>{bet.tournamentName}</td>
                <td style={{width: "3em"}}>
                    {(true == bet.isBank) && <span className="bet-bank" title="Bank">B</span>}
                </td>
                <td style={{width: "16%"}} className="pl-0">
                    {bet.gameName}
                    {bet.rivalNames && bet.rivalNames.join(" - ")}
                </td>
                <td style={{width: "8%"}}>{bet.tipName}</td>
                <td style={{width: "14%"}} className={classNames({
                    "text-success": (true == bet.isWon) || (bet.adjustedOddValue),
                    "text-danger": (true == bet.isLost)
                })}>{bet.oddName}</td>

                <td style={{width: "3%"}} title={(() => {
                    if (true == bet.isWon) return "Won";
                    if (true == bet.isLost) return "Lost";
                    if (true == bet.isRefund) return "Refunded";
                    if (true == bet.isHalfLost) return "Half Lost";
                    if (true == bet.isHalfWon) return "Half Won";
                })()}>
                    <Odd isWon={(!bet.adjustedOddValue) && (true == bet.isWon)}
                         isLost={true == bet.isLost}
                         value={bet.oddValue} />
                    {bet.adjustedOddValue && <span><Odd isWon={true} value={bet.adjustedOddValue} /></span>}
                </td>
                <td style={{width: "4rem"}}>
                    {(true == bet.isLive) &&
                    <span className="bet-live">
                        LIVE
                        <span>@{bet.scoreHome}:{bet.scoreAway}</span>
                    </span>
                    }
                </td>
                <td style={{width: "8rem"}} className="text-right pr-2">
                    {(bet.gameStatus == "Cancelled") && "Cancelled"}
                    <TicketTipResult sportIconName={bet.sportIconName} results={bet.gameResults}/>
                </td>
            </tr>
        )
    }
}

export default BetTableRow;