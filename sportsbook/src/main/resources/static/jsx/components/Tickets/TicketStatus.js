import React, {Component} from "react";

class TicketStatus extends Component {
    render() {
        const {ticket} = this.props;
        return (
            <span>
                {(true == ticket.isOpen) && <span className="ticket-status-open" title="Open">O</span>}
                {(true == ticket.isPaymentError) && <span className="ticket-status-payment-error" title="Payment Error">PE</span>}
                {(true == ticket.isRefund) && <span className="ticket-status-refunded" title="Refunded">R</span>}
                {(true == ticket.isLost) && <span className="ticket-status-lost" title="Lost">L</span>}
                {(true == ticket.isWon) && <span className="ticket-status-won" title="Won">W</span>}
                {(true == ticket.isCancelled) && <span className="ticket-status-cancelled" title="Cancelled">C</span>}
                {(true == ticket.isCashOut) && <span className="ticket-status-cashout" title="Cash Out">CO</span>}
                {(true == ticket.isPayedOut) && <span className="ticket-status-payed-out" title="Payed Out">P</span>}
                {(true == ticket.isPayoutReady) && <span className="ticket-status-payout-ready" title="Pay Out Ready">PR</span>}
                {(true == ticket.isRequest) && <span className="ticket-status-request" title="Request">Q</span>}
                {(true == ticket.isNotStatistic) && <span className="ticket-status-open" title="Not Statistic">S</span>}
            </span>
        )
    }
}

export default TicketStatus;