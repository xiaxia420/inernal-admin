import React, {Component} from "react";

import DefaultTicketTipResult from "./default";
import GridTicketTipResult from "./grid";

class TicketTipResult extends Component {

    render() {
        let {sportIconName, results} = this.props;

        if (!sportIconName) {
            sportIconName = "";
        }

        if (!results) {
            return "-:-";
        }

        switch (sportIconName.toUpperCase()) {
            case "TE":
            case "TT":
            case "VB":
            case "BB":
            case "AF":
            case "RG":
            case "IH":
            case "SN":
            case "BA":
                return <GridTicketTipResult results={results}/>;
            case "FB":
                // Don't show the result of the second period in soccer (only half-time and end-result)
                return <DefaultTicketTipResult results={results.filter(r => r.playTimePeriodFlag !== "PERIOD_2")}/>
            default:
                return <DefaultTicketTipResult results={results}/>;
        }
    }
}

export default TicketTipResult;