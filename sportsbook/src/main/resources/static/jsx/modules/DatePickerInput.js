import React, {Component} from "react";
import PropTypes from 'prop-types';
import DayPicker from "react-day-picker";
import {FormattedMessage} from "react-intl";
import {LOCALE_UTILS} from "./DatePicker";
import * as Utils from "../utils";

class DatePickerInput extends Component {

    constructor(props) {
        super(props);
        this.handleDayClick = this.handleDayClick.bind(this);

        const {fromDate} = this.props;
        let today = new Date(fromDate);
        this.state = {
            lastSelectedDay : today
        };
    }

    getFirstDayOfWeek = (d) => {
        const {locale} = this.props;
        d = new Date(d);
        var day = d.getDay();

        if (locale == "en") {
            // first day of a week is Sunday
            var diff = d.getDate() - day; // adjust when day is sunday
        } else {
            // first day of a week is Monday
            var diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
        }
        return new Date(d.setDate(diff));
    };

    handleDayClick = (day, {selected}) => {
        //const {filterRange} = this.state;
        const {filterRange} = this.props;

        // only treat select
        if (!selected) {
            if (filterRange == "DAY") {
                // select a day
                let days = [day];
                this.props.requestHistoryNavigationSports(Utils.formatDate(day), "DAY")
                    .then(
                    () => {
                        this.setState({
                            lastSelectedDay: day
                        });
                    }
                );
            } else {
                // select a day in a week
                let monday = this.getFirstDayOfWeek(day);
                this.props.requestHistoryNavigationSports(Utils.formatDate(monday), "WEEK")
                    .then(
                    () => {
                        this.setState({
                            //selectedDays : days,
                            lastSelectedDay: day
                        });
                    }
                );
            }
        }
    };

    onFilterRangeChange = (event) => {
        //const {filterRange, selectedDays} = this.state;
        const {filterRange, fromDate} = this.props;

        if (event.target.checked) {
            // show week
            if (filterRange == "DAY") {
                // change from day to week
                let monday = this.getFirstDayOfWeek(fromDate);
                this.props.requestHistoryNavigationSports(Utils.formatDate(monday), "WEEK");
            }
        } else {
            // select day show
            if (filterRange == "WEEK") {
                // change from week to day
                let day = this.state.lastSelectedDay;
                this.props.requestHistoryNavigationSports(Utils.formatDate(day), "DAY");
            }
        }

    };

    render() {
        const {locale, fromDate, filterRange} = this.props;

        let days = [];
        let today = new Date(fromDate);
        if (filterRange == "DAY") {
            days.push(today);
        } else {
            let monday = this.getFirstDayOfWeek(today);
            for (var i = 0; i < 7; i++) {
                let nextDay = new Date(monday.getTime());
                nextDay.setDate(monday.getDate() + i);
                days.push(nextDay);
            }
        }

        /*
        style={{position: "relative", left: "3rem"}}
        style={{position: "relative", left: "3.5rem", verticalAlign: "top"}} htmlFor="selectWeek"
         */

        //
        return (
            <div style={{width: "100%"}}>
                <div className="checkbox checkbox-inline" style={{display: "table", margin: "0 auto", paddingTop: "1rem"}}>
                    <input
                           type="checkbox"
                           id="selectWeek"
                           name="selectWeek"
                           onChange={this.onFilterRangeChange}
                           checked={filterRange == "WEEK"}
                    />
                    <label htmlFor="selectWeek" className="c__footer__label--black">
                        <FormattedMessage id="history.filter.weekly_view" />
                    </label>
                </div>

                <DayPicker
                    locale={locale}
                    localeUtils={LOCALE_UTILS}
                    selectedDays={days}
                    onDayClick={this.handleDayClick}
                >
                </DayPicker>

            </div>
        );
    }

}

export default DatePickerInput;