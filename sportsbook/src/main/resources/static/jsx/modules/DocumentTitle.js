import withSideEffect from "react-side-effect";
import {changeTitle} from "../actions/app";
import React, {Component, Children} from "react";
import PropTypes from 'prop-types';

class DocumentTitle extends Component {

    render() {
        const {children} = this.props;

        if (children) {
            return Children.only(children);
        } else {
            return null;
        }
    }
}

function reducePropsToState(propsList) {
    var innermostProps = propsList[propsList.length - 1];
    if (innermostProps) {
        return innermostProps;
    }
}

function handleStateChangeOnClient(props) {
    if (props && props.title) {
        document.title = props.title;
        props.store.dispatch(changeTitle(props.title));
    } else {
        document.title = "";
    }
}

/* Necessary for .rewind() to work */
DocumentTitle.displayName = "DocumentTitle";
DocumentTitle.propTypes = {
    title: PropTypes.string.isRequired,
    store: PropTypes.object.isRequired
};

export default withSideEffect(
    reducePropsToState,
    handleStateChangeOnClient
)(DocumentTitle);