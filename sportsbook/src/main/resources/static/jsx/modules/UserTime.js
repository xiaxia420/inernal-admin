import React from "react";
import PropTypes from 'prop-types';
import {FormattedTime} from "react-intl";

const UserTime = props => (
    /* Force UTC Timezone, because the times come pre-adjusted from the server, display them as-is */
    <FormattedTime hour="numeric" minute="2-digit" {...props} timeZone="UTC" hour12={false}/>
);

UserTime.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
};

export default UserTime;