import React from "react";
import PropTypes from 'prop-types';
import DocumentTitle from "./DocumentTitle";

const PageTitle = ({children, id, values}, {intl, store}) => (
    <DocumentTitle title={ intl.formatMessage({id}, values) } store={ store }>
        {children}
    </DocumentTitle>
);

PageTitle.propTypes = {
    id: PropTypes.string.isRequired,
    values: PropTypes.object
};

PageTitle.contextTypes = {
    intl: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
};

export default PageTitle;