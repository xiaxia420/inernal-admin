import React, {Component} from "react";
import PropTypes from 'prop-types';
import ReactRecaptcha from "react-recaptcha";
import classNames from "classnames";

const isLoaded = () => typeof window !== 'undefined' && typeof window.grecaptcha !== 'undefined';

class Recaptcha extends Component {

    componentDidMount() {
        const {locale} = this.props;

        if (!isLoaded()) {
            // Async lazy load
            const head = document.head || document.getElementsByTagName('head')[0];
            const script = document.createElement('script');
            script.src = `https://www.google.com/recaptcha/api.js?hl=${locale}`;
            script.type = 'text/javascript';
            script.async = true;
            script.defer = true;

            head.appendChild(script);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.reset && nextProps.reset) {
            this.recaptcha.reset();
        }
    }

    render() {
        const {id, className, siteKey, expiredCallback, callback, locale} = this.props;
        return (
            <div className="c__recaptcha-wrap">
                <ReactRecaptcha
                    elementID={id}
                    sitekey={siteKey}
                    theme="dark"
                    render="explicit"
                    onloadCallback={() => {}} // Must be set for explicit rendering and elementID to be used
                    className={classNames('c__recaptcha-code g-recaptcha', className)}
                    expiredCallback={expiredCallback}
                    verifyCallback={callback}
                    hl={locale}
                    ref={(ref) => this.recaptcha = ref}
                />
            </div>
        );
    }
}

Recaptcha.propTypes = {
    id: PropTypes.string.isRequired,
    callback: PropTypes.func.isRequired,
    expiredCallback: PropTypes.func.isRequired,
    siteKey: PropTypes.string.isRequired,
    locale: PropTypes.string.isRequired,
    className: PropTypes.string
};

export default Recaptcha;
