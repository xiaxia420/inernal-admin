import React, {Component} from "react";
import PropTypes from 'prop-types';

class Pagination extends Component {

    getPages = (page, pageCount) => {
        // Show max 5 pages at a time
        let pages = [];

        // How many pages are displayed at once max, must be uneven
        const pagesShown = 5;
        const pagesPadding = Math.floor(pagesShown / 2);

        if (pageCount <= pagesShown) {
            for (let i = 1; i <= pageCount; i++) {
                pages.push(i);
            }
            return pages;
        }

        let start;
        let end;
        if (page + pagesPadding > pageCount) {
            start = pageCount - (pagesShown - Math.max(pageCount - page, 1));
            end = pageCount;
        } else if (page - pagesPadding < 1) {
            start = 1;
            end = pagesShown;
        } else {
            start = page - pagesPadding;
            end = page + pagesPadding;
        }

        for (let i = start; i <= end; i++) {
            pages.push(i);
        }

        if (pages[0] > 1) {
            // ... at front
            pages[0] = "ph-prev";
        }

        if (pages[pagesShown - 1] < pageCount) {
            // ... at end
            pages[pagesShown - 1] = "ph-next";
        }

        return pages;
    };

    onPageClicked = page => () => {
        this.props.onChange(page);
    };

    render() {

        const {page, pageCount} = this.props;

        const isFirstPage = page === 1;
        const isLastPage = page === pageCount;

        if (pageCount <= 1) {
            return <div></div>;
        }

        return (
            <div className="m__pagination__wrap">
                <ul className="m__pagination">
                    <li className={ "m__pagination__li" + (isFirstPage ? " m__pagination__li--inactive" : "")}>
                        <a className="m__pagination__link m__pagination__link--action m__pagination__arrow-left-double t__cursor__pointer"
                           onClick={isFirstPage ? null : this.onPageClicked(1)}>fast-backward</a>
                    </li>
                    <li className={ "m__pagination__li" + (isFirstPage ? " m__pagination__li--inactive" : "") }>
                        <a className="m__pagination__link m__pagination__link--action m__pagination__arrow-left t__cursor__pointer"
                           onClick={isFirstPage ? null : this.onPageClicked(page - 1)}>backward</a>
                    </li>
                    { this.getPages(page, pageCount).map((p, i) =>
                        <li key={p}
                            className={ "m__pagination__li" + (p === page ? " m__pagination__li--active" : "") }>
                            { typeof p === "string"
                                ? <span className="m__pagination__link">&hellip;</span>
                                : <a className="m__pagination__link t__cursor__pointer"
                                     onClick={this.onPageClicked(p)}>{p}</a>
                            }
                        </li>
                    )}
                    <li className={ "m__pagination__li" + (isLastPage ? " m__pagination__li--inactive" : "")}>
                        <a className="m__pagination__link m__pagination__link--action m__pagination__arrow-right t__cursor__pointer"
                           onClick={isLastPage ? null : this.onPageClicked(page + 1)}>forward</a>
                    </li>
                    <li className={ "m__pagination__li" + (isLastPage ? " m__pagination__li--inactive" : "")}>
                        <a className="m__pagination__link m__pagination__link--action m__pagination__arrow-right-double t__cursor__pointer"
                           onClick={isLastPage ? null : this.onPageClicked(pageCount)}>fast-forward</a>
                    </li>
                </ul>
            </div>
        )
    }

}


Pagination.contextTypes = {
    store: PropTypes.object.isRequired
};

Pagination.defaultProps = {
    onChange: () => {
    }
};

Pagination.propTypes = {
    pageCount: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    onChange: PropTypes.func
};


export default Pagination;