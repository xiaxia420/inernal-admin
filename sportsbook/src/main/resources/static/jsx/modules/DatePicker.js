import React, {Component} from "react";
import PropTypes from 'prop-types';
import DayPicker, {DateUtils} from "react-day-picker";
import {FormattedMessage} from "react-intl";

export const WEEKDAYS_LONG = {
    de: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
    en: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    zh: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
    tr: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
    it: ["Domenica", "lunedì", "martedì", "mercoledì", "giovedì", "venerdì", "sabato"],
    ru: ["Воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
    es: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    am: ["Կիրակի", "Երկուշաբթի", "Երեքշաբթի", "Չորեքշաբթի", "Հինգշաբթի", "Ուրբաթ", "Շաբաթ"],
    el: ["Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη", "Πέμπτη", "Παρασκευή", "Σάββατο"],
    fr: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"]

};

export const WEEKDAYS_SHORT = {
    de: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
    en: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    zh: ["日", "一", "二", "三", "四", "五", "六"],
    tr: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    it: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    ru: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    es: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    am: ["Կի", "Երկ", "Երե", "Չո", "Հի", "Ու", "Շա"],
    el: ["Κυ", "Δε", "Τρ", "Τε", "Πέ", "Πα", "Σά"],
    fr: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"]
};

export const MONTHS = {
    de: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
    en: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    zh: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    tr: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
    it: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
    ru: ["Январь", "Февраль", "Марш", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
    es: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    am: ["Յունուար", "Փետրուար", "Մարտ", "Ապրիլ", "Մայիս", "Յունիս", "Յուլիս", "Օգոստոս", "Սեպտեմբեր", "Հոկտեմբեր", "Նոյեմբեր", "Դեկտեմբեր"],
    el: ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"],
    fr: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
};

export const FIRST_DAY_OF_WEEK = {
    de: 1,
    en: 0,
    zh: 1,
    tr: 1,
    it: 1,
    ru: 1,
    es: 1,
    am: 1,
    el: 1,
    fr: 1
};

export const LOCALE_UTILS = {
    formatDay: (d, locale = "en") => `${WEEKDAYS_LONG[locale][d.getDay()]}, ${d.getDate()} ${MONTHS[locale][d.getMonth()]} ${d.getFullYear()}`,
    formatWeekdayShort: (index, locale = 'en') => WEEKDAYS_SHORT[locale][index],
    formatWeekdayLong: (index, locale = 'en') => WEEKDAYS_LONG[locale][index],
    getFirstDayOfWeek: locale => FIRST_DAY_OF_WEEK[locale],
    getMonths: locale => MONTHS[locale],
    formatMonthTitle: (d, locale) => `${MONTHS[locale][d.getMonth()]} ${d.getFullYear()}`,
};

class DatePicker extends Component {
    constructor(props) {
        super(props);
        this.dateFormat =  new Intl.DateTimeFormat(props.locale, {
            year: 'numeric', month: '2-digit', day: '2-digit'
        });
    }

    state = {
        showOverlay: false,
        value: "",
        selectedDay: null,
        selectedMonth: null
    };

    componentWillUnmount() {
        clearTimeout(this.clickTimeout);
    }

    input = null;
    clickedInside = false;
    clickTimeout = null;

    handleContainerMouseDown = () => {
        this.clickedInside = true;
        // The input"s onBlur method is called from a queue right after onMouseDown event.
        // setTimeout adds another callback in the queue, but is called later than onBlur event
        this.clickTimeout = setTimeout(() => {
            this.clickedInside = false;
        }, 0);
    };

    handleInputFocus = () => {
        this.setState({
            showOverlay: true,
        });
    };

    handleInputBlur = () => {
        const showOverlay = this.clickedInside;
        this.setState({
            showOverlay,
        });

        // Force input"s focus if blur event was caused by clicking on the calendar
        if (showOverlay) {
            this.input.focus();
        }
    };

    handleDayClick = (day, modifiers, e) => {
        this.props.onChange(this.dateToValue(day));
        this.setState({
            showOverlay: false,
            selectedDay: day,
            selectedMonth: new Date(day.getFullYear(), day.getMonth())
        });
    };

    dateToValue = (date) => {
        const month = date.getMonth() + 1;
        const day = date.getDate();
        return `${date.getFullYear()}-${month < 10 ? `0${month}` : month}-${day < 10 ? `0${day}` :day}`;
    };

    getValueAsDate = () => {
        const {value} = this.props;
        if (!value) {
            return "";
        }
        const dateArray = value.split("-");
        const date = new Date();
        // must first set date before month, otherwise there is a bug
        date.setDate(dateArray[2]);
        date.setMonth(dateArray[1] - 1);
        date.setFullYear(dateArray[0]);
        return date;
    };

    render() {
        const {selectedDay, selectedMonth} = this.state;
        const date = this.getValueAsDate();
        let value = "";
        if (date) {
            try {
                value = this.dateFormat.format(date);
            } catch (e) {
                /* Invalid Date */
            }
        }

        return (
            <div
                className="form-group d__form-group c__account-bets__input-date "
                onMouseDown={this.handleContainerMouseDown}
            >
                <label className="d__form-control__label c__account-bets__label">
                    <FormattedMessage id={this.props.label}/>
                </label>
                <div className="input-group c__account-bets__input-group">
                    <div className="input-group-addon d__form-input__group-addon c__account-bets__cal-icon">
                        <span className="u__calendar"/>
                    </div>
                    <input
                        type="text"
                        ref={(el) => {
                            this.input = el;
                        }}
                        value={value}
                        onChange={() => { /* Do nothing */ }}
                        onFocus={this.handleInputFocus}
                        onBlur={this.handleInputBlur}
                        className="d__form-control__input form-control c__account-bets__timescale-input"
                    />
                    {this.state.showOverlay &&
                    <div className="m__daypicker m__daypicker--show">
                        {/*
                        <DayPicker
                            months={MONTHS}
                            weekdaysLong={WEEKDAYS_LONG}
                            weekdaysShort={WEEKDAYS_SHORT}
                            ref={(el) => {
                                this.daypicker = el;
                            }}
                            onDayClick={this.handleDayClick}
                            selectedDays={day => DateUtils.isSameDay(date, day)}
                        />
                         */}

                        <DayPicker
                            locale={this.props.locale}
                            localeUtils={LOCALE_UTILS}
                            onDayClick={this.handleDayClick}
                            selectedDays={selectedDay}
                            initialMonth={selectedMonth ? selectedMonth : new Date() }
                        />

                    </div>
                    }
                </div>
            </div>
        );
    }
}

DatePicker.defaultProps = {
    value: ""
};

DatePicker.propTypes = {
    label: PropTypes.string.isRequired,
    locale: PropTypes.string.isRequired,
    value: PropTypes.string
};

export default DatePicker;