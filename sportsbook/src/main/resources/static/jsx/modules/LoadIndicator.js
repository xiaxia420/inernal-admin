import React from "react";
import PropTypes from 'prop-types';
import classNames from "classnames";

const LoadIndicator = ({id, size, ...props}) => {
    let circleProps = {
        r: 20,
        cx: 50,
        cy: 50,
        strokeWidth: 3,
        strokeMiterlimit: 10
    }, additionalClassNames = [];

    if (props.color === "white") {
        additionalClassNames.push("loader-white");
    }

    switch (size) {
        case "large":
            additionalClassNames.push("loader-large");
            circleProps = {
                ...circleProps,
                strokeWidth: 4,
                r: 35
            };
            break;
        case "button":
            circleProps = {
                ...circleProps,
                r: 10,
            };
            break;
    }

    return (
        <div className={classNames("loader", ...additionalClassNames)} id={id} {...props}>
            <svg className="circular">
                <circle className="path" fill="none" {...circleProps}/>
            </svg>
        </div>
    )
};

LoadIndicator.propTypes = {
    id: PropTypes.string,
    size: PropTypes.string,
    color: PropTypes.string
};

LoadIndicator.defaultProps = {
    size: "regular"
};

export default LoadIndicator;