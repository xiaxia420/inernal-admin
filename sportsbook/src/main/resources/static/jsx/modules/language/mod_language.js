import {FormattedMessage} from "react-intl";
import React, {Component} from "react";
import PropTypes from 'prop-types';

class ModLanguage extends Component {

    onChange = (event) => {
        this.props.onChange(event.target.value);
    };

    render() {
        const {languages, value} = this.props;
        return (
            <div className="form-group m__language">
                <div className="input-group m__language__wrap">
                    <label><FormattedMessage id="lbl.language"/></label>
                    <select value={ value } className="d__form-control__select form-control m__language__select"
                            onChange={ this.onChange }>
                        { languages.map(language =>
                            <option key={ language.id } value={ language.code }>{ language.name }</option>
                        ) }
                    </select>
                </div>
            </div>
        )
    }

}

ModLanguage.propTypes = {
    languages: PropTypes.array.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};

export default ModLanguage;