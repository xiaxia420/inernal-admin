import ApiRestClient from "../networking/ApiRestClient";
import {
    REQUEST_PAYMENT_PROVIDERS,
    RECEIVE_PAYMENT_PROVIDERS,
    REQUEST_PAYMENT_TRANSACTIONS,
    RECEIVE_PAYMENT_TRANSACTIONS,
    RESET_PAYMENT_TRANSACTIONS_FILTER,
    REQUEST_PAYMENT_CHANNELS,
    RECEIVE_PAYMENT_CHANNELS,
    RESET_PAY_OUT_REQUEST_FILTER,
    REQUEST_PAY_OUT_REQUESTS,
    RECEIVE_PAY_OUT_REQUESTS,
    CLEAR_PAYMENT_TRANSACTIONS,
    CLEAR_PAY_OUT_REQUESTS,
    REQUEST_ACCEPT_PAYMENT_TRANSACTION,
    REQUEST_DENY_PAYMENT_TRANSACTION,
    REQUEST_ACCEPT_PAYOUT_REQUEST,
    REQUEST_DENY_PAYOUT_REQUEST,
    REQUEST_SYNC_PAYMENT_TRANSACTION
} from "../actionTypes/payment";
import {toUTC} from "./app";
import md5 from "md5";

/** request payment providers */
export function requestPaymentProvidersIfNeeded() {
    return (dispatch, getState) => {
        const {paymentProviders} = getState().payment;
        if (!paymentProviders || paymentProviders.length == 0) {
            return dispatch(requestPaymentProviders());
        }
    }
}

function requestPaymentProviders() {
    return (dispatch, getState) =>
        ApiRestClient.send({
        type:  REQUEST_PAYMENT_PROVIDERS
    }).then(
        ({paymentProviders}) => {
            dispatch({type: RECEIVE_PAYMENT_PROVIDERS, paymentProviders})
        }
    );
}

/** request payment channels */
export function requestPaymentChannelsIfNeeded() {
    return (dispatch, getState) => {
        const {paymentChannels} = getState().payment;
        if (!paymentChannels || paymentChannels.length == 0) {
            return dispatch(requestPaymentChannels());
        }
    }
}

function requestPaymentChannels() {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_PAYMENT_CHANNELS
        }).then(
            ({paymentChannels}) => {
                dispatch({type: RECEIVE_PAYMENT_CHANNELS, paymentChannels})
            }
        );
}

/** reset payment transactions filter */
export function resetPaymentTransactionsFilter(filter) {
    return (dispatch, getState) => {
        const {paymentChannels} = getState().payment;
        dispatch({type: RESET_PAYMENT_TRANSACTIONS_FILTER, filter, paymentChannels});
        return new Promise(resolve => resolve());
    };
}

/** request payment transactions */
export function requestPaymentTransactions(pageIndex, pageSize) {
    return (dispatch, getState) => {
        const {paymentTransactionsFilter} = getState().payment;

        const originSignature = paymentTransactionsFilter.signature;
        const signature = signTransactionsFilter(paymentTransactionsFilter);
        if (originSignature !== signature) {
            pageIndex = 0;
            pageSize = 25;
        }

        const {from, to} = paymentTransactionsFilter;

        let localFrom = from;
        let localTo = to;
        if (from) {
            localFrom = toUTC(localFrom);
        }
        if (to) {
            localTo = toUTC(localTo);
        }

        ApiRestClient.send({
            type:  REQUEST_PAYMENT_TRANSACTIONS, pageIndex, pageSize,
            ...paymentTransactionsFilter,
            from: localFrom, to: localTo
        }, false).then(
            ({paymentTransactions, pagination}) => {
                dispatch({type: RECEIVE_PAYMENT_TRANSACTIONS, paymentTransactions, pagination, signature})
            }
        );
    };
}

/** accept a payout transaction */
export function requestAcceptPaymentTransaction(id) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_ACCEPT_PAYMENT_TRANSACTION, id,
        }, true);
}

/** deny a payout transaction */
export function requestDenyPaymentTransaction(id) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_DENY_PAYMENT_TRANSACTION, id,
        }, true);
}

/** synchronize with payment provider */
export function requestSyncPaymentTransaction(id) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_SYNC_PAYMENT_TRANSACTION, id
        }, true);
}

function signTransactionsFilter(paymentTransactionsFilter) {
    const {paymentProviderId, customerId, paymentChannelId, customerUsername, direction, status, from, to} = paymentTransactionsFilter;
    let str = "";
    str = str + (paymentProviderId !== null ? paymentProviderId : "paymentProviderId");
    str = str + (paymentChannelId !== null ? paymentChannelId : "paymentChannelId");
    str = str + (customerId !== null ? customerId : "customerId");
    str = str + customerUsername;
    str = str + (direction !== null ? direction : "direction");
    str = str + (status !== null ? status : "status");
    str = str + (from !== null ? from : "from");
    str = str + (to !== null ? to : "to");
    return md5(str);
}

function signPayOutRequestsFilter(paymentTransactionsFilter) {
    const {companyId, payOutUserId, customerUsername, from, to, keyWord, status, flag} = paymentTransactionsFilter;
    let str = "";
    str = str + (companyId !== null ? companyId : "companyId");
    str = str + (payOutUserId !== null ? payOutUserId : "payOutUserId");
    str = str + (flag !== null ? flag : "flag");
    str = str + customerUsername;
    str = str + (from !== null ? from : "from");
    str = str + (to !== null ? to : "to");
    str = str + keyWord;
    str = str + (status !== null ? status : "status");

    return md5(str);
}

export function clearPaymentTransactions() {
    return (dispatch, getState) => {
        dispatch({type: CLEAR_PAYMENT_TRANSACTIONS})
    }
}

/* reset pay out request filter */
export function resetPayOutRequestsFilter(filter) {
    return (dispatch, getState) => {
        const {allCashiers} = getState().customer;
        dispatch({type: RESET_PAY_OUT_REQUEST_FILTER, filter, allCashiers});
        return new Promise(resolve => resolve());
    };
}

/** request pay out requests */
export function requestPayOutRequests(pageIndex, pageSize) {
    return (dispatch, getState) => {
        const {payOutRequestsFilter} = getState().payment;

        const originSignature = payOutRequestsFilter.signature;
        const signature = signPayOutRequestsFilter(payOutRequestsFilter);
        if (originSignature !== signature) {
            pageIndex = 0;
            pageSize = 25;
        }


        const {from, to} = payOutRequestsFilter;

        let localFrom = from;
        let localTo = to;
        if (from) {
            localFrom = toUTC(localFrom);
        }
        if (to) {
            localTo = toUTC(localTo);
        }

        ApiRestClient.send({
            type:  REQUEST_PAY_OUT_REQUESTS, pageIndex, pageSize,
            ...payOutRequestsFilter,
            from: localFrom, to: localTo
        }, false).then(
            ({payOutRequests, pagination}) => {
                dispatch({type: RECEIVE_PAY_OUT_REQUESTS, payOutRequests, pagination})
            }
        );
    };
}

/** accept payout request */
export function requestAcceptPayOutRequest(id) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_ACCEPT_PAYOUT_REQUEST, id,
        }, true);
}

/** deny payout request */
export function requestDenyPayOutRequest(id) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_DENY_PAYOUT_REQUEST, id,
        }, true);
}

export function clearPayOutRequests() {
    return (dispatch, getState) => {
        dispatch({type: CLEAR_PAY_OUT_REQUESTS})
    }
}

