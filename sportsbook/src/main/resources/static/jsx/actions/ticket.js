import ApiRestClient from "../networking/ApiRestClient";
import {
    CLEAR_CASH_OUT_ANALYTICS,
    CLEAR_TICKET_STATISTICS,
    CLEAR_TICKETS,
    CLOSE_EDIT_TICKET,
    OPEN_EDIT_TICKET,
    RECEIVE_CASH_OUT_ANALYTICS,
    RECEIVE_DELETE_TICKET,
    RECEIVE_TICKET_STATISTICS,
    RECEIVE_TICKETS,
    REQUEST_CASH_OUT_ANALYTICS,
    REQUEST_DELETE_TICKET,
    REQUEST_TICKET_STATISTICS,
    REQUEST_TICKETS,
    RESET_CASH_OUT_ANALYTICS_FILTER,
    RESET_TICKET_STATISTIC_FILTER,
    RESET_TICKETS_FILTER
} from "../actionTypes/ticket";
import {toUTC} from "./app";
import md5 from "md5";

/**
    reset ticket statistic filter
 */
export function resetTicketStatisticFilter(filter) {
    return (dispatch, getState) => {
        dispatch({
            type: RESET_TICKET_STATISTIC_FILTER, filter
        })
    }
}

/**
* request ticket statistics
* */
export function requestTicketStatistics() {
    return (dispatch, getState) => {
        const {ticketStatisticFilter} = getState().ticket;
        const {userCurrencyId} = getState().user;
        const {insertFrom, insertTo} = ticketStatisticFilter;

        let localFrom = insertFrom;
        let localTo = insertTo;

        if (insertFrom) {
            localFrom = toUTC(localFrom);
        }

        if (insertTo) {
            localTo = toUTC(localTo);
        }

        ApiRestClient.send({
            type: REQUEST_TICKET_STATISTICS,
            ...ticketStatisticFilter,
            insertFrom: localFrom, insertTo: localTo,
            currencyId: userCurrencyId
        }, false).then(
            ({ticketStatistics}) => {
                dispatch({
                    type: RECEIVE_TICKET_STATISTICS, ticketStatistics
                })
            }
        )
    };
}

/**
    clear ticket statistics
 */
export function clearTicketStatistics() {
    return (dispatch, getState) => {
        dispatch({
            type: CLEAR_TICKET_STATISTICS
        })
    }
}

/**
    reset ticket filter
 */
export function resetTicketsFilter(filter) {
    return (dispatch, getState) => {
        dispatch({
            type: RESET_TICKETS_FILTER, filter
        });

        return new Promise(resolve => resolve());
    }
}

/**
    request tickets
 */
export function requestTickets(pageIndex, pageSize) {
    return (dispatch, getState) => {
        const {ticketsFilter} = getState().ticket;
        const {userCurrencyId} = getState().user;
        const originSignature = ticketsFilter.signature;
        const signature = signTicketsFilter(ticketsFilter);
        const {insertFrom, insertTo} = ticketsFilter;

        if (originSignature !== signature) {
            pageIndex = 0;
            pageSize = 25;
        }

        let localFrom = insertFrom;
        let localTo = insertTo;

        if (insertFrom) {
            localFrom = toUTC(localFrom);
        }

        if (insertTo) {
            localTo = toUTC(localTo);
        }

        ApiRestClient.send({
            type: REQUEST_TICKETS,
            ...ticketsFilter,
            insertFrom: localFrom, insertTo: localTo, pageIndex, pageSize,
            currencyId: userCurrencyId
        }, false).then(
            ({tickets, pagination, ticketStatistic}) => {
                dispatch({
                    type: RECEIVE_TICKETS, tickets, pagination, ticketStatistic, signature
                })
            }
        )
    };
}

function signTicketsFilter(ticketsFilter) {
    const {companyId, customerUsername, ticketActive, ticketNumber, insertFrom, insertTo, stakeFrom, stakeTo} = ticketsFilter;
    let str = "";
    str = str + (companyId !== null ? companyId : "companyId");
    str = str + customerUsername;
    str = str + (ticketActive !== null ? ticketActive : "ticketActive");
    str = str + ticketNumber;
    str = str + (insertFrom !== null ? insertFrom : "insertFrom");
    str = str + (insertTo !== null ? insertTo : "insertTo");
    str = str + stakeFrom;
    str = str + stakeTo;
    return md5(str);
}

/**
    clear tickets
 */
export function clearTickets() {
    return (dispatch, getState) => {
        dispatch({
            type: CLEAR_TICKETS
        })
    }
}

/**
 * edit ticket
 */
export function openTicketEditor(ticket) {
    return (dispatch, getState) => {
        dispatch({
            type: OPEN_EDIT_TICKET, ticket
        })
    }
}

/**
    close edit ticket
 */
export function closeTicketEditor(ticket) {
    return (dispatch, getState) => {
        dispatch({
            type: CLOSE_EDIT_TICKET, ticket
        })
    }
}

/**
    cancel ticket
 */
export function requestCancelTicket(ticket) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type: REQUEST_DELETE_TICKET, id: ticket.id
        }, false).then(
            ({ticket}) => {
                dispatch({
                    type: RECEIVE_DELETE_TICKET, ticket
                })
            }
        );
}

/**
 * cash out analytics
 * */
export function requestCashOutAnalytics(from, to) {
    return (dispatch, getState) => {
        let localFrom = from;
        let localTo = to;
        if (from) {
            localFrom = toUTC(localFrom);
        }
        if (to) {
            localTo = toUTC(localTo);
        }

        ApiRestClient.send({
            type: REQUEST_CASH_OUT_ANALYTICS, from: localFrom, to: localTo
        }, false).then(
            ({cashOutAnalytics}) => {
                dispatch({
                    type: RECEIVE_CASH_OUT_ANALYTICS, cashOutAnalytics
                })
            }
        )
    }
}

/**
 * clear cash out analytics
 */
export function clearCashOutAnalytics() {
    return (dispatch, getState) => {
        dispatch({
            type: CLEAR_CASH_OUT_ANALYTICS
        })
    }
}

/**
 * change cash out analytics filter
 * */
export function resetCashOutAnalyticsFilter(from, to) {
    return (dispatch, getState) => {
        dispatch({
            type: RESET_CASH_OUT_ANALYTICS_FILTER, from, to
        })
    }
}


