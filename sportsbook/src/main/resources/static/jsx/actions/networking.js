import {CONNECTED, DISCONNECTED, CONNECTION_FORCE_CLOSED} from "../actionTypes/networking";

export function connected() {
    return {
        type: CONNECTED
    }
}

export function disconnected() {
    return {
        type: DISCONNECTED
    }
}

export function connectionForceClosed() {
    return {
        type: CONNECTION_FORCE_CLOSED
    }
}