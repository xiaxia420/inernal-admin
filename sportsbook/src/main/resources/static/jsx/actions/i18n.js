import ApiRestClient from "../networking/ApiRestClient";
import {
    REQUEST_TIMEZONES,
    RECEIVE_TIMEZONES,

    REQUEST_LANGUAGES,
    RECEIVE_LANGUAGES,

    REQUEST_CURRENCIES,
    RECEIVE_CURRENCIES,
} from "../actionTypes/app";
import {
    CHANGE_TIMEZONE_DIFF
} from "../actionTypes/user";


export function requestLanguagesIfNeeded() {
    return (dispatch, getState) => {
        if (getState().app.i18n.languages.length === 0) {
            return dispatch(requestLanguages());
        }
    }
}

export function requestTimezonesIfNeeded() {
    return (dispatch, getState) => {
        if (getState().app.i18n.timezones.length === 0) {
            return dispatch(requestTimezones());
        }
    }
}

export function requestCurrenciesIfNeeded() {
    return (dispatch, getState) => {
        if (getState().user.currencies.length === 0) {
            return dispatch(requestCurrencies());
        }
    }
}

export function requestLanguages() {
    return dispatch => ApiRestClient.send({
        type: REQUEST_LANGUAGES
    }).then(({languages}) => dispatch({
        type: RECEIVE_LANGUAGES,
        languages
    }))
}

export function requestTimezones() {
    return (dispatch, getState) => ApiRestClient.send({
        type: REQUEST_TIMEZONES
    }).then(
        ({timezones}) => {
            if (timezones) {
                if (timezones.length > 0) {
                    const {timezone} = getState().user;
                    let userTimezone = timezones.find(zone => zone.id == timezone);
                    if (userTimezone) {
                        const date = new Date();
                        const dateOffSet = 0 - date.getTimezoneOffset();

                        dispatch({
                            type: CHANGE_TIMEZONE_DIFF,
                            diff: userTimezone.offset - dateOffSet
                        })
                    }
                }
            }

            dispatch({
                type: RECEIVE_TIMEZONES,
                timezones
            })
        }
    )
}

export function requestCurrencies() {
    return dispatch => ApiRestClient.send({
        type: REQUEST_CURRENCIES
    }).then(({currencies}) => dispatch({
        type: RECEIVE_CURRENCIES,
        currencies
    }))
}