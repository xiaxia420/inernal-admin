import {SESSION_EXPIRED} from "../actionTypes/user";

export function sessionExpired() {
    return (dispatch, getState) => {
        const state = getState();

        if (state.user.showForceLogoutModal) {
            // Don't show the session expired modal if the force logout modal is shown
            // Force logout = session removed => would always show both
            return;
        }

        if (state.user.authenticated) {
            return dispatch({
                type: SESSION_EXPIRED
            });
        }

        // If the user isn't logged in, just reload the page.
        window.location.reload();
    }
}