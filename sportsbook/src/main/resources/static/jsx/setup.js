import {browserHistory} from "react-router/es";
import React, {Component} from "react";
import PropTypes from 'prop-types';
import {syncHistoryWithStore} from "react-router-redux";
import configureStore from "./store/configureStore";

import ApiRestClient from "./networking/ApiRestClient";
import log from "loglevel";
import NotificationHandler from "./utils/NotificationHandler";

const initialState = window.__INITIAL_STATE__;

log.setLevel(initialState.user.isDebugMode ? "debug" : "error");

export const store = configureStore(initialState);
export const history = syncHistoryWithStore(browserHistory, store);

ApiRestClient.connect(store);

NotificationHandler.setStore(store);