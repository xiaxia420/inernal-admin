class I18N {
    constructor(props) {
        this.props = props;
    }

    text(key) {
        return this.props[key] || "{" + key + "}";
    }

    textArgs(key, args) {
        var parts = this.text(key).split(/\{\{|}}/g);
        return mapAlternate(parts,
            x => x,
            x => args[x]
        );

    }
}

function mapAlternate(array, fn1, fn2, thisArg) {
    var fn = fn1, output = [];
    for (var i = 0; i < array.length; i++) {
        output[i] = fn.call(thisArg, array[i], i, array);
        // toggle between the two functions
        fn = fn === fn1 ? fn2 : fn1;
    }
    return output;
}

export default I18N;