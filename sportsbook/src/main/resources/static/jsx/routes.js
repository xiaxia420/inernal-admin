import {Route, IndexRoute, IndexRedirect} from "react-router";
import Login from "./components/Login";
import React from "react";
import Error404 from "./components/Error/Error404";
import {requireAuthentication} from "./components/AuthenticatedComponent";
import TicketStatistics from "./components/Tickets/TicketStatistics";
import Tickets from "./components/Tickets/index";
import TicketAddFlag from "./components/Tickets/InternalFlag/TicketAddFlag";
import TicketRemoveFlag from "./components/Tickets/InternalFlag/TicketRemoveFlag";
import HomePage from "./components/HomePage";
import Company from "./components/Company";
import Cashier from "./components/Account/Cashier";
import Customer from "./components/Customer";
import ForgotPasswordLog from "./components/Customer/ForgotPasswordLog";
import Moment from "moment";
import momentLocalizer from "react-widgets-moment";
import PaymentTransaction from "./components/Account/PaymentTransaction";
import CashierBooking from "./components/Account/Cashier/CashierBooking";
import PayOutRequest from "./components/Account/PayOutRequest";
import AccountBooking from "./components/Customer/AccountBooking";
import Messages from "./components/Customer/Messages";
import CashOutAnalytics from "./components/Tickets/CashOutAnalytics/CashOutAnalytics";

Moment.locale("de");
momentLocalizer();

const routes = (
    <Route>
        <Route path="/">
            <IndexRoute component={requireAuthentication(HomePage)}/>

            <Route path="login" component={Login} />

            <Route path="ticket" component={requireAuthentication(HomePage)}>
                <Route path="statistics" component={TicketStatistics} />
                <Route path="cash_out_analytics" component={CashOutAnalytics} />
                <Route path="tickets" component={Tickets} />
                <Route path="add_flag" component={TicketAddFlag} />
                <Route path="remove_flag" component={TicketRemoveFlag} />

            </Route>

            <Route path="company" component={requireAuthentication(HomePage)}>
                <Route path="shops" component={Company} />
            </Route>

            <Route path="account" component={requireAuthentication(HomePage)}>
                <Route path="cashier" component={Cashier} />
                <Route path="cashier_book_account" component={CashierBooking} />
                <Route path="payment_transactions" component={PaymentTransaction} />
                <Route path="payout_requests" component={PayOutRequest} />
            </Route>

            <Route path="customer" component={requireAuthentication(HomePage)}>
                <Route path="customers" component={Customer} />
                <Route path="account_bookings" component={AccountBooking} />
                <Route path="forgot_password" component={ForgotPasswordLog} />
                <Route path="messages" component={Messages} />
            </Route>

            {/* Must always be last - 404 */}
            <Route path="*" component={Error404}/>
        </Route>
    </Route>
);

export default routes;