// Partly taken from https://github.com/alicelieutier/smoothScroll/blob/master/smoothscroll.js

// Get the top position of an element in the context
var getTop = function (element, context) {
    return element.getBoundingClientRect().top + window.pageYOffset;
};

// ease in out function thanks to:
// http://blog.greweb.fr/2012/02/bezier-curve-based-easing-functions-from-concept-to-implementation/
var easeInOutCubic = function (t) {
    return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1
};

// calculate the scroll position we should be in
// given the start and end point of the scroll
// the time elapsed from the beginning of the scroll
// and the total duration of the scroll (default 500ms)
var position = function (start, end, elapsed, duration) {
    if (elapsed > duration) return end;
    return start + (end - start) * easeInOutCubic(elapsed / duration); // <-- you can change the easing funtion there
    // return start + (end - start) * (elapsed / duration); // <-- this would give a linear scroll
};

export function scrollToHtmlId(id) {
    const scrollTarget = document.querySelector("#" + id);
    if (!scrollTarget instanceof HTMLElement) {
        return;
    }

    // Offset by the height of the static menu bar at the top
    const topNavHeight = document.querySelector("#top-nav").offsetHeight;

    smoothScroll(scrollTarget, {offset: -topNavHeight})
    // window.scroll(0, getTop(scrollTarget) - topNavHeight);
}

export function smoothScrollToTop() {
    smoothScroll(document.body);
}

export function smoothScroll(el, options = {}) {
    options = {
        offset: 0,
        duration: 500,
        context: window,
        ...options
    };

    const {duration, offset, context, callback} = options;

    const start = window.pageYOffset;

    const clock = Date.now();

    var requestAnimationFrame = window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        function (fn) {
            window.setTimeout(fn, 15);
        };

    let end;
    if (typeof el === 'number') {
        end = parseInt(el) + offset;
    } else {
        end = getTop(el, context) + offset;
    }

    var step = () => {

        const elapsed = Date.now() - clock;
        if (context !== window) {
            context.scrollTop = position(start, end, elapsed, duration);
        } else {
            window.scroll(0, position(start, end, elapsed, duration));
        }

        if (elapsed > duration) {
            if (typeof callback === 'function') {
                callback(el);
            }
        } else {
            requestAnimationFrame(step);
        }
    };
    step();
}

const SCROLLBAR_COMPENSATION_CLASS = "scrollbar-compensated";
export function removeScrollbarCompensation(elementId) {
    const element = document.getElementById(elementId);

    element.style.paddingRight = null;
    element.classList.remove(SCROLLBAR_COMPENSATION_CLASS);
}

export function addScrollbarCompensation(elementId, scrollbarWidth) {
    const element = document.getElementById(elementId);

    if (element.classList.contains(SCROLLBAR_COMPENSATION_CLASS)) {
        return;
    }

    const currentPadding = parseFloat(window.getComputedStyle(element).getPropertyValue('padding-right'));
    element.style.paddingRight = `${currentPadding + scrollbarWidth}px`;
    element.classList.add(SCROLLBAR_COMPENSATION_CLASS);
}

export function getScrollbarWidth() {
    const hasScrollbar = document.body.scrollHeight > document.body.clientHeight;
    if (!hasScrollbar) {
        return 0; // No scrollbar visible
    }

    let inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    let outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    let w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    let w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2);
}