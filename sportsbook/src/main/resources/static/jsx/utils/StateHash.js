import md5 from "md5";
import * as Utils from "./index";
import log from "loglevel";

/**
 * Theses methods mirror the server-side StateHash class. The Hashes are the
 * base for determining if data has to be updated and must equal the server-side
 * implementation to work.
 */
export function forNavigationSports(state) {
    // filter
    let filter = state.user.preLiveSelection.filter;
    if (!filter) {
        filter = "";
    } else {
        filter = filter.trim();
    }

    // keyword
    let keyword = state.user.preLiveSelection.keyword;
    if (!keyword) {
        keyword = "";
    } else {
        keyword = keyword.trim();
    }

    // favorite
    let favorite = "false";
    if (state.user.preLiveSelection.showFavorite == true) {
        favorite = "true";
    }

    return hash(filter + keyword + favorite);
}

export function forSport(sportId, tipModelId, filter, keyword, maxOdd, minOdd) {
    let filterStr = filter ? filter : "";
    let keywordStr = keyword ? keyword : "";
    let maxOddStr = maxOdd ? maxOdd : "";
    let minOddStr = minOdd ? minOdd : "";

    return hash(`${sportId}` + `${tipModelId}` + filterStr + keywordStr + maxOddStr + minOddStr);
}

export function forTipGroups(gameId) {
    return hash(`${gameId}`);
}

export function forBetSlip({mode, games, systems, proceedIfOddsChanged, calcMaxStake}, authenticated, acceptOddModification, calcAllSystems, debug = false) {
    let state = mode + `${calcMaxStake}` + `${authenticated}` + (acceptOddModification || proceedIfOddsChanged) +
        games.reduce((acc, game) => acc.concat([game.isBank + game.bets.map(bet => bet.oddId).join(",")]), []).join(",");

    if (calcAllSystems) {
        state += "true"; // CalcAllSystems means automatically that systems are empty (+ true)
        if (debug) {
            log.debug("CalcAllSystems true, BetSlip State: ", state, hash(state));
        }
        return hash(state);
    }

    const activeSystems = systems.filter(s => s.isActive);

    state += (activeSystems.length === 0);

    activeSystems.forEach(({isActive, minSystem, stake}) => {
        state += `${isActive}` + `${minSystem}` + (stake ? `${Utils.toFixedPrecision(stake.toString().replace(",", "."), 5)}` : "null");
    });

    if (debug) {
        log.debug("BetSlip State: ", state, hash(state));
    }
    return hash(state);
}

export function forAccountTickets(ticketType, interval, page, from, to) {
    return md5(ticketType + interval + `${page}` + from + to);
}

export function hash(string) {
    return md5(string);
}
