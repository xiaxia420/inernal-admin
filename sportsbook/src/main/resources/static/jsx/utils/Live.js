import * as Utils from "./index";

function lockGame(game) {
    return {
        ...game,
        locked: true
    }
}

export function liveifyGame(liveState, game) {

    const gameIndex = liveState.indices.game;

    if (!gameIndex) {
        return lockGame(game);
    }

    const gamePath = gameIndex[game.gameId];

    if (!gamePath) {
        return lockGame(game);
    }

    const liveGame = Utils.getElementAtPath(liveState, gamePath);

    if (!liveGame) {
        return lockGame(game);
    }

    // Base Score
    const score = liveGame.scores.find(s => s.resultValueType === 1);

    if (score) {
        game = {
            ...game,
            score
        }
    }

    const locked = liveGame.feedStatus !== 1;

    game = {
        ...game,
        locked,
        status: liveGame.status,
        oneLineStatus: liveGame.oneLineStatus
    };

    /*
    if (!locked && game.notifications && game.notifications.length > 0) {
        // Remove stopped notifications from the game if the game is not locked.
        game = {
            ...game,
            notifications: game.notifications.filter(n => n.isStopped)
        }
    }
    */

    if (game.bets) {
        game = {
            ...game,
            bets: locked ? game.bets : game.bets.map(bet => liveifyBet(liveState, bet))
        }
    }

    return game;
}

function liveifyBet(liveState, bet) {
    const oddValueNew = getLiveOddValue(liveState, bet.oddId);
    const oddValueChanged = typeof oddValueNew !== "undefined" && oddValueNew !== null && oddValueNew !== bet.oddValue;

    if (oddValueChanged) {

        return {
            ...bet,
            oddValueChanged,
            oddValueNew
        }

    }

    return bet;
}

export function getLiveOddValue(liveState, oddId) {
    const oddPath = liveState.indices.odd[oddId];

    if (!oddPath) {
        return null;
    }

    const liveOdd = Utils.getElementAtPath(liveState, oddPath);

    if (!liveOdd) {
        return null;
    }

    return liveOdd.oddValue;
}