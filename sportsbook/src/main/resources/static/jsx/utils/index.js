export function getElementAtPath(state, path) {
    let key = path[0];
    if (typeof key === "function") {
        key = state.findIndex(key);
    }
    if (path.length == 0) {
        return state;
    }
    if (key === -1 || key === null) {
        return null;
    }
    return getElementAtPath(state[key], path.slice(1));
}

export function updateElementAtPath(state, path, func) {
    // if (typeof state === "undefined") {
    //     log.debug("Found undefined when trying to access", path);
    //     return;
    // }
    if (typeof path === "string") {
        path = path.split(".");
    }
    let key = path[0];
    /* Use functions to determine indices */
    if (typeof key === "function") {
        key = state.findIndex(key);
    }

    if (path.length == 0) {
        return func(state);
    } else if (typeof key === "number") {
        if (key === -1) {
            return state;
        }
        return [
            ...state.slice(0, key),
            updateElementAtPath(state[key], path.slice(1), func),
            ...state.slice(key + 1)
        ];
    } else {
        return {
            ...state,
            [key]: updateElementAtPath(state[key], path.slice(1), func)
        }
    }
}

export function deleteElementAtPath(state, path) {
    const deletePath = path.slice(0, path.length - 1);
    const deleteCondition = path[path.length - 1];
    return updateElementAtPath(state, deletePath, array => removeElement(array, deleteCondition));
}

export function removeElementAtIndex(array, index) {
    if (index === -1) return array;
    return [
        ...array.slice(0, index),
        ...array.slice(index + 1)
    ];
}

export function removeElement(array, removeCondition) {
    let index = -1;
    if (typeof removeCondition === "number") {
        index = removeCondition;
    } else if (typeof removeCondition === "function") {
        index = array.findIndex(removeCondition);
    }
    return removeElementAtIndex(array, index);
}

export function toFixedPrecision(value, precision) {
    if (typeof value === "string") {
        value = parseFloat(value);
    }
    return value.toFixed(precision);
}

/* If its a number, it is calculated by the reducer, so format it */
export const formatStake = (stake, intl, currencyDecimalDigits) => {
    if (typeof stake !== "number") {
        return stake;
    } else {
        if (stake == 0) {
            return null;
        }
        return intl.formatNumber(stake, {
            minimumFractionDigits: currencyDecimalDigits,
            maximumFractionDigits: currencyDecimalDigits,
            useGrouping: false
        }).replace(",", ".");
    }
};

/**
 * Sorts path arrays so the highest indices come first. This is for ordering deletions
 * without shifting indices that would affect following operations.
 *
 * Usage: array.sort(comparePaths)
 *
 * @param a
 * @param b
 * @returns {number}
 */
export function comparePaths(a, b) {
    const length = Math.max(a.length, b.length);

    for (let i = 0; i < length; i++) {
        // If the indices are equal, go to the next.
        if (a[i] === b[i]) {
            if (i === length - 1) {
                // If we reached the end here, the arrays are equal.
                return 0;
            }
            continue;
        }
        // A is shorter than B
        if (typeof a[i] === "undefined") {
            // Put A behind B
            return 1;
        }
        // B is shorter than A
        if (typeof b[i] === "undefined") {
            // Put B behind A
            return -1;
        }
        // Index from A is smaller than the one from B
        if (a[i] < b[i]) {
            // Put A behind B
            return 1;
        }
        // Index from B is smaller than the one from A
        if (b[i] < a[i]) {
            // Put B behind A
            return -1;
        }
    }
}

export function formatDate(date) {
    if ((date instanceof Date)) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!
        var yyyy = date.getFullYear();
        if( dd < 10 ) {
            dd = '0' + dd;
        }

        if( mm < 10 ) {
            mm = '0' + mm;
        }

        return yyyy + '-' + mm + '-' + dd;
    }
    return null;
}