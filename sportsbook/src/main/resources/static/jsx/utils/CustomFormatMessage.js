import React, {Component} from "react";
import PropTypes from 'prop-types';
import {intlShape, injectIntl} from "react-intl"

class CustomFormatMessage extends React.Component {

    setDom =(ele, cssClass, messageText, values) => {
        let  valop = (typeof(values) === "object")? true : false;

        if (ele ===  "div") {
            if (valop) {
                return <div className={cssClass} dangerouslySetInnerHTML= {{__html: messageText}} />
            }
            return <div className={cssClass}>{messageText}</div>;
        }
        else if (ele=== "span") {
            if(valop) {
                return <span className={cssClass} dangerouslySetInnerHTML= {{__html: messageText}} />
            }
            return <span className={cssClass}>{messageText}</span>;
        }
    }

    render() {
        const {cssClass, ele, id, values, intl} = this.props;

        const valObj = {
            id: id,
            defaultMessage:"error parsing CustomFormatMessage"
        }

        const messageText = intl.formatHTMLMessage({
                ...valObj
        }, values);
        
        const output = this.setDom(ele, cssClass, messageText, values);

        return (
            output
        );
    }
}

CustomFormatMessage.propTypes = {
    intl: intlShape.isRequired,
    id: PropTypes.string.isRequired,
    ele: PropTypes.string,
    cssClass: PropTypes.string,
    values: PropTypes.object
};

CustomFormatMessage.defaultProps = {
    ele: "div"
}

CustomFormatMessage = injectIntl(CustomFormatMessage, {withRef: true});


export default CustomFormatMessage;