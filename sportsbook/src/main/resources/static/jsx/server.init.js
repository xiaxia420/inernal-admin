import {Provider} from "react-redux";
import {Router, match, RouterContext} from "react-router";
import {createStore} from "redux";
import rootReducer from "./reducers";
import I18NProvider from "./components/I18NProvider";
import DocumentTitle from "./modules/DocumentTitle";
import configureStore from "./store/configureStore";
import routes from "./routes";
import React from "react";
import ReactDOMServer from "react-dom/server";

export {
    React,
    ReactDOMServer,
    Provider,
    Router,
    match,
    RouterContext,
    createStore,
    rootReducer,
    I18NProvider,
    configureStore,
    routes,
    DocumentTitle
};
