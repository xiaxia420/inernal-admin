import {createSelector} from "reselect";

const timezoneSelector = (state, props) => state.user.timezone;
const timezonesSelector = (state, props) => state.app.i18n.timezones;

const languageSelector = (state, props) => state.user.locale;
const languagesSelector = (state, props) => state.app.i18n.languages;

export const userTimeZoneSelector = createSelector(
    timezoneSelector,
    timezonesSelector,
    (timezone, timezones) => {
        return timezones.find(tz => tz.id == timezone);
    }
);

export const userLanguageSelector = createSelector(
    languageSelector,
    languagesSelector,
    (language, languages) => {
        return languages.find(l => l.code == language);
    }
);
