import {SHOW_NOTIFICATION} from "../actionTypes/notifications";
import NotificationHandler from "../utils/NotificationHandler";

/**
 * Shows SHOW_NOTIFICATION actions with the notification system.
 * The action properties:
 *   - [category] For special handling of some notifications that trigger further actions.
 *   - level The level (success/error/warning/info)
 *   - message The message to display
 */
export const notificationMiddleware = store => next => action => {
    if (action.type === SHOW_NOTIFICATION) {
        NotificationHandler.handleNotification(action);
    }

    return next(action);
};