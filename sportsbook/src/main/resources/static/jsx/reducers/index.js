import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import app from "./app";
import networking from "./networking";
import user from "./user";
import payment from "./payment";
import company from "./company";
import customer from "./customer";
import notification from "./notification";
import ticket from "./ticket";
import {createResponsiveStateReducer} from "redux-responsive";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
    routing: routerReducer,
    networking,
    app,
    user,
    payment,
    company,
    customer,
    ticket,
    notification,
    form: formReducer,
    /*
    form: formReducer.normalize({
        register: registerNormalizer,
        myData: registerNormalizer
    }),
    */
    browser: createResponsiveStateReducer({
        xs: 479,
        is: 767,
        sm: 991,
        md: 1199,
        lg: 1499
    })
});

export default rootReducer;