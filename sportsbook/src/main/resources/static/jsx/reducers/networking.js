import {combineReducers} from "redux";
import {CONNECTED, DISCONNECTED, CONNECTION_FORCE_CLOSED} from "../actionTypes/networking";

function initialConnect(state = true, action) {
    switch (action.type) {
        case CONNECTED:
            return false;
        default:
            return state;
    }
}

function connected(state = false, action) {
    switch (action.type) {
        case CONNECTED:
            return true;
        case DISCONNECTED:
            return false;
        default:
            return state;
    }
}

function forceDisconnected(state = false, action) {
    switch (action.type) {
        case CONNECTION_FORCE_CLOSED:
            return true;
        default:
            return state;
    }
}

const networkingReducer = combineReducers({
    initialConnect,
    forceDisconnected,
    connected
});

export default networkingReducer;