import {combineReducers} from "redux";
import {
    SHOW_GENERAL_ERROR
} from "../../actionTypes/app";
import {
    RECEIVE_CASH_FLOW_NOTIFICATION,
    RECEIVE_MESSAGE_NOTIFICATION,
    RECEIVE_PAY_OUT_REQUEST_NOTIFICATION,
    RECEIVE_PAYMENT_TRANSACTION_NOTIFICATION,
    REQUEST_CASH_FLOW_NOTIFICATION,
    REQUEST_MESSAGE_NOTIFICATION,
    REQUEST_PAY_OUT_REQUEST_NOTIFICATION,
    REQUEST_PAYMENT_TRANSACTION_NOTIFICATION
} from "../../actionTypes/notification";

function messageNotification(state = {isLoading: false, notification: null}, action) {
    switch (action.type) {
        case REQUEST_MESSAGE_NOTIFICATION:
            return {
                ...state,
                isLoading: true
            };
        case RECEIVE_MESSAGE_NOTIFICATION:
            return {
                isLoading: false,
                notification: action.notification
            };
        case SHOW_GENERAL_ERROR:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
}

function payOutRequestNotification(state = {isLoading: false, notification: null}, action) {
    switch (action.type) {
        case REQUEST_PAY_OUT_REQUEST_NOTIFICATION:
            return {
                ...state,
                isLoading: true
            };
        case RECEIVE_PAY_OUT_REQUEST_NOTIFICATION:
            return {
                isLoading: false,
                notification: action.notification
            };
        case SHOW_GENERAL_ERROR:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
}

function paymentTransactionNotification(state = {isLoading: false, notification: null}, action) {
    switch (action.type) {
        case REQUEST_PAYMENT_TRANSACTION_NOTIFICATION:
            return {
                ...state,
                isLoading: true
            };
        case RECEIVE_PAYMENT_TRANSACTION_NOTIFICATION:
            return {
                isLoading: false,
                notification: action.notification
            };
        case SHOW_GENERAL_ERROR:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
}

function cashFlowNotification(state = {isLoading: false, notification: null}, action) {
    switch (action.type) {
        case REQUEST_CASH_FLOW_NOTIFICATION:
            return {
                ...state,
                isLoading: true
            };
        case RECEIVE_CASH_FLOW_NOTIFICATION:
            return {
                isLoading: false,
                notification: action.notification
            };
        case SHOW_GENERAL_ERROR:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
}


const notificationReducer = combineReducers({
    messageNotification,
    payOutRequestNotification,
    paymentTransactionNotification,
    cashFlowNotification
});

export default notificationReducer;