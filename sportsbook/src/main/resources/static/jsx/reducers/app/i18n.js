import {combineReducers} from "redux";
import {
    REQUEST_TIMEZONES,
    RECEIVE_TIMEZONES,

    REQUEST_CURRENCIES,
    RECEIVE_CURRENCIES,

    REQUEST_LANGUAGES,
    RECEIVE_LANGUAGES

} from "../../actionTypes/app";

function messages(state = {}) {
    return state;
}

function languages(state = [], action) {
    switch (action.type) {
        case REQUEST_LANGUAGES:
            return [];
        case RECEIVE_LANGUAGES:
            return action.languages;
        default:
            return state;
    }
}

function timezones(state = [], action) {
    switch (action.type) {
        case REQUEST_TIMEZONES:
            return [];
        case RECEIVE_TIMEZONES:
            return action.timezones;
        default:
            return state;
    }
}

const i18nReducer = combineReducers({
    messages,
    languages,
    timezones
});

export default i18nReducer;