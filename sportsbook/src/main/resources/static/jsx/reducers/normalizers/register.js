import {compose, onlyNums, maxLength} from "./index";

const registerNormalizer = {
    firstName: maxLength(40),
    lastName: maxLength(40),
    phone: onlyNums(),
    street: maxLength(80),
    postalCode: maxLength(20),
    city: maxLength(40),
    email: maxLength(120),
    emailConfirm: maxLength(120),
    username: maxLength(25),
    password: maxLength(120),
    bonusCode: maxLength(120),

    birthdayDay: compose(maxLength(2), onlyNums()),
    birthdayMonth: compose(maxLength(2), onlyNums()),
    birthdayYear: compose(maxLength(4), onlyNums()),
    // Calculated value
    birthday: (v, pv, allValues) => {
        const {birthdayYear, birthdayMonth, birthdayDay} = allValues;
        return new Date(birthdayYear, birthdayMonth - 1, birthdayDay);
    },
    // additional for my data
    birthPlace: maxLength(40),
    secondaryEmail: maxLength(120)
    
};

export default registerNormalizer;