import {combineReducers} from "redux";
import {
    REQUEST_FORGOT_PASSWORD_LOGS,
    RECEIVE_FORGOT_PASSWORD_LOGS,
    ERROR_FORGOT_PASSWORD_LOGS,
    RESET_SEARCH_CUSTOMER_FILTER,
    REQUEST_SEARCH_CUSTOMERS,
    RECEIVE_SEARCH_CUSTOMERS,
    REQUEST_SEARCH_CASHIERS,
    RECEIVE_SEARCH_CASHIERS,
    CLEAR_SEARCH_CUSTOMERS,
    RESET_CUSTOMER_ACCOUNT_BOOKINGS_FILTER,
    RECEIVE_CUSTOMER_ACCOUNT_BOOKINGS,
    REQUEST_CUSTOMER_ACCOUNT_BOOKINGS,
    CLEAR_CUSTOMER_ACCOUNT_BOOKINGS,
    REQUEST_CUSTOMER_MESSAGES, RESET_CUSTOMER_MESSAGES_FILTER, CLEAR_CUSTOMER_MESSAGES, RECEIVE_CUSTOMER_MESSAGES
} from "../../actionTypes/customer";
import {SHOW_GENERAL_ERROR} from "../../actionTypes/app";

function forgotPasswordPagination(state = null, action) {
    switch (action.type) {
        case RECEIVE_FORGOT_PASSWORD_LOGS:
            return action.pagination;
        default:
            return state;
    }
}

function forgotPasswordLogs(state = {isLoading: false, logs: []}, action) {
    switch (action.type) {
        case REQUEST_FORGOT_PASSWORD_LOGS:
            return {
                isLoading: true,
                logs: []
            };
        case RECEIVE_FORGOT_PASSWORD_LOGS:
            return {
                isLoading: false,
                logs: action.customerLogs
            };
        case ERROR_FORGOT_PASSWORD_LOGS:
            return {
                isLoading: false,
                logs: []
            };
        default:
            return state;
    }
}

function forgotPasswordError(state = null, action) {
    switch (action.type) {
        case REQUEST_FORGOT_PASSWORD_LOGS:
            return null;
        case RECEIVE_FORGOT_PASSWORD_LOGS:
            return null;
        case ERROR_FORGOT_PASSWORD_LOGS:
            return action.error;
        default:
            return state;
    }
}

function customerSearchFilter(state = {companyId: null, keyWord: "", signature: ""}, action) {
    switch (action.type) {
        case RESET_SEARCH_CUSTOMER_FILTER:
            return {
                companyId: action.companyId,
                keyWord: action.keyWord,
                signature: state.signature
            };
        case CLEAR_SEARCH_CUSTOMERS:
            return {
                companyId: null, keyWord: "", signature: ""
            };
        case RECEIVE_SEARCH_CUSTOMERS:
            return {
                ...state,
                signature: action.signature
            };
        default: return state;
    }
}

function customerSearchPagination(state = {pageIndex: 0, pageSize: 25}, action) {
    switch (action.type) {
        case RECEIVE_SEARCH_CUSTOMERS:
            return action.pagination;
        case CLEAR_SEARCH_CUSTOMERS:
            return {pageIndex: 0, pageSize: 25};
        default:
            return state;
    }
}

function customers(state = {isLoading: false, customers: []}, action) {
    switch (action.type) {
        case REQUEST_SEARCH_CUSTOMERS:
            return {
                isLoading: true,
                customers: []
            };
        case RECEIVE_SEARCH_CUSTOMERS:
            return {
                isLoading: false,
                customers: action.customers
            };
        case CLEAR_SEARCH_CUSTOMERS:
            return {
                isLoading: false, customers: []
            };
        case SHOW_GENERAL_ERROR:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
}

function allCashiers(state = null, action) {
    switch (action.type) {
        case REQUEST_SEARCH_CASHIERS:
            return null;
        case RECEIVE_SEARCH_CASHIERS:
            return action.customers;
        default:
            return state;
    }
}

function customerAccountBookingsFilter(
    state = {
        customerUsername: "",
        keyWord: "",
        from: null,
        to: null,
        bookingType: null,
        bookingReason: null,
        category: 1,
        orderDescend: true,
        signature: ""
    }, action
) {
    switch (action.type) {
        case RESET_CUSTOMER_ACCOUNT_BOOKINGS_FILTER:
            return {
                ...action.filter,
                signature: state.signature
            };
        case CLEAR_CUSTOMER_ACCOUNT_BOOKINGS:
            return {
                customerUsername: "",
                keyWord: "",
                from: null,
                to: null,
                bookingType: null,
                bookingReason: null,
                category: 1,
                orderDescend: true,
                signature: ""
            };
        case RECEIVE_CUSTOMER_ACCOUNT_BOOKINGS:
            return {
                ...state,
                signature: action.signature
            };
        default:
            return state;
    }
}

function customerAccountBookingsPagination(state = {pageIndex: 0, pageSize: 25}, action) {
    switch (action.type) {
        case RECEIVE_CUSTOMER_ACCOUNT_BOOKINGS:
            return action.pagination;
        case CLEAR_CUSTOMER_ACCOUNT_BOOKINGS:
            return {pageIndex: 0, pageSize: 25};
        default:
            return state;
    }
}

function customerAccountBookings(state = {isLoading: false, bookings: []}, action) {
    switch (action.type) {
        case REQUEST_CUSTOMER_ACCOUNT_BOOKINGS:
            return {
                isLoading: true,
                bookings: []
            };
        case RECEIVE_CUSTOMER_ACCOUNT_BOOKINGS:
            return {
                isLoading: false,
                bookings: action.accountBookings
            };
        case CLEAR_CUSTOMER_ACCOUNT_BOOKINGS:
            return {isLoading: false, bookings: []};
        case SHOW_GENERAL_ERROR:
            return {...state, isLoading: false};
        default:
            return state;
    }
}

function customerMessagesFilter(
    state = {from: null, to: null, messageStatus: null, signature: "", customerUsername: ""},
    action
) {
    switch (action.type) {
        case RESET_CUSTOMER_MESSAGES_FILTER:
            return {
                ...action.filter,
                signature: state.signature
            };
        case CLEAR_CUSTOMER_MESSAGES:
            return {
                from: null,
                to: null,
                messageStatus: null,
                signature: ""
            };
        case RECEIVE_CUSTOMER_MESSAGES:
            return {
                ...state,
                signature: action.signature
            };
        default:
            return state;
    }
}

function customerMessagesPagination(state = {pageIndex: 0, pageSize: 25}, action) {
    switch (action.type) {
        case RECEIVE_CUSTOMER_MESSAGES:
            return action.pagination;
        case CLEAR_CUSTOMER_MESSAGES:
            return {pageIndex: 0, pageSize: 25};
        default:
            return state;
    }
}

function customerMessages(state = {isLoading: false, messages: []}, action) {
    switch (action.type) {
        case REQUEST_CUSTOMER_MESSAGES:
            return {
                isLoading: true,
                messages: []
            };
        case RECEIVE_CUSTOMER_MESSAGES:
            return {
                isLoading: false,
                messages: action.messages
            };
        case CLEAR_CUSTOMER_MESSAGES:
            return {
                isLoading: false,
                messages: []
            };
        case SHOW_GENERAL_ERROR:
            return {...state, isLoading: false};
        default:
            return state;
    }
}

const customerReducer = combineReducers({
    forgotPasswordPagination,
    forgotPasswordLogs,
    forgotPasswordError,

    customerSearchFilter,
    customerSearchPagination,
    customers,
    allCashiers,

    customerAccountBookingsFilter,
    customerAccountBookingsPagination,
    customerAccountBookings,

    customerMessagesFilter,
    customerMessagesPagination,
    customerMessages
});

export default customerReducer;