const webpack = require("webpack");
const WebpackConfig = require("webpack-config").Config;

module.exports = new WebpackConfig().merge({
    plugins: [
        // Only load necessary momentjs locales
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|en/)
    ]
});