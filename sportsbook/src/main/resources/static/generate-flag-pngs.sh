#!/bin/sh

# Works on OSX without dependencies and produces better results than e.g. ImageMagick

rm -r themes/betserv/img/flags
mkdir -p themes/betserv/img/flags
for file in node_modules/flag-icon-css/flags/1x1/*
do
    echo "Converting $file"
    qlmanage -t -s 30 -o themes/betserv/img/flags "$file"
done

for file in themes/betserv/img/flags/*
do
    echo "Renaming $file"
    mv "$file" "${file%.*}@2x.png"
done