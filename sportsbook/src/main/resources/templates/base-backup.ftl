<#import "spring.ftl" as spring>
<#--<#import "react.ftl" as react>-->
<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->

<!DOCTYPE html>
<html lang="en" id="top">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><#if (reactResult.pageTitle)??>${reactResult.pageTitle!}</#if></title>

    <link rel="stylesheet" href="<@spring.url "/resources/js/bundle/main.css"/>" type="text/css"/>

    <#include 'themes/favicons-' + theme + '.ftl'/>

    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    <#if betradarSirWidgetClient?hasContent>
        <script async src="https://widgets.sir.sportradar.com/${betradarSirWidgetClient}/widgetloader" data-sr-language="${language}" data-sr-theme="neutral"></script>
    </#if>
    <#if betradarScriptUrl?hasContent>
        <script async src=${betradarScriptUrl} type="text/javascript"></script>
    </#if>
<script>
    window.__INITIAL_STATE__ = ${state};
    window.csrfHeader = {"${_csrf.headerName}" : "${_csrf.token}"};
    <#if webSocketPort?hasContent>
    window.webSocketPort = ${webSocketPort};
    <#-- window.webSocketPort = ${webSocketPort?c}; -->
    </#if>
</script>

</head>
<body>
<#compress>
    <div id="app" class="l__grid-site">${(reactResult.html)!}</div>
</#compress>

<script src="<@spring.url "/resources/js/bundle/vendor.bundle.js" />"></script>
<script src="<@spring.url "/resources/js/bundle/main.bundle.js" />"></script>

</body>
</html>
