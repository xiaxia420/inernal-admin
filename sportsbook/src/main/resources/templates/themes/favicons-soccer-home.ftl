<#import "../spring.ftl" as spring>

<link rel="apple-touch-icon" sizes="180x180" href="<@spring.url "/resources/img/favicons/apple-touch-icon.png"/>">
<link rel="icon" type="image/png" sizes="32x32" href="<@spring.url "/resources/img/favicons/favicon-32x32.png"/>">
<link rel="icon" type="image/png" sizes="16x16" href="<@spring.url "/resources/img/favicons/favicon-16x16.png"/>">
<link rel="manifest" href="<@spring.url "/resources/img/favicons/manifest.json"/>">
<link rel="mask-icon" href="<@spring.url "/resources/img/favicons/safari-pinned-tab.svg"/>" color="#c72127">
<meta name="theme-color" content="#ffffff">